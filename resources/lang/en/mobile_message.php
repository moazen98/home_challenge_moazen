<?php

return [
    'success' => 'The data created successful',
    'delete' => 'The data deleted successful',
    'failed' => 'There is an error',
    'string' => 'The field must be a string',
    'update' => 'The data updated successful',

];
