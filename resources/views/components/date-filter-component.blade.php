<div class="col-{{$col}}">
    <div class="form-group">
        <label class="form-label" for="name">{{ _t('dashboard.Date') }}</label>
        <div class="mb-1">
            <input type="date"
                   id="publish_date"
                   class="form-control"
                   name="publish_date"
                   aria-label="publish_date"
                   aria-describedby="basic-addon-name">
        </div>
    </div>
</div>
