<link rel='stylesheet'
      href='https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'>


<div class="col-12">
    <div class="alert alert-primary" role="alert">
        <div class="alert-body">
            <!-- Vertical Wizard -->
            <div class="card-body">
                <div class="mb-1">

                    <div>
                        <small>{{_t('dashboard.Meta information')}} . ({{_t('dashboard.SEO')}})</small>
                    </div>

                    <br>
                    @if(!$hideSlug)
                        <div class="row col-12 mb-2">
                            <div class="col-6">
                                <label class="form-label"
                                       for="basic-addon-name">{{ _t('dashboard.Slug') }}</label>
                                <div class="input-box">
                                    <span class="prefix">www.domain.com/url/</span>
                                    <input
                                        type="text"
                                        id="basic-addon-name"
                                        class="form-control"
                                        name="slug"
                                        aria-label="slug"
                                        aria-describedby="basic-addon-name"
                                        value="{{old('slug')}}"
                                    />
                                </div>

                                @error('slug')
                                <span class="text-danger">{{$message }}</span>
                                @enderror
                            </div>

                        </div>
                    @endif


                    <div class="row col-12 mb-2">
                        @foreach (config('translatable.locales') as $locale)
                            <div class="col-6">
                                <label class="form-label"
                                       for="basic-addon-name">{{ _t('dashboard.Title') }} {{__($locale)}}</label>
                                <input
                                    type="text"
                                    id="basic-addon-name"
                                    class="form-control"
                                    name="{{ $locale }}[title_seo]"
                                    aria-describedby="basic-addon-name"
                                    value="{{ old($locale . '.title_seo') }}"
                                />
                                @error($locale . '.title_seo')
                                <span class="text-danger">{{$message }}</span>
                                @enderror
                            </div>
                        @endforeach

                    </div>

                    <div class="row col-12 mb-2">
                        @foreach (config('translatable.locales') as $locale)
                            <div class="mb-2 col-6">
                                <label class="form-label"
                                       for="basic-addon-name">{{ _t('dashboard.Description') }} {{__($locale)}}</label>
                                <input
                                    type="text"
                                    id="basic-addon-name"
                                    class="form-control"
                                    name="{{ $locale }}[description_seo]"
                                    aria-describedby="basic-addon-name"
                                    value="{{ old($locale . '.description_seo') }}"
                                />
                                @error($locale . '.description_seo')
                                <span class="text-danger">{{$message }}</span>
                                @enderror
                            </div>
                        @endforeach
                    </div>


                    <div class="row col-12 mb-2">
                        <label class="form-label"
                               for="basic-addon-name">{{ _t('dashboard.Keywords') }}</label>
                        <div
                            style="margin-bottom: 1%;">{{ _t('dashboard.Please press enter to insert  the attributes.') }}</div>
                        <div class="form-group">
                            <input type="text" name="atrr" class="form-control"
                                   value="{{ old('atrr') }}"
                                   data-role="tagsinput"
                                   style=" width: 600px;color: #C0C0C0;!important;"/>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
