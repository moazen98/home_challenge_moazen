<div class="col-{{$col}}">
    <label class="form-label" for="main">{{ _t('dashboard.Main') }}</label>
    <div class="mb-1">
        <select class="select2 form-select" id="main" name="main">
            <option value="2" selected>{{ _t('dashboard.All') }}</option>
            <option value="1">{{_t('dashboard.Main')}}</option>
            <option value="0">{{_t('dashboard.Not main')}}</option>
        </select>
    </div>
</div>
