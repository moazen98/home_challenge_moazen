<div class="col-{{$col}}">
    <label class="form-label" for="STATUS">{{ _t('dashboard.Message status') }}</label>
    <div class="mb-1">
        <select class="select2 form-select" id="READ" name="READ">
            <option value="2" selected>{{ _t('dashboard.All') }}</option>
            <option
                value="0">{{_t('dashboard.Not read')}}</option>
            <option
                value="1">{{_t('dashboard.Read')}}</option>
        </select>
    </div>
</div>
