<div class="row col-12 mb-2">
<div class="card">

    <div class="card-body">
        <div
            class="invoice-repeater">
            <label class="form-label" for="image">{{ _t('dashboard.Media images') }}</label>
            <div
                data-repeater-list="invoice_images">
                <div
                    data-repeater-item>
                    <div style="    display: flex;
                                  flex-direction: row;" class="row d-flex align-items-end">

                        <div class="row col-12 mb-2 mt-3">
                            <div class="col-6">
                                <label class="form-label" for="image">{{ _t('dashboard.Images') }}</label>
                                <div class="card-body">
                                    <input class="form-control" id="uploadImage" type="file" name="images"
                                           accept="image/png, image/jpg, image/jpeg" required/>
                                    @error('images')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                    <img style="width: 129px; margin-top: 2%;" id="uploadPreview"/>
                                </div>
                            </div>
                        </div>

                        <div
                            class="mb-1">
                            <button
                                class="btn btn-outline-danger text-nowrap px-1"
                                data-repeater-delete
                                type="button">
                                <i data-feather="x"
                                   class="me-25"></i>
                                <span>{{_t('dashboard.Delete')}}</span>
                            </button>
                        </div>

                    </div>
                    <hr/>
                </div>
            </div>
            <div class="row mt-2">
                <div
                    class="col-12">
                    <button
                        class="btn btn-icon btn-primary"
                        type="button"
                        data-repeater-create>
                        <i data-feather="plus"
                           class="me-25"></i>
                        <span>{{_t('dashboard.Add new')}}</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
