<div class="row col-12 mb-2">
    <div class="col-6">
        <label for="section">{{_t('dashboard.Tags')}}</label>
        <select class="select2 form-select" name="category[]" multiple>
            @foreach($tags['tags'] as $tag)
                <option
                    value="{{$tag['id']}}">{{$tag['title']}}</option>
            @endforeach
        </select>
        @error('category')
        <span class="text-danger">{{$message }}</span>
        @enderror
    </div>
</div>
