<div class="col-{{$col}}">
    <label class="form-label" for="STATUS">{{ _t('dashboard.User status') }}</label>
    <div class="mb-1">
        <select class="select2 form-select" id="STATUS" name="STATUS">
            <option value="2" selected>{{ _t('dashboard.All') }}</option>
            <option
                value="1">{{_t('dashboard.Active')}}</option>
            <option
                value="0">{{_t('dashboard.InActive')}}</option>
        </select>
    </div>
</div>
