<div class="form-group col-{{$col}}" style="margin-bottom: 10px">
    <input type="text" class="form-control" name="search" id="search"
           placeholder="{{$message}}"
           value="{{ request()->input('search') }}">
    <span class="text-danger">@error('search'){{ $message }} @enderror</span>
</div>
