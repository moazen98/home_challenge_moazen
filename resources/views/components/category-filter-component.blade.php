<div class="col-{{$col}}">
    <label class="form-label" for="STATUS">{{ _t('dashboard.Category') }}</label>
    <div class="mb-1">
        <select class="select2 form-select" id="category" name="category">
            <option value="NON" selected>{{ _t('dashboard.All') }}</option>
            @foreach($categories as $category)
                <option
                    value="{{$category['id']}}">{{$category['value_name']}}</option>
            @endforeach
        </select>
    </div>
</div>
