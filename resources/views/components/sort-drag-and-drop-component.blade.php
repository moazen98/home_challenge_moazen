<h2 class="my-2">{{$title}} :</h2>
<div class="card mb-4" style="width: 50%">
    <ul class="list-group list-group-flush" id="sortable-list">
        @foreach($items['items'] as $item)
            <li data-id="{{$item['id']}}"
                class="list-group-item">{{$item['title'] .' - '. _t('dashboard.ID') .' : '. $item['id']}}</li>
        @endforeach
    </ul>
</div>






