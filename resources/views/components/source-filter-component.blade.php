<div class="col-{{$col}}">
    <label class="form-label" for="STATUS">{{ _t('dashboard.Source') }}</label>
    <div class="mb-1">
        <select class="select2 form-select" id="source" name="source">
            <option value="NON" selected>{{ _t('dashboard.All') }}</option>
            @foreach($sources as $source)
                <option
                    value="{{$source['id']}}">{{$source['name']}}</option>
            @endforeach
        </select>
    </div>
</div>
