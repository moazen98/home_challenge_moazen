<div class="row col-12 mb-2">
    <div class="col-6">
        <label for="section">{{_t('dashboard.Tags')}}</label>
        <select class="select2 form-select" name="category[]" multiple>
            @foreach($categories['tags'] as $category)
                <option
                    @if($tags['tags_ids']) @if(in_array($category['id'], $tags['tags_ids'])) selected
                    @endif @endif
                    value="{{$category['id']}}">{{$category['title']}}</option>
            @endforeach
        </select>
        @error('category')
        <span class="text-danger">{{$message }}</span>
        @enderror
    </div>
</div>
