<div class="col-4">
    <select class="form-select select2" id="page_counter"
            name="page_counter">
        <option value="{{\App\Enums\PaginateType::FIRST}}" selected>{{\App\Enums\PaginateType::FIRST}}</option>
        <option value="{{\App\Enums\PaginateType::SECOND}}">{{\App\Enums\PaginateType::SECOND}}</option>
        <option value="{{\App\Enums\PaginateType::THIRD}}">{{\App\Enums\PaginateType::THIRD}}</option>
    </select>
</div>
<div class="col-4">
    <select class="form-select select2" id="hidden_sort_type"
            name="hidden_sort_type">

        <option value="ASC">{{_t('dashboard.Ascending')}}</option>
        <option value="DESC" selected>{{_t('dashboard.Descent')}}</option>
    </select>
</div>
