<!-- BEGIN: Vendor CSS-->

@if ($configData['direction'] === 'rtl' && isset($configData['direction']))
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/vendors-rtl.min.css')) }}"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>

    <!--end::Fonts-->

    <!-- Moazen custom-->

    <link rel="stylesheet" href="{{asset('dashboard/css/custom.css')}}"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- End Moazen custom-->


    <style>
        .bs-stepper .bs-stepper-content .content {
            margin-inline-start: 0;
        }
    </style>

    <style>
        .iti--allow-dropdown input, .iti--allow-dropdown input[type=text], .iti--allow-dropdown input[type=tel], .iti--separate-dial-code input, .iti--separate-dial-code input[type=text], .iti--separate-dial-code input[type=tel] {
            padding-right: 100px !important;
        }
    </style>



@else

    <!-- Moazen custom-->

    <link rel="stylesheet" href="{{asset('dashboard/css/custom.css')}}"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- End Moazen custom-->


    <style>
        .bs-stepper .bs-stepper-content .content {
            margin-inline-start: 0;
        }
    </style>

    <style>
        .iti--allow-dropdown input, .iti--allow-dropdown input[type=text], .iti--allow-dropdown input[type=tel], .iti--separate-dial-code input, .iti--separate-dial-code input[type=text], .iti--separate-dial-code input[type=tel] {
            padding-right: 100px !important;
        }
    </style>


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/vendors.min.css')) }}"/>
@endif

@yield('vendor-style')
<!-- END: Vendor CSS-->


<!-- BEGIN: Theme CSS-->


@php $configData = Helper::applClasses(); @endphp


<!-- BEGIN: Page CSS-->
@if ($configData['mainLayoutType'] === 'horizontal')
    <link rel="stylesheet" href="{{ asset(mix('css/base/core/menu/menu-types/horizontal-menu.css')) }}"/>
@else
    <link rel="stylesheet" href="{{ asset(mix('css/base/core/menu/menu-types/vertical-menu.css')) }}"/>



@endif

@yield('page-style')

<!-- laravel style -->
<link rel="stylesheet" href="{{ asset(mix('css/overrides.css')) }}"/>

<!-- BEGIN: Custom CSS-->
@if(session()->get('direction') === 'rtl')
    {{--    @if( app()->getLocale() === 'ar')--}}
    {{--    @if($configData['direction'] === 'rtl')--}}



    <link rel="stylesheet" href="{{ asset(mix('css/base/themes/dark-layout.css')) }}"/>
    <link rel="stylesheet" href="{{ asset(mix('css/base/themes/bordered-layout.css')) }}"/>
    <link rel="stylesheet" href="{{ asset(mix('css/base/themes/semi-dark-layout.css')) }}"/>


    <link rel="stylesheet" href="{{ asset('vendors/css/vendors-rtl.min.css') }}"/>

    <link rel="stylesheet" href="{{ asset('css-rtl/custom-rtl.css') }}"/>

    <link rel="stylesheet" href="{{ asset('css-rtl/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css-rtl/bootstrap-extended.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css-rtl/colors.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css-rtl/components.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css-rtl/dark-layout.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css-rtl/bordered-layout.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css-rtl/semi-dark-layout.min.css') }}"/>


    <link rel="stylesheet" href="{{ asset('css-rtl/dashboard-ecommerce.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css-rtl/ext-component-toastr.min.css') }}"/>


@else

    <link rel="stylesheet" href="{{ asset(mix('css/core.css')) }}"/>
    <link rel="stylesheet" href="{{ asset(mix('css/base/themes/dark-layout.css')) }}"/>
    <link rel="stylesheet" href="{{ asset(mix('css/base/themes/bordered-layout.css')) }}"/>
    <link rel="stylesheet" href="{{ asset(mix('css/base/themes/semi-dark-layout.css')) }}"/>

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
@endif



