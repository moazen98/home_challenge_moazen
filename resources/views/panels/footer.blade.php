<!-- BEGIN: Footer-->
<footer class="footer footer-light {{($configData['footerType'] === 'footer-hidden') ? 'd-none':''}} {{$configData['footerType']}}">
  <p class="clearfix mb-0">
    <span @if(app()->getLocale() == 'en') class="float-md-start d-block d-md-inline-block mt-25" @else class="float-md-end d-block d-md-inline-block mt-25" @endif >{{_t('dashboard.COPYRIGHT')}} &copy;
      <script>document.write(new Date().getFullYear())</script><a class="ms-25" href="https://www.linkedin.com/in/mohamad-al-moazen/" target="_blank">{{_t('dashboard.Mohamad Al Moazen')}}</a>,
      <span class="d-none d-sm-inline-block">{{_t('dashboard.All rights reserved')}}</span>
    </span>
  </p>
</footer>
<!-- END: Footer-->
