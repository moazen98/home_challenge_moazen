@extends('layouts/contentLayoutMaster')

@section('title', _t('dashboard.Show employee'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')

    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">
@endsection

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ _t('dashboard.General info') }}</h3>
                            <div class="float-right">

                                <div class="row">
                                    @permission('update_section')
                                    <div class="col-5">
                                        <a type="button" class="btn btn-primary"
                                           href="{{ route('section.edit',$data['id']) }}"><i
                                                class="fa fa-edit"></i></a>
                                    </div>
                                    @endpermission

                                        @permission('delete_section')
                                        <div class="col-5">
                                            <form method="post" id="myForm"
                                                  action="{{route('section.destroy',$data['id'])}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="button" class="btn btn-danger delete"
                                                        title="{{_t('dashboard.Delete')}}"
                                                ><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>
                                        @endpermission

                                </div>
                            </div>

                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <div class="user-info text-center">
                                    <h4>{{$data['name']}}</h4>
                                    <span
                                        class="badge rounded-pill badge-light-{{$data['is_active_class']}}">{{$data['is_active_string']}}</span>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card-body">

                            <div class="row mb-2">
                                <div class="col-6">
                                    <h4>{{ _t('dashboard.Name') }}:</h4>
                                    <p> {{ $data['name'] }} </p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Status') }}:</h4>
                                    <span
                                        class="badge rounded-pill badge-light-{{$data['is_active_class']}}">{{$data['is_active_string']}}</span>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-6">
                                    <h4>{{ _t('dashboard.Date') }}:</h4>
                                    <p> {{ $data['date'] }} </p>
                                </div>

                            </div>

                            <div class="row mb-2">
                                <div class="col-12">
                                    <h4>{{ _t('dashboard.Description') }}:</h4>
                                    <p> {{ $data['description'] }} </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="col-12">
                                    <h4 class="mt-2 pt-50">{{_t('dashboard.Section employees')}}</h4>
                                </div>
                            </div>
                            <!-- Permission table -->
                            <div class="card-datatable overflow-auto">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>{{_t('dashboard.User name')}}</th>
                                        <th>{{_t('dashboard.Email')}}</th>
                                        <th>{{_t('dashboard.Role')}}</th>
                                        <th>{{_t('dashboard.Status')}}</th>
                                    </tr>
                                    <tbody>
                                    @if($data['employees_count'] > 0)
                                        @foreach ($data['employees']['users'] as $index => $employee)
                                            <tr class="{{getTrTableClass($employee['is_active'])}}">
                                                <td>{{$employee['full_name']}}</td>
                                                <td>{{$employee['email']}}</td>
                                                <td>{{$employee['role']}}</td>
                                                <td>
                                                                   <span
                                                                       class="badge rounded-pill badge-light-{{$employee['is_active_class']}}">{{$employee['is_active_string']}}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        @include('admin.include.no_data')
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- Permission table -->
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>

@endsection

@include('admin.user.section.include.scripts')
