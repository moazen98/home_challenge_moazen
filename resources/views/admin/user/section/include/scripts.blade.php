<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {
                "ar[description]": {
                    required: function()
                    {
                        CKEDITOR.instances['description_0'].updateElement();
                    },
                },
                "en[description]": {
                    required: function()
                    {
                        CKEDITOR.instances['description_1'].updateElement();
                    },
                },

                "en[name]": {
                    required: function()
                    {
                        CKEDITOR.instances['title_0'].updateElement();
                    },
                },
                "ar[name]": {
                    required: function()
                    {
                        CKEDITOR.instances['title_1'].updateElement();
                    },
                },
                order: {
                    number: true
                },
            },
            messages: {
                "ar[name]": {
                    required: "{{_t('validation.The field is required')}}",
                },
                "en[name]": {
                    required: "{{_t('validation.The field is required')}}",
                },
                "ar[description]": {
                    required: "{{_t('validation.The field is required')}}",
                },
                "en[description]": {
                    required: "{{_t('validation.The field is required')}}",
                },
            }
        });
    });
</script>

