<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {

            },
            messages: {
                "ar[name]": {
                    required: "{{_t('validation.The field is required')}}",
                },
                "en[name]": {
                    required: "{{_t('validation.The field is required')}}",
                } ,
                type: {
                    required: "{{_t('validation.The field is required')}}",
                }
            }
        });
    });
</script>

