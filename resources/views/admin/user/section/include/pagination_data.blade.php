@if(count($data) > 0)
    @foreach ($data as $index => $section)
        <tr class="{{getTrTableClass($section['is_active'])}}">
            <td>{{$section['id']}}</td>
            <td>{{$section['name']}}</td>
            <td>{{$section['employees_count']}}</td>
            <td>
                <span
                    class="badge rounded-pill badge-light-{{$section['is_active_class']}}">{{$section['is_active_string']}}</span>
            </td>
            <td>{{$section['date']}}</td>
            <td>
                <div class="dropdown">
                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0"
                            data-bs-toggle="dropdown">
                        <i data-feather="more-vertical"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        @permission('update_section')
                        <a class="dropdown-item"
                           href="{{route('section.edit',$section['id'])}}">
                            <i data-feather="edit-2" class="me-50"></i>
                            <span>{{_t('dashboard.Edit')}}</span>
                        </a>
                        @endpermission


                        @permission('read_section')
                        <a class="dropdown-item"
                           href="{{route('section.show',$section['id'])}}">
                            <i data-feather="eye" class="me-50"></i>
                            <span>{{_t('dashboard.Show')}}</span>
                        </a>
                        @endpermission

                        @permission('delete_section')
                        <div class="col-4">
                            <form method="post" id="myForm"
                                  action="{{route('section.destroy',$section['id'])}}">
                                @csrf
                                @method('DELETE')
                                <button class="btn delete dropdown-item"><i data-feather="trash"
                                                                            class="me-50"></i>
                                    <span>{{_t('dashboard.Delete')}}</span></button>
                            </form>
                        </div>
                        @endpermission

                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    <td colspan="3">
        {!! $data->appends(request()->query())->links("pagination::bootstrap-4") !!}
    </td>

@else
    @include('admin.include.no_data')
@endif


<!-- this script is important for every paginate component to show the feather spans when filter -->
<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>
