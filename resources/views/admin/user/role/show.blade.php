@extends('layouts/contentLayoutMaster')

@section('title', _t('dashboard.Show role'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection

@include('admin.user.role.include.styles')

@section('content')

    <div class="row">


        <section id="basic-horizontal-layouts">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{_t('dashboard.Edit role in the system')}}</h4>
                    </div>
                    <div class="alert-body">
                        <!-- Vertical Wizard -->
                        <div class="card-body">
                            @csrf
                            @php
                                $models = config('laratrust_seeder.role_structure.super_admin');
                                $maps = config('laratrust_seeder.permissions_map');
                            @endphp

                            <div class="modal-body px-5 pb-5">
                                <div class="text-center mb-4">
                                    <h1 class="role-title">{{_t('dashboard.Show Role')}}</h1>
                                    <p>{{_t('dashboard.Show role permissions')}}</p>
                                </div>
                                <!-- Add role form -->
                                <div class="row">
                                    <div class="col-6">
                                        <label class="form-label" for="modalRoleName">{{_t('dashboard.Role name arabic')}}</label>
                                        <input
                                            type="text"
                                            id="modalRoleName"
                                            name="name_ar"
                                            class="form-control"
                                            placeholder="{{_t('dashboard.Enter role name in arabic')}}"
                                            tabindex="-1"
                                            data-msg="Please enter role name"
                                            value="{{$data['name_ar']}}"
                                            readonly
                                            disabled
                                        />
                                        @error('name_ar')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="form-label"
                                               for="modalRoleName">{{_t('dashboard.Role name english')}}</label>
                                        <input
                                            type="text"
                                            id="modalRoleName"
                                            name="name"
                                            class="form-control"
                                            placeholder="{{_t('dashboard.Enter role name in english')}}"
                                            tabindex="-1"
                                            data-msg="Please enter role name"
                                            value="{{$data['name_en']}}"
                                            readonly
                                            disabled
                                        />
                                        @error('name')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-6">
                                        <label class="form-label"
                                               for="modalRoleName">{{_t('dashboard.Role description arabic')}}</label>
                                        <input
                                            type="text"
                                            id="modalRoleName"
                                            name="description_ar"
                                            class="form-control"
                                            placeholder="{{_t('dashboard.Enter role description in arabic')}}"
                                            tabindex="-1"
                                            data-msg="Please enter role name"
                                            value="{{$data['description_ar']}}"
                                            readonly
                                            disabled
                                        />
                                        @error('description_ar')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="form-label"
                                               for="modalRoleName">{{_t('dashboard.Role description english')}}</label>
                                        <input
                                            type="text"
                                            id="modalRoleName"
                                            name="description"
                                            class="form-control"
                                            placeholder="{{_t('dashboard.Enter role description in english')}}"
                                            tabindex="-1"
                                            data-msg="Please enter role name"
                                            value="{{$data['description_en']}}"
                                            readonly
                                            disabled
                                        />
                                        @error('description')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12">
                                    <h4 class="mt-2 pt-50">{{_t('dashboard.Role permissions')}}</h4>
                                    <!-- Permission table -->
                                    <div class="table-responsive">
                                        <table class="table table-flush-spacing">
                                            <tbody>
                                            @foreach ($models as $index=>$model)

                                                @php
                                                    $res = str_replace( array('_'), ' ', $index);
                                                @endphp

                                                <tr>
                                                    <td class="text-nowrap fw-bolder">{{_t('dashboard.'.$res)}}</td>
                                                    <td>
                                                        @foreach ($maps as $index2=> $map)
                                                            <div class="d-flex">
                                                                <div class="form-check me-3 me-lg-5">
                                                                    <input class="form-check-input" type="checkbox"
                                                                           name="permissions[]"
                                                                           {{ $data['role']->hasPermission($map . '_' . $index) ? 'checked' : '' }}
                                                                           id="userManagementRead-{{$index}}-{{$index2}}"
                                                                           value="{{ $map . '_' . $index }}"
                                                                           readonly
                                                                           disabled
                                                                    />
                                                                    <label class="form-check-label"
                                                                           for="userManagementRead-{{$index}}-{{$index2}}">
                                                                        {{_t('dashboard.'.$map)}} </label>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                    </td>
                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Permission table -->
                                </div>
                                <!--/ Add role form -->
                            </div>
                    </form>
                    </div>
                    <!-- /Vertical Wizard -->
                </div>
            </div>
    </div>
    </section>
@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>

    @include('admin.user.role.include.scripts')

@endsection

