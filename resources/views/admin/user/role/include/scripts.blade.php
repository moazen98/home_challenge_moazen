<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {
                name_ar: "required",
                name: "required",
                description_ar: "required",
                description: "required",
            },
            messages: {
                name_ar: {
                    required: "{{_t('validation.The field is required')}}",
                },
                name: {
                    required: "{{_t('validation.The field is required')}}",
                },
                description_ar: {
                    required: "{{_t('validation.The field is required')}}",
                },
                description: {
                    required: "{{_t('validation.The field is required')}}",
                },
            }
        });
    });
</script>
