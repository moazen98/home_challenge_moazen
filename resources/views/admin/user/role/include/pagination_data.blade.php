@if(count($data) > 0)
    @foreach ($data as $index => $role)
        <tr>
            <td>{{$role['id']}}</td>
            <td>{{$role['name']}}</td>
            <td>{{$role['employees_number']}}</td>
            <td>
               <span
                   class="badge rounded-pill badge-light-{{$role['is_main_class']}}">{{$role['is_main_string']}}</span>
            </td>
            <td>
                <div class="dropdown">
                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0"
                            data-bs-toggle="dropdown">
                        <i data-feather="more-vertical"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        @permission('update_role')
                        <a class="dropdown-item" href="{{route('role.edit',$role['id'])}}">
                            <i data-feather="edit-2" class="me-50"></i>
                            <span>{{_t('dashboard.Edit')}}</span>
                        </a>
                        @endpermission


                        @permission('read_role')
                        <a class="dropdown-item" href="{{route('role.show',$role['id'])}}">
                            <i data-feather="eye" class="me-50"></i>
                            <span>{{_t('dashboard.Show')}}</span>
                        </a>
                        @endpermission


                        @if(!$role['is_main'])
                            @permission('delete_role')
                            <div class="col-4">
                                <form method="post" id="myForm"
                                      action="{{route('role.destroy',$role['id'])}}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn delete dropdown-item"><i data-feather="trash"
                                                                                class="me-50"></i>
                                        <span>{{_t('dashboard.Delete')}}</span></button>
                                </form>
                            </div>
                            @endpermission
                        @endif

                    </div>
                </div>
            </td>

        </tr>
    @endforeach
    <td colspan="3">
        {!! $data->appends(request()->query())->links("pagination::bootstrap-4") !!}
    </td>

@else
    @include('admin.include.no_data')
@endif


<!-- this script is important for every paginate component to show the feather spans when filter -->
<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>
