<script>
    $(document).ready(function () {

        let timer;

        function fetch_data(page, sort_type, column_name, query, page_counter,status,role) {
            $.ajax({
                url: "{{ route('employee.fetchData') }}",
                data: {
                    page: page,
                    sorttype: sort_type,
                    query: query,
                    page_counter: page_counter,
                    status: status,
                    role: role,
                },
                success: function (data) {
                    $('.user_table_body').html('');
                    $('.user_table_body').html(data);
                }
            })
        }


        $(document).on('keyup', '#search', function () {

            clearTimeout(timer);
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var page = $('#hidden_page').val();
            var status = $('#STATUS').val();
            var role = $('#ROLE').val();
            timer = setTimeout(function () {
                fetch_data(page, sort_type, column_name, query, page_counter,status,role);
            }, time_filter);
        });


        $("#page_counter,#hidden_sort_type,#STATUS,#ROLE").change(function () {

            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var page = $('#hidden_page').val();
            var status = $('#STATUS').val();
            var role = $('#ROLE').val();
            fetch_data(page, sort_type, column_name, query, page_counter,status,role);
        });


        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            $('#hidden_page').val(page);
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var query = $('#search').val();
            var page_counter = $('#page_counter').val();

            $('li').removeClass('active');
            $(this).parent().addClass('active');
            fetch_data(page, sort_type, column_name, query, page_counter);
        });
    });
</script>


<script>
    function item_id_edit(elem) {

        $('#user_id').text(elem.id);
        $('#user_name').text(elem.full_name);
        $('#details_of').text("{{_t('dashboard.Details of')}} "+ elem.full_name);
        $('#user_job').text(elem.job_title);
        $('#user_image').attr('src',elem.image_path);
        $('#user_image').attr('alt',elem.full_name);
        $('#user_email').text(elem.email);
        $('#user_birthdate').text(elem.birthdate);
        $('#user_status').text(elem.is_active_string);
        $('#user_status').addClass('badge-light-'+elem.is_active_class);
        $("#user_edit_href").attr("href",'/admin/users/employee/edit/'+elem.id);
        $("#user_show_href").attr("href",'/admin/users/employee/show/'+elem.id);
        $("#user_delete_form").attr("action",'/admin/users/employee/delete/'+elem.id);

        if(elem.basic_user){
            $("#user_delete_form").hide();
        }
    }
</script>
