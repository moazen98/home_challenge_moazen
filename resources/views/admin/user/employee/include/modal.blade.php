<div
    class="modal fade text-start"
    id="inlineForm"
    tabindex="-1"
    aria-labelledby="myModalLabel33"
    aria-hidden="true"
>
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title" id="details_of"></h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <tbody>
                    <tr data-dt-row="99" data-dt-column="2">
                        <td>{{_t('dashboard.id')}}:</td>
                        <td id="user_id"></td>
                    </tr>
                    <tr data-dt-row="99" data-dt-column="3">
                        <td>{{_t('dashboard.Name')}}:</td>
                        <td>
                            <div class="d-flex justify-content-start align-items-center user-name">
                                <div class="avatar-group">
                                    <div
                                        data-bs-toggle="tooltip"
                                        data-popup="tooltip-custom"
                                        data-bs-placement="top"
                                        class="avatar pull-up my-0"
                                    >
                                        <img
                                            id="user_image"
                                            src=""
                                            alt=""
                                            height="26"
                                            width="26"
                                        />
                                    </div>

                                </div>
                                <div class="d-flex flex-column"><span id="user_name"
                                                                      class="emp_name text-truncate"></span><small
                                        class="emp_post text-truncate text-muted" id="user_job"></small></div>
                            </div>
                        </td>
                    </tr>
                    <tr data-dt-row="99" data-dt-column="4">
                        <td>{{_t('dashboard.Email')}}:</td>
                        <td id="user_email"></td>
                    </tr>
                    <tr data-dt-row="99" data-dt-column="5">
                        <td>{{_t('dashboard.Date')}}:</td>
                        <td id="user_birthdate"></td>
                    </tr>
                    <tr data-dt-row="99" data-dt-column="7">
                        <td>{{_t('dashboard.Status')}}:</td>
                        <td><span class="badge rounded-pill " id="user_status"></span></td>
                    </tr>
                    <tr data-dt-row="99" data-dt-column="8">
                        <td>{{_t('dashboard.Actions')}}:</td>
                        <td>
                            <div class="dropdown">
                                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0"
                                        data-bs-toggle="dropdown" style="border: none !important; background: none !important;">
                                    <i data-feather="more-vertical"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-end">

                                    @permission('update_employee')
                                    <a class="dropdown-item" href="" id="user_edit_href">
                                        <i data-feather="edit-2" class="me-50"></i>
                                        <span>{{_t('dashboard.Edit')}}</span>
                                    </a>
                                    @endpermission

                                    @permission('read_employee')
                                    <a class="dropdown-item" href="" id="user_show_href">
                                        <i data-feather="eye" class="me-50"></i>
                                        <span>{{_t('dashboard.Show')}}</span>
                                    </a>
                                    @endpermission


                                    @permission('delete_employee')
                                    <form method="post"
                                          action="" id="user_delete_form">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn delete delete-style dropdown-item"><i data-feather="trash"
                                                                                                 class="me-50"></i>
                                            <span>{{_t('dashboard.Delete')}}</span></button>
                                    </form>
                                    @endpermission

                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
