<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                section: "required",
                email: {
                    email : true,
                    required: true
                },
                password : {
                    minlength : 6,
                    required : true,
                },
                confirm_password : {
                    required : true,
                    minlength : 6,
                    equalTo : "#password"
                },
                role: "required",
                country: "required",
                city: "required",
                phone_number: {
                    number: true
                },
            },
            messages: {
                first_name: {
                    required: "{{_t('validation.The field is required')}}",
                },
                last_name: {
                    required: "{{_t('validation.The field is required')}}",
                },
                section: {
                    required: "{{_t('validation.The field is required')}}",
                },
                email: {
                    required: "{{_t('validation.The field is required')}}",
                    email: "{{_t('validation.The field must be an email')}}",
                },
                confirm_password: {
                    minlength: "{{_t('validation.Password field must be greater than 6 character')}}",
                    required: "{{_t('validation.The field is required')}}",
                    equalTo: "{{_t('validation.The field not matching')}}",
                },
                password: {
                    minlength: "{{_t('validation.Password field must be greater than 6 character')}}",
                    required: "{{_t('validation.The field is required')}}",
                },
                role: {
                    required: "{{_t('validation.The field is required')}}",
                },
                country: {
                    required: "{{_t('validation.The field is required')}}",
                },
                city: {
                    required: "{{_t('validation.The field is required')}}",
                },
                phone_number: {
                    number: "{{_t('validation.The field must be a number')}}",
                },
            }
        });
    });
</script>



<script>
    $(document).ready(function () {

        $("#regForm2").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                section: "required",
                email: {
                    email : true,
                    required: true
                },
                role: "required",
                country: "required",
                city: "required",
                phone_number: {
                    required : true,
                    number: true
                },
            },
            messages: {
                first_name: {
                    required: "{{trans('validation.required')}}",
                },
                last_name: {
                    required: "{{trans('validation.required')}}",
                },
                section: {
                    required: "{{trans('validation.required')}}",
                },
                email: {
                    required: "{{trans('validation.required')}}",
                    email: "{{trans('validation.email')}}",
                },
                role: {
                    required: "{{trans('validation.required')}}",
                },
                country: {
                    required: "{{trans('validation.required')}}",
                },
                city: {
                    required: "{{trans('validation.required')}}",
                },
                phone_number: {
                    required: "{{trans('validation.required')}}",
                    number: "{{trans('validation.numeric')}}",
                },
            }
        });
    });
</script>
