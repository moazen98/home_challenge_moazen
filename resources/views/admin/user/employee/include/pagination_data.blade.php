@if(count($data) > 0)
    @foreach ($data as $index => $employee)
        <tr class="{{getTrTableClass($employee['is_active'])}}">
            <td class="control" tabindex="0" style="">
                <a class="btn" href="javascript:;" onclick="item_id_edit({{ json_encode($employee) }})" data-bs-toggle="modal"
                   data-bs-target="#inlineForm" ><i data-feather="feather" class="text-primary"></i></a>
            </td>
            <td>{{$employee['full_name']}}</td>
            <td>{{$employee['email']}}</td>

            <td>{{$employee['role']}}</td>

            <td>
                <span
                    class="badge rounded-pill badge-light-{{$employee['is_active_class']}}">{{$employee['is_active_string']}}</span>
            </td>

        </tr>
    @endforeach
    <td colspan="3">
        {!! $data->appends(request()->query())->links("pagination::bootstrap-4") !!}
    </td>

@else
    @include('admin.include.no_data')
@endif

<!-- this script is important for every paginate component to show the feather spans when filter -->
<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>


