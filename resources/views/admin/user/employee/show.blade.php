@extends('layouts.contentLayoutMaster')

@section('title', _t('dashboard.Show employee'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <!-- vendor css files part2 -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">

@endsection

@section('page-style')
    {{-- Page Css files --}}

    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ _t('dashboard.General info') }}</h3>
                            <div class="float-right">

                                <div class="row">
                                    @permission('update_employee')
                                    <div class="col-5">
                                        <a type="button" class="btn btn-primary"
                                           href="{{ route('employee.edit',$employee['id']) }}"><i
                                                class="fa fa-edit"></i></a>
                                    </div>
                                    @endpermission

                                    @if(!$employee['basic_user'])
                                        @permission('delete_employee')
                                        <div class="col-5">
                                            <form method="post" id="myForm"
                                                  action="{{route('employee.destroy',$employee['id'])}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="button" class="btn btn-danger delete"
                                                        title="{{_t('dashboard.Delete')}}"
                                                ><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>
                                        @endpermission
                                    @endif

                                </div>

                            </div>


                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <img
                                    class="img-fluid rounded mt-3 mb-2"
                                    src="{{$employee['image_path']}}"
                                    height="110"
                                    width="110"
                                    alt="User avatar"
                                />
                                <div class="user-info text-center">
                                    <h4>{{$employee['first_name']}}  {{$employee['last_name']}}</h4>
                                    <span class="badge bg-light-info">{{$employee['job_title']}}</span>
                                    @if($employee['basic_user'])
                                        <span class="badge bg-light-warning">{{_t('dashboard.Main user')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card-body">


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.First name') }}:</h4>
                                    <p> {{ $employee['first_name'] }} </p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Last name') }}:</h4>
                                    <p> {{ $employee['last_name'] }} </p>
                                </div>
                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{_t('dashboard.Employee section')}}:</h4>
                                    <p> {{ $employee['section_name'] }}</p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Job title') }}:</h4>
                                    <p> {{ $employee['job_title'] }} </p>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Phone number') }}:</h4>
                                    <p> {{$employee['full_phone']}} </p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Email') }}:</h4>
                                    <p> {{ $employee['email'] }} </p>
                                </div>
                            </div>

                            {{-- Status & Classification --}}
                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Country') }}:</h4>
                                    <p>{{$employee['country']}}</p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.City') }}:</h4>
                                    <p>{{$employee['city']}}</p>
                                </div>


                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Address') }}:</h4>
                                    <p> {{ $employee['address'] }} </p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Employee role') }}:</h4>
                                    <p>{{$employee['role']}}</p>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Status') }}:</h4>
                                    <span
                                        class="badge rounded-pill badge-light-{{$employee['is_active_class']}}">{{$employee['is_active_string']}}</span>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-12">
                                    <h4>{{ _t('dashboard.Notes') }}:</h4>
                                    <p> {{ $employee['note'] }} </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    @php
                        $models = config('laratrust_seeder.role_structure.super_admin');
                        $maps = config('laratrust_seeder.permissions_map');
                    @endphp


                    <div class="content-wrapper container-xxl p-0">
                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="col-12">
                                                <h4 class="mt-2 pt-50">{{_t('dashboard.Role permissions')}}
                                                    : {{$employee['role']}} </h4>
                                            </div>
                                        </div>
                                        <!-- Permission table -->
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-flush-spacing">
                                                    <tbody>
                                                    @foreach ($models as $index =>$model)

                                                        @php
                                                            $res = str_replace( array('_'), ' ', $index);
                                                        @endphp
                                                        <tr>
                                                            <td class="text-nowrap fw-bolder">{{_t('dashboard.'.$res)}}</td>
                                                            <td>
                                                                @foreach ($maps as $map)
                                                                    <div class="d-flex">
                                                                        <div class="form-check me-3 me-lg-5">
                                                                            <input class="form-check-input"
                                                                                   type="checkbox"
                                                                                   name="permissions[]"
                                                                                   @if($employee['role_object'])
                                                                                   {{ $employee['role_object']->hasPermission($map . '_' . $index) ? 'checked' : '' }} @endif
                                                                                   id="userManagementRead"
                                                                                   value="{{ $map . '_' . $index }}"
                                                                                   readonly
                                                                                   disabled
                                                                            />
                                                                            <label class="form-check-label"
                                                                                   for="userManagementRead">
                                                                                {{_t('dashboard.'.$map)}} </label>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                            </td>
                                                        </tr>

                                                    @endforeach


                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- Permission table -->
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>


    @include('admin.partials.scripts')

@endsection
