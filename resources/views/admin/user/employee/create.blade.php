@extends('layouts.contentLayoutMaster')

@section('title', _t('dashboard.Add employee'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection

@section('content')


    <section id="basic-horizontal-layouts">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{_t('dashboard.Add employee to the system')}}</h4>
                </div>
                <div class="alert-body">
                    <!-- Vertical Wizard -->
                    <div class="card-body">
                        <form action="{{ route('employee.store') }}" method="post" enctype="multipart/form-data"
                              id="regForm">
                            @csrf

                            <div class="mb-1">
                                <div class="row col-12 mb-2">
                                    <div class="col-6">
                                        <label class="form-label"
                                               for="first_name">{{ _t('dashboard.First name') }} *</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="first_name"
                                            aria-label="Full name"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('first_name')}}" required
                                        />
                                        @error('first_name')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label" for="last_name">{{ _t('dashboard.Last name') }}
                                            *</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="last_name"
                                            aria-label="last_name"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('last_name')}}" required
                                        />
                                        @error('last_name')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row col-12 mb-2">
                                    <div class="col-6">
                                        <label for="section">{{_t('dashboard.Employee section')}} *</label>
                                        <select class="select2 form-select" name="section" required>
                                            <option selected
                                                    disabled>{{_t('dashboard.Please select section')}}</option>
                                            @foreach($sections['sections'] as $section)
                                                <option @if(old('section') == $section['id']) selected @endif
                                                value="{{$section['id']}}">{{$section['name']}}</option>
                                            @endforeach
                                        </select>
                                        @error('section')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label"
                                               for="job_title">{{ _t('dashboard.Job title') }}</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="job_title"
                                            aria-label="job_title"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('job_title')}}"
                                        />
                                        @error('job_title')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="row col-12 mb-2">
                                    <div class="col-6">
                                        <label class="form-label" for="gender">{{_t('dashboard.Gender')}}</label>
                                        <select class="select2 form-select" name="gender">
                                            <option selected disabled>{{_t('dashboard.Please select gender')}}</option>
                                            <option @if(old('gender') == \App\Enums\GenderType::MALE) selected
                                                    @endif
                                                    value="{{\App\Enums\GenderType::MALE}}">{{_t('dashboard.Male')}}</option>

                                            <option @if(old('gender') == \App\Enums\GenderType::FEMALE) selected
                                                    @endif
                                                    value="{{\App\Enums\GenderType::FEMALE}}">{{_t('dashboard.Female')}}</option>

                                            <option @if(old('gender') == \App\Enums\GenderType::OTHER) selected
                                                    @endif
                                                    value="{{\App\Enums\GenderType::OTHER}}">{{_t('dashboard.Other')}}</option>
                                        </select>
                                        @error('gender')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label"
                                               for="basic-addon-name">{{ _t('dashboard.Birthdate') }}</label>
                                        <input
                                            type="date"
                                            class="form-control"
                                            name="birthdate"
                                            aria-label="job_title"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('birthdate')}}"
                                        />
                                        @error('birthdate')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <input type="hidden" id="code" name="international_code" value="+971"/>
                                <div class="row col-12 mb-2">
                                    <div class="col-6">
                                        <label class="form-label" for="phone">{{ _t('dashboard.Phone') }}</label>
                                        <br>
                                        <input class="form-control" id="phone" name="phone_number" type="tel"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                               value="{{old('full')}}">
                                        @error('phone_number')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="form-label" for="basic-addon-name">{{ _t('dashboard.Email') }}
                                            *</label>
                                        <input
                                            type="email"
                                            class="form-control"
                                            name="email"
                                            aria-label="email"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('email')}}" required
                                        />
                                        @error('email')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row col-12 mb-2">
                                    <div class="col-6">
                                        <div class="d-flex justify-content-between">
                                            <label class="form-label" for="password">{{_t('dashboard.Password')}}
                                                *</label>
                                        </div>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            <input
                                                type="password"
                                                class="form-control form-control-merge"
                                                name="password" id="password"
                                                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                aria-describedby="login-password" required
                                            />
                                            <span class="input-group-text cursor-pointer"><i
                                                    data-feather="eye"></i></span>
                                        </div>
                                        @error('password')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <div class="d-flex justify-content-between">
                                            <label class="form-label"
                                                   for="confirm_password">{{_t('dashboard.Confirm password')}} *</label>
                                        </div>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            <input
                                                type="password"
                                                class="form-control form-control-merge"
                                                name="confirm_password"
                                                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                aria-describedby="login-password" required
                                            />
                                            <span class="input-group-text cursor-pointer"><i
                                                    data-feather="eye"></i></span>
                                        </div>
                                        @error('confirm_password')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row col-12 mb-2">
                                    <div class="col-md-6">
                                        <label class="form-label" for="country">{{_t('dashboard.Country')}}</label>
                                        <select id="COUNTRY_CATEGORY" class="select2 form-control" name="country"
                                                required
                                        >
                                            <option disabled selected>{{_t('dashboard.Select country')}}</option>
                                            @foreach($countries['countries'] as $country)
                                                <option value="{{$country['id']}}"
                                                        @if(old('country') == $country['id']) selected @endif
                                                        name="COUNTRY_ID">{{$country['name']}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('country'))
                                            <span
                                                class="text-danger">{{ $errors->first('country') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-6">
                                        <label class="form-label" for="city">{{_t('dashboard.City')}}</label>

                                        <select id="CITY" class="select2 form-control" name="city">
                                            <option @if(old('city') != null) value="{{old('city')}}" selected
                                                    @else disabled
                                                    selected @endif >@if(old('city') != null) @if(app()->getLocale() == 'ar'){{\App\Models\City::find(old('city'))->name_ar}} @else {{\App\Models\City::find(old('city'))->name_en}}  @endif @else{{_t('dashboard.Please select city')}}@endif
                                            </option>
                                        </select>
                                        @if($errors->has('city'))
                                            <span
                                                class="text-danger">{{ $errors->first('city') }}</span>
                                        @endif
                                    </div>

                                </div>

                                <div class="row col-12 mb-2">
                                    <div class="col-12">
                                        <label class="form-label"
                                               for="address">{{ _t('dashboard.Address') }}</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="address"
                                            aria-label="address"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('address')}}"
                                        />
                                        @error('address')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="row col-12 mb-2">
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="note">{{_t('dashboard.Notes')}}</label>
                                            <textarea
                                                class="form-control"
                                                rows="3"
                                                name="note"
                                                placeholder="{{_t('dashboard.Insert Note')}}"
                                            >{{old('note')}}</textarea>
                                        </div>
                                        @error('note')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row col-12 mb-2">

                                    <div class="col-6">

                                        <label for="section" class="form-label">{{_t('dashboard.Employee role')}}
                                            *</label>
                                        <select class="select2 form-select" name="role" required>
                                            <option selected disabled>{{_t('dashboard.Please select role')}}</option>
                                            @foreach($roles['roles'] as $role)
                                                <option @if(old('role') == $role['id']) selected @endif
                                                value="{{$role['id']}}">{{$role['name']}}</option>
                                            @endforeach
                                        </select>
                                        @error('role')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <!-- remove thumbnail file upload starts -->
                                        <label class="form-label"
                                               for="image">{{ _t('dashboard.Employee image') }}</label>
                                        <div class="card-body">
                                            <input class="form-control" id="uploadInputImage" type="file" name="files[]"
                                                   accept=".jpg,.png,.jpeg"/>
                                            @error('image')
                                            <span class="text-danger">{{$message }}</span>
                                            @enderror
                                            <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div style="margin-left: 3%;" class="mb-2 form-check form-switch">
                                            <input type="checkbox" class="form-check-input" name="is_active"
                                                   id="customSwitch1" @if(old('is_active') == 'on') checked @endif/>
                                            <label class="form-check-label"
                                                   for="customSwitch1">{{ _t('dashboard.Activate') }}</label>
                                        </div>
                                    </div>

                                </div>


                            </div>
                            @permission('create_employee')
                            <button type="submit" class="btn btn-primary">{{ _t('dashboard.Submit') }}</button>
                            @endpermission
                        </form>
                    </div>
                    <!-- /Vertical Wizard -->
                </div>
            </div>
        </div>
    </section>

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>

    @include('admin.user.employee.include.crud_script')

    @include('admin.partials.scripts')

@endsection


@endsection


