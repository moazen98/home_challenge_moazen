@if(count($blogs) > 0)
    @foreach ($blogs as $index => $blog)
        <tr>
            <td>{{$blog['id']}}</td>
            <td>{{$blog['title']}}</td>

            <td>
               <span
                   class="badge rounded-pill badge-light-success">{{$blog['author']}}</span>
            </td>

            <td>
               <span
                    class="badge rounded-pill badge-light-info">{{$blog['category_name']}}</span>
            </td>

            <td>
               <span
                    class="badge rounded-pill badge-light-primary">{{$blog['source_name']}}</span>
            </td>
            <td>{{$blog['published_at_format']}}</td>
            <td>
                <div class="dropdown">
                    <button type="button"
                            class="btn btn-sm dropdown-toggle hide-arrow py-0"
                            data-bs-toggle="dropdown">
                        <i data-feather="more-vertical"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">

                        @permission('read_blog')
                        <a class="dropdown-item" href="{{route('social.blog.ai.show',$blog['id'])}}">
                            <i data-feather="eye" class="me-50"></i>
                            <span>{{_t('dashboard.Show')}}</span>
                        </a>
                        @endpermission

                        @permission('delete_blog')
                        <div class="col-4">
                            <form method="post" id="myForm"
                                  action="{{route('social.blog.ai.destroy',$blog['id'])}}">
                                @csrf
                                @method('DELETE')
                                <button
                                    style="display: inline-flex;width: 137px;"
                                    class="btn delete dropdown-item"><i
                                        data-feather="trash" class="me-50"></i>
                                    <span>{{_t('dashboard.Delete')}}</span></button>
                            </form>
                        </div>
                        @endpermission
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    {{--<tr>--}}
    <td colspan="3">
        {!! $blogs->appends(request()->query())->links("pagination::bootstrap-4") !!}
    </td>
    {{--</tr>--}}
@else
    @include('admin.include.no_data')
@endif

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

