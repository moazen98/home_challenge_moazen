@extends('layouts.contentLayoutMaster')

@section('title', _t('dashboard.Show News AI blog'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <!-- vendor css files part2 -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Roboto+Slab&family=Slabo+27px&family=Sofia&family=Ubuntu+Mono&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">

@endsection

@section('page-style')
    {{-- Page Css files --}}


    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ _t('dashboard.General Info') }}</h3>
                            <div class="float-right">

                                <div class="row">

                                    @permission('delete_blog')
                                    <div class="col-5">
                                        <form method="post" id="myForm"
                                              action="{{route('social.blog.ai.destroy',$data['id'])}}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="button" class="btn btn-danger delete"
                                                    title="{{_t('dashboard.Delete')}}"
                                            ><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>
                                    @endpermission

                                </div>

                            </div>


                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                @if($data['image_url'])
                                    <img
                                        class="img-fluid rounded mt-3 mb-2"
                                        src="{{$data['image_url']}}"
                                        height="110"
                                        width="110"
                                        alt="User avatar"
                                    />
                                @endif

                            </div>
                        </div>

                        <br>

                        <div class="card-body">


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.ID') }}:</h4>
                                    <p> {{ $data['id'] }} </p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Order') }}:</h4>
                                    <p> {{ $data['order'] }} </p>
                                </div>
                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{_t('dashboard.Type')}}:</h4>
                                    <p> {{ $data['type_string'] }}</p>
                                </div>

                                @if($data['source_name'])
                                    <div class="col-md-12 col-12">
                                        <h4>{{ _t('dashboard.Source') }}:</h4>
                                        <p> {!! $data['source_name'] !!} </p>
                                    </div>
                                @endif


                            </div>

                            @if($data['title'])
                                <div class="row mb-2">
                                    <div class="col-md-12 col-12">
                                        <h4>{{ _t('dashboard.Title') }}:</h4>
                                        <p> {{$data['title'] }} </p>
                                    </div>
                                </div>
                            @endif

                            @if($data['author'])
                                <div class="row mb-2">
                                    <div class="col-md-12 col-12">
                                        <h4>{{ _t('dashboard.Author') }}:</h4>
                                        <p> {{$data['author'] }} </p>
                                    </div>
                                </div>
                            @endif

                            @if($data['description'])
                                <div class="row mb-2">
                                    <div class="col-md-12 col-12">
                                        <h4>{{ _t('dashboard.Description') }}:</h4>
                                        <p> {!! $data['description'] !!} </p>
                                    </div>
                                </div>
                            @endif

                            @if($data['content'])
                                <div class="row mb-2">
                                    <div class="col-md-12 col-12">
                                        <h4>{{ _t('dashboard.Content') }}:</h4>
                                        <p> {!! $data['content'] !!} </p>
                                    </div>
                                </div>
                            @endif
                            @if($data['url'])
                                <div class="row mb-2">
                                    <div class="col-md-12 col-12">
                                        <h4>{{ _t('dashboard.Url') }}:</h4>
                                        <p> {!! $data['url'] !!} </p>
                                    </div>
                                </div>
                            @endif

                            @if($data['published_at_format'])
                                <div class="row mb-2">
                                    <div class="col-md-12 col-12">
                                        <h4>{{ _t('dashboard.Publish date') }}:</h4>
                                        <p> {!! $data['published_at_format'] !!} </p>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>


    @include('admin.partials.scripts')

@endsection
