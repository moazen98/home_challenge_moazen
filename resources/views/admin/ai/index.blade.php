@extends('layouts/contentLayoutMaster')

@section('title', _t('dashboard.News AI Blogs'))


@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')




    <div class="row">

        <x-search-filter-component :col="5" :message="_t('dashboard.Search by Title, Description, Author, Content, ID')"/>


        @permission('update_blog')
        <div class="col-2">
            <a href="{{route('social.blog.ai.updateBlogData')}}">
                <button type="button"
                        class="btn btn-flat-primary">{{ _t('dashboard.Update blogs') }}
                    <i class="fa fa-refresh"></i>
                </button>
            </a>
        </div>
        @endpermission

        @permission('update_blog')
        <div class="col-2">
            <a href="{{route('social.blog.ai.reorder')}}">
                <button type="button"
                        class="btn btn-flat-info">{{ _t('dashboard.Reorder blogs') }}
                    <i class="fa fa-edit"></i>
                </button>
            </a>
        </div>
        @endpermission


    </div>


    <section id="ajax-datatable">


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{_t('dashboard.Blogs News AI info')}}</h4>


                        <div class="row">

                            @permission('read_blog')
                            <div class="col-4">
                                <a href="{{route('social.blog.ai.export')}}">
                                    <button type="button"
                                            class="btn btn-success">{{ _t('dashboard.Export') }}
                                        <i class="fa fa-file-excel-o"></i>
                                    </button>
                                </a>
                            </div>
                            @endpermission


                            <x-filter-other-component/>

                        </div>


                    </div>


                    <div class="card mt-2">
                        <div class="row"
                             style="margin-right: unset !important;margin-left: unset !important;">

                            <x-category-filter-component :col="4" :categories="$categories['categories']"/>

                            <x-source-filter-component :col="4" :sources="$sources['sources']"/>

                            <x-date-filter-component :col="4"/>

                        </div>
                    </div>

                    <div class="card-datatable overflow-auto">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>{{ _t('dashboard.ID') }}</th>
                                <th>{{ _t('dashboard.Title') }}</th>
                                <th>{{ _t('dashboard.Author') }}</th>
                                <th>{{ _t('dashboard.Category') }}</th>
                                <th>{{ _t('dashboard.Source') }}</th>
                                <th>{{ _t('dashboard.Date') }}</th>
                                <th>{{ _t('dashboard.Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @include('admin.ai.include.pagination_data')
                            </tbody>

                        </table>
                    </div>

                    <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
                    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id"/>
                    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc"/>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>

    @include('admin.partials.scripts')
    @include('admin.ai.include.filters')
    @include('admin.partials.alert')
@endsection
