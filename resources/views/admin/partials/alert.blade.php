<script>

    var toastMixin = Swal.mixin({
        toast: true,
        icon: 'success',
        title: 'General Title',
        animation: false,
        position: 'top-right',
        showConfirmButton: false,
        allowOutsideClick: true,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

</script>

@if(\Illuminate\Support\Facades\Session::has('success'))
    <script>

        toastMixin.fire({
            icon: 'success',
            animation: true,
            title: '{{Session::get('success')}}',
            allowOutsideClick: true,
            timer: 3000
        });

    </script>

    @php
        Session::forget('success');
    @endphp

@elseif(\Illuminate\Support\Facades\Session::has('failed'))

    <script>

        toastMixin.fire({
            title: '{{Session::get('failed')}}',
            icon: 'error',
            allowOutsideClick: true,
            timer: 3000
        });

    </script>
    @php
        Session::forget('failed');
    @endphp

@elseif(Session::has('info'))

    <script>

        toastMixin.fire({
            icon: 'info',
            animation: true,
            allowOutsideClick: true,
            title: '{{Session::get('info')}}',
            timer: 3000
        });

    </script>

    @php
        Session::forget('info');
    @endphp



@endif
