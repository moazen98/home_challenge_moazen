
<script src='https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'></script>
{{--<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>--}}
<script src="https://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>

<!-- TODO: This for filter search tables setTimeout  -->
<script>
    var time_filter = 1000;
</script>

<script>
    $(function () {
        $('input').on('change', function (event) {

            var $element = $(event.target);
            var $container = $element.closest('.example');

            if (!$element.data('tagsinput'))
                return;

            var val = $element.val();
            if (val === null)
                val = "null";
            var items = $element.tagsinput('items');

            $('code', $('pre.val', $container)).html(($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\""));
            $('code', $('pre.items', $container)).html(JSON.stringify($element.tagsinput('items')));


        }).trigger('change');
    });
</script>

<script>
    $(document).ready(function () {

        $(document).on('change', '#COUNTRY_CATEGORY', function () {

            var country_id = $("#COUNTRY_CATEGORY").val();
            $.ajax({
                url: "{{ route('area.cities') }}",
                type: "post",
                mode: "abort",
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    country_id: country_id,
                },
                success: function (data) {
                    var locate = "{!! config('app.locale') !!}";
                    if (locate == 'ar') {
                        jQuery('select[name="city"]').empty();
                        jQuery.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + data[key].id + '">' + data[key].name_ar + '</option>')
                        });
                    } else {
                        jQuery('select[name="city"]').empty();
                        jQuery.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + data[key].id + '">' + data[key].name_en + '</option>')
                        });
                    }

                }, error: function (data) {
                    console.log('error');
                }

            })
        })


    })
</script>

<script>
    $('document').ready(function () {
        $("#uploadInputImage").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>

<script>
    $('document').ready(function () {
        $("#uploadInputImage2").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview2').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>


<script>
    $('document').ready(function () {
        $("#uploadInputImage3").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview3').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>


<script>
    $('document').ready(function () {
        $("#uploadInputImage4").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview4').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>


<script>
    $('document').ready(function () {
        $("#uploadInputImage5").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview5').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>

<script>
    $('document').ready(function () {
        $("#uploadInputImage6").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview6').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>

<script>
    $('document').ready(function () {
        $("#uploadInputImage7").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview7').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>

<script>
    $(document).ready(function () {

        //delete
        $('.delete').click(function (e) {

            var that = $(this)

            e.preventDefault();

            var n = new swal({
                title: '{{_t('dashboard.Are you sure you want to delete this record?')}}',
                icon: "warning",
                position: 'top-end',
                // buttons: true,
                showCancelButton: true,
                showConfirmButton: true,
                dangerMode: true,
                type: "danger",
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
            }).then((willDelete) => {
                if (willDelete['isConfirmed'] == true) {
                    that.closest('form').submit();
                } else {
                    // n.close();
                }
            });

        });//end of delete
    });//end of ready

</script>

<script>
    $("#phone").intlTelInput({
        hiddenInput: "full",
        initialCountry: "ae",
        separateDialCode: true,
        formatOnDisplay: false,
        utilsScript: "{{asset('build/js/utils.js')}}",
    }).on("countrychange", function () {
        $("#code").val("+" + ($("#phone").intlTelInput("getSelectedCountryData").dialCode));
    });
</script>

<!-- TODO: it use for textarea validation  -->
<script>
    jQuery.validator.addMethod('ckrequired', function (value, element, params) {
        var idname = jQuery(element).attr('id');
        var messageLength =  jQuery.trim ( CKEDITOR.instances[idname].getData() );
        return !params  || messageLength.length !== 0;
    }, "Image field is required");
</script>



<script>

    CKEDITOR.replaceAll(function (textarea, config) {
        config.replaceClass = 'ckeditor_test'; //TODO: Need fix
        config.language = '{{app()->getLocale()}}';
        config.extraPlugins = 'font';
        config.filebrowserUploadUrl = "/plugins/ckeditor/upload.php";
    });

</script>


