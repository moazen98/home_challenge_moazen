<script>
    $(document).ready(function () {
        $(".dropdown-toggle").dropdown();
    });
</script>

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>


<script>
    $(document).ready(function () {
        $('#phone').keyup(function () {
            var numbers = $(this).val();
            $(this).val(numbers.replace(/\D/, ''));
        });
    });
</script>


<script>
    $(document).ready(function () {

        //delete
        $('.delete').click(function (e) {

            var that = $(this)

            e.preventDefault();

            var n = new swal({
                title: '{{_t('dashboard.Are you sure you want to delete this record?')}}',
                icon: "warning",
                // buttons: true,
                showCancelButton: true,
                showConfirmButton: true,
                dangerMode: true,
                type: "danger",
                closeOnConfirm: false,
                confirmButtonClass: "btn-danger",
            }).then((willDelete) => {
                if (willDelete['isConfirmed'] == true) {
                    that.closest('form').submit();
                } else {
                    // n.close();
                }
            });

        });//end of delete
    });//end of ready

</script>

<script>
    $(document).ready(function () {

        $(document).on('change', '#COUNTRY_CATEGORY', function () {

            var country_id = $("#COUNTRY_CATEGORY").val();
            $.ajax({
                url: "{{ route('area.cities') }}",
                type: "post",
                mode: "abort",
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    country_id: country_id,
                },
                success: function (data) {
                    var locate = "{!! config('app.locale') !!}";
                    if (locate == 'ar') {
                        jQuery('select[name="city"]').empty();
                        jQuery.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + data[key].id + '">' + data[key].name_ar + '</option>')
                        });
                    } else {
                        jQuery('select[name="city"]').empty();
                        jQuery.each(data, function (key, value) {
                            $('select[name="city"]').append('<option value="' + data[key].id + '">' + data[key].name_en + '</option>')
                        });
                    }

                }, error: function (data) {
                    console.log('error');
                }

            })
        })


    })

</script>


<script>
    $('document').ready(function () {
        $("#uploadInputImage").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadInputPreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
<script src="{{ asset('js/share.js') }}"></script>
