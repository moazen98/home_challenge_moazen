@extends('layouts/contentLayoutMaster')

@section('title', _t('dashboard.Translations'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

    @include('admin.translation.include.style')

@endsection

@section('content')



    <section id="basic-datatable">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form method="GET" action="{{route('translation.index')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="mb-1">
                                    <label class="form-label" for="lang_name">{{_t('dashboard.Choose language')}}</label>
                                    <select class="select2 form-select search" name="lang_name">
                                        <option value="{{NULL}}" selected>{{_t('dashboard.All')}}</option>
                                        @foreach($locales as $key => $locale)
                                            <option
                                                {{ isset( $_GET['lang_name']) && $_GET['lang_name'] == $key ? 'selected' : ''}} value="{{$key}}">{{$locale}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-lg-4">
                                <div class="mb-1">
                                    <label class="form-label" for="lang_file_name">{{_t('dashboard.Translation key')}}</label>
                                    <select class="select2 form-select" name="lang_file_name">
                                        <option value="{{NULL}}" selected>{{_t('dashboard.All')}}</option>
                                        @foreach($translationTypes as $key => $locale)
                                            <option value="{{$locale->type}}" @if(request()->lang_file_name == $locale->type) selected @endif>{{$locale->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name">{{_t('dashboard.Search')}}</label>
                                    <input class="form-control" name="key" type="text" value="{{$_GET['key'] ?? ''}}">
                                </div>
                            </div>


                        </div>


                        <div class="row">


                        </div>

                        <div class="text-left">
                            <button type="submit" class="btn btn-primary mt-2">{{_t('dashboard.Search')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-datatable overflow-auto">
                        <table class="category compact table">
                            <thead>
                            <tr>
                                <th>{{_t('dashboard.Action')}}</th>
                                <th>{{_t('dashboard.ID')}}</th>
                                <th>{{_t('dashboard.Key')}}</th>
                                <th>{{_t('dashboard.Title')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($translations as $translation)
                                <tr>

                                    <td>
                                        @permission('update_translation')
                                        <div class="d-inline-flex">
                                            <a href="{{route('translation.edit',$translation->id)}}"
                                               class="pe-1 item-edit">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-edit font-small-4">
                                                    <path
                                                        d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                    <path
                                                        d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                </svg>
                                            </a>

                                        </div>
                                        @endpermission
                                        @permission('delete_translation')
                                        <div class="d-inline-flex">
                                            <form method="post" id="myForm"
                                                  action="{{route('translation.destroy',$translation->id)}}">
                                                @csrf
                                                <a class="delete"
                                                >
                                                    <i
                                                        data-feather="trash" class="me-50"></i>
                                                </a>
                                            </form>
                                        </div>
                                        @endpermission

                                    </td>
                                    <td>{{$translation->id}}</td>
                                    <td>
                                    <span
                                        class="badge rounded-pill badge-light-primary"> {{ strtok($translation->key, '.')}}</span>

                                  </td>
                                    <td>{{$translation->value}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="mt-2" style="text-align: center;">
                        {!! $translations->appends(request()->query())->links("pagination::bootstrap-4") !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>

    @include('admin.partials.scripts')
    @include('admin.partials.alert')

@endsection
