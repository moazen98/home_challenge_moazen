<script>
    $(document).ready(function () {

        $("#regForm").validate({
            ignore: [],
            rules: {

            },
            messages: {

                order: {
                    required: "{{_t('validation.The field is required')}}",
                },
                language: {
                    required: "{{_t('validation.The field is required')}}",
                },
                date: {
                    required: "{{_t('validation.The field is required')}}",
                },
            }
        });
    });
</script>
