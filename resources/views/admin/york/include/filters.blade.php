<script>
    $(document).ready(function () {

        let timer;

        function fetch_data(page, sort_type, column_name, source, page_counter,category,query,publish_date) {
            $.ajax({
                url: "{{ route('social.blog.york.fetchData') }}",
                data: {
                    page: page,
                    sortby: sort_type,
                    query: query,
                    page_counter: page_counter,
                    source: source,
                    category: category,
                    publish_date: publish_date,
                },
                success: function (data) {
                    $('tbody').html('');
                    $('tbody').html(data);
                }
            })
        }


        $(document).on('keyup', '#search', function () {

            clearTimeout(timer);
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var source = $('#source').val();
            var category = $('#category').val();
            var page = $('#hidden_page').val();
            var publish_date = $('#publish_date').val();

            timer = setTimeout(function () {
                fetch_data(page, sort_type, column_name, source, page_counter,category,query,publish_date);
            }, 1000);
        });


        $("#category,#page_counter,#hidden_sort_type,#source,#publish_date").change(function () {

            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var source = $('#source').val();
            var category = $('#category').val();
            var page = $('#hidden_page').val();
            var publish_date = $('#publish_date').val();

            fetch_data(page, sort_type, column_name, source, page_counter,category,query,publish_date);
        });



        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            $('#hidden_page').val(page);
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page_counter = $('#page_counter').val();
            var source = $('#source').val();
            var category = $('#category').val();
            var publish_date = $('#publish_date').val();

            $('li').removeClass('active');
            $(this).parent().addClass('active');
            fetch_data(page, sort_type, column_name, source, page_counter,category,query,publish_date);
        });
    });
</script>
