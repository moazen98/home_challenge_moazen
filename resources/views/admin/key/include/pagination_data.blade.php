@if(count($data) > 0)
    @foreach ($data as $index => $key)
        <tr class="{{getTrTableClass($key['is_active'])}}">
            <td>{{$key['id']}}</td>
            <td>{{$key['secret_key']}}</td>

            <td>
                           <span
                            class="badge rounded-pill badge-light-{{$key['is_active_class']}}">{{$key['is_active_string']}}</span>
            </td>
            <td>
                           <span
                               class="badge rounded-pill badge-light-info">{{$key['type_string']}}</span>
            </td>

            <td>
                                                            <span
                                                                class="badge rounded-pill badge-light-warning">{{$key['date']}}</span>
            </td>

            <td>
                <div class="dropdown">
                    <button type="button"
                            class="btn btn-sm dropdown-toggle hide-arrow py-0"
                            data-bs-toggle="dropdown">
                        <i data-feather="more-vertical"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">

                        @permission('read_key')
                        <a class="dropdown-item" href="{{route('key.show',$key['id'])}}">
                            <i data-feather="eye" class="me-50"></i>
                            <span>{{_t('dashboard.Show')}}</span>
                        </a>
                        @endpermission

                        @permission('update_key')
                        <a class="dropdown-item"
                           href="{{route('key.edit',$key['id'])}}">
                            <i data-feather="edit-2" class="me-50"></i>
                            <span>{{_t('dashboard.Edit')}}</span>
                        </a>
                        @endpermission

                        @permission('delete_key')
                        <div class="col-4">
                            <form method="post" id="myForm"
                                  action="{{route('key.destroy',$key['id'])}}">
                                @csrf
                                @method('DELETE')


                                <button
                                    style="display: inline-flex;width: 137px;"
                                    class="btn delete dropdown-item"><i
                                        data-feather="trash" class="me-50"></i>
                                    <span>{{_t('dashboard.Delete')}}</span></button>
                            </form>
                        </div>
                        @endpermission
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    <td colspan="3">
        {!! $data->appends(request()->query())->links("pagination::bootstrap-4") !!}
    </td>
@else
    @include('admin.include.no_data')
@endif
<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

