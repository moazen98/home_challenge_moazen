
<style>
    select2-container--default {
        display: flex !important;
    }
</style>

<style>
    #map {
        height: 400px;
    }
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<link rel='stylesheet'
      href='https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>




<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<style>
    .form-switch .form-check-label .switch-text-left i, .form-switch .form-check-label .switch-text-left svg, .form-switch .form-check-label .switch-text-right i, .form-switch .form-check-label .switch-text-right svg, .form-switch .form-check-label .switch-icon-left i, .form-switch .form-check-label .switch-icon-left svg, .form-switch .form-check-label .switch-icon-right i, .form-switch .form-check-label .switch-icon-right svg {
        height: 13px;
        width: 13px;
        margin-top: 6px;
        font-size: 13px;
    }


    .input-box {
        display: flex;
        align-items: center;
        max-width: 300px;
        background: #fff;
        border: 1px solid #a0a0a0;
        border-radius: 4px;
        padding-left: 0.5rem;
        overflow: hidden;
        font-family: sans-serif;
    }

    .input-box .prefix {
        font-weight: 300;
        font-size: 14px;
        color: #999;
    }

    .input-box input {
        flex-grow: 1;
        font-size: 14px;
        background: #fff;
        border: none;
        outline: none;
        padding: 0.5rem;
    }

    .input-box:focus-within {
        border-color: #777;
    }

</style>


<style>
    .tag {
        padding: 5px;
        background: #05d8ff;
        border: deepskyblue 1px solid;
        border-radius: 5px;
    }

    #inputText {
        border: none;
        background: none;
    }

    #container {
        background: white;
        padding: 10px;
    }

</style>
<style type="text/css">

    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

</style>
