@extends('layouts/contentLayoutMaster')

@section('title', _t('dashboard.Public setting'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/maps/leaflet.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">

@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/maps/map-leaflet.css')) }}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

    @include('admin.setting.map-style')
@endsection

@include('admin.setting.style')

@section('content')


    <section id="basic-horizontal-layouts">

        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{_t('dashboard.Update setting in the system')}}</h4>
                </div>
                <!-- Vertical Wizard -->
                <div class="card-body">
                    <form action="{{ route('setting.update') }}" method="post" enctype="multipart/form-data"
                          id="regForm">
                        @csrf

                        <div class="mb-1">
                            <div class="row col-12 mb-2">
                                <div class="col-12">
                                    <label class="form-label" for="basic-addon-name">{{ _t('dashboard.English logo') }}
                                        *</label>
                                    <input class="form-control" type="file" name="en_logo"
                                           accept=".jpg,.png,.jpeg" id="uploadInputImage"/>
                                    @error('en_logo')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                    @if($common)
                                        <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview"
                                             @if($common['en_logo']) src="{{$common['en_logo']}}" @endif/>
                                    @else
                                        <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview"
                                        />
                                    @endif
                                </div>
                            </div>


                            <div class="row col-12 mb-2">
                                <div class="col-12">
                                    <label class="form-label" for="basic-addon-name">{{ _t('dashboard.Arabic logo') }}
                                        *</label>
                                    <input class="form-control" type="file" name="ar_logo"
                                           accept=".jpg,.png,.jpeg" id="uploadInputImage2"/>
                                    @error('ar_logo')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                    @if($common)
                                        <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview2"
                                             @if($common['ar_logo']) src="{{$common['ar_logo']}}" @endif/>
                                    @else
                                        <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview2"
                                        />
                                    @endif
                                </div>
                            </div>

                            <div class="row col-12 mb-2">
                                <div class="col-12">
                                    <label class="form-label" for="basic-addon-name">{{ _t('dashboard.English icon') }}
                                        *</label>
                                    <input class="form-control" id="uploadInputImage3" type="file" name="en_icon"
                                           accept=".jpg,.png,.jpeg"/>
                                    @error('en_icon')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                    @if($common)
                                        <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview3"
                                             @if($common['en_icon']) src="{{$common['en_icon']}}" @endif/>
                                    @else
                                        <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview3"
                                        />
                                    @endif
                                </div>
                            </div>


                            <div class="row col-12 mb-2">
                                <div class="col-12">
                                    <label class="form-label" for="basic-addon-name">{{ _t('dashboard.Arabic icon') }}
                                        *</label>
                                    <input class="form-control" id="uploadInputImage4" type="file" name="ar_icon"
                                           accept=".jpg,.png,.jpeg"/>
                                    @error('ar_icon')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                    @if($common)
                                        <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview4"
                                             @if($common['ar_icon']) src="{{$common['ar_icon']}}" @endif/>
                                    @else
                                        <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview4"
                                        />
                                    @endif
                                </div>
                            </div>

                            <div class="row col-12 mb-2">
                                @foreach (config('translatable.locales') as $locale)
                                    <div class="col-6">
                                        <label class="form-label"
                                               for="basic-addon-name">{{ _t('dashboard.Title') }} {{__($locale)}}</label>
                                        <input
                                            type="text"
                                            id="basic-addon-name"
                                            class="form-control"
                                            name="{{ $locale }}[title]"
                                            aria-describedby="basic-addon-name"
                                            required
                                            @if($common)
                                            @if($common['title'])
                                            value="{{$common['data']->translate($locale)->title}}"
                                            @else
                                            value="{{old('title')}}"
                                            @endif
                                            @else
                                            value="{{old('title')}}"
                                            @endif
                                            value="{{ old($locale . '.title') }}"
                                        />
                                        @error($locale . '.title')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                @endforeach

                            </div>

                            <div class="row col-12 mb-2">
                                @foreach (config('translatable.locales') as $index => $locale)

                                    <div class="mb-2 col-12">
                                        <label class="form-label"
                                               for="basic-addon-name">{{ _t('dashboard.Description') }} {{__($locale)}}</label>
                                        <textarea
                                            class="form-control ckeditor"
                                            id="description_{{$index}}"
                                            rows="3"
                                            name="{{ $locale }}[description]"
                                            placeholder="{{_t('dashboard.Description')}}"
                                        >  @if($common)
                                                @if($common['description'])
                                                    {{$common['data']->translate($locale)->description}}
                                                @else
                                                    {{old('description')}}
                                                @endif
                                            @else
                                                {{old('description')}}
                                            @endif
                                            {{old('description')}}</textarea>
                                        @error($locale . '.description')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                @endforeach

                            </div>


                            <div class="row col-12 mb-2">

                                <!-- Draggable Marker With Popup Starts -->
                                <div class="col-12">
                                    <div class="card mb-4">
                                        <div class="card-header">
                                            <h4 class="card-title">{{_t('dashboard.Location on Map')}}</h4>
                                        </div>
                                        <div class="card-body">
                                            <div id="map" style="z-index: 1"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Draggable Marker With Popup Ends -->

                            </div>

                            <input
                                type="text"
                                id="longitude"
                                class="form-control"
                                name="longitude"
                                aria-label="longitude"
                                aria-describedby="basic-addon-name"
                                hidden
                                @if($common)
                                @if($common['longitude'])
                                value="{{$common['longitude']}}"
                                @else
                                value="{{old('longitude')}}"
                                @endif
                                @else
                                value="{{old('longitude')}}"
                                @endif
                            />
                            <input
                                type="text"
                                id="latitude"
                                class="form-control"
                                name="latitude"
                                aria-label="latitude"
                                aria-describedby="basic-addon-name"
                                hidden
                                @if($common)
                                @if($common['latitude'])
                                value="{{$common['latitude']}}"
                                @else
                                value="{{old('latitude')}}"
                                @endif
                                @else
                                value="{{old('latitude')}}"
                                @endif
                            />

                        </div>

                        <x-seo-edit-component :data="$common['seo']" :slug="$common['slug']"
                                              :keywords="$common['keywords_object']"/>

                        @permission('update_setting')
                        <button type="submit" class="btn btn-primary">{{ _t('dashboard.Submit') }}</button>
                        @endpermission
                    </form>
                </div>
                <!-- /Vertical Wizard -->
            </div>
        </div>
        </div>
    </section>


@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/maps/leaflet.min.js'))}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/maps/map-leaflet.js'))}}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
    @include('admin.setting.map')

    @include('admin.partials.scripts')
    @include('admin.setting.crud_script')
    @include('admin.partials.alert')
@endsection




@endsection
