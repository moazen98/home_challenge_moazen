<script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"
        integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ=="
        crossorigin=""></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol@v0.76.1/dist/L.Control.Locate.min.js" charset="utf-8"></script>



<script>

    var lng = $("input[name='longitude']").val();
    var lat = $("input[name='latitude']").val();

    if (lat) {
        var map = L.map('map').setView([lat, lng], 13);

        var circle = L.circle([lat, lng], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 500
        }).addTo(map);

    } else {
        var map = L.map('map').setView([38.415342,27.144474], 13);

        var circle = L.circle([lat, lng], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 500
        }).addTo(map);

    }

    // var map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap'
    }).addTo(map);

    if (lat) {
        var marker = L.marker([lat, lng]).addTo(map);
    }

    // var marker = L.marker([51.5, -0.09]).addTo(map);

    if (!lat) {
        var popup = L.popup()
            .setLatLng([38.415342,27.144474])
            .setContent("الرجاء أختيار العنوان الخاص بك")
            .openOn(map);
    }

    var popup = L.popup();

    function onMapClick(e) {

        $("input[name='latitude']").val(e.latlng.lat)
        $("input[name='longitude']").val(e.latlng.lng)

        popup
            .setLatLng(e.latlng)
            .setContent("انت الآن في الأحداثية (" + e.latlng.lat + "," + e.latlng.lng + ")")
            .openOn(map);
    }

    L.control.locate().addTo(map);


    map.on('click', onMapClick);



</script>
