@extends('layouts.contentLayoutMaster')

@section('title', _t('dashboard.Show info'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <!-- vendor css files part2 -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Roboto+Slab&family=Slabo+27px&family=Sofia&family=Ubuntu+Mono&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">

@endsection

@section('page-style')
    {{-- Page Css files --}}


    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ _t('dashboard.General Info') }}</h3>
                            <div class="float-right">

                                <div class="row">
                                    @permission('update_info')
                                    <div class="col-5">
                                        <a type="button" class="btn btn-primary"
                                           href="{{ route('info.edit',$data['id']) }}"><i
                                                class="fa fa-edit"></i></a>
                                    </div>
                                    @endpermission

                                    @permission('delete_info')
                                    <div class="col-5">
                                        <form method="post" id="myForm"
                                              action="{{route('info.destroy',$data['id'])}}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="button" class="btn btn-danger delete"
                                                    title="{{_t('dashboard.Delete')}}"
                                            ><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>
                                    @endpermission

                                </div>

                            </div>


                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <div class="user-info text-center">
                                    <span class="badge bg-light-info">{{$data['title']}}</span>
                                    <span class="badge bg-light-warning">{{$data['type_string']}}</span>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card-body">
                            @if($data['title'])
                                <div class="row mb-2">
                                    <div class="col-md-6 col-12">
                                        <h4>{{ _t('dashboard.Title') }}:</h4>
                                        <p> {{ $data['title'] }} </p>
                                    </div>
                                </div>
                            @endif


                            @if($data['type_string'])
                                <div class="row mb-2">
                                    <div class="col-md-6 col-12">
                                        <h4>{{ _t('dashboard.Type') }}:</h4>
                                        <p> {{ $data['type_string'] }} </p>
                                    </div>
                                </div>
                            @endif

                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Status') }}:</h4>
                                    <span
                                        class="badge rounded-pill badge-light-{{$data['is_active_class']}}">{{$data['is_active_string']}}</span>
                                </div>
                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{_t('dashboard.ID')}}:</h4>
                                    <p> {{ $data['id'] }}</p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h4>{{ _t('dashboard.Date') }}:</h4>
                                    <p> {{ $data['date'] }} </p>
                                </div>
                            </div>


                            @if($data['description'])
                                <div class="row mb-2">
                                    <div class="col-md-12 col-12">
                                        <h4>{{ _t('dashboard.Description') }}:</h4>
                                        <p> {!! $data['description'] !!} </p>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>


    @include('admin.partials.scripts')

@endsection
