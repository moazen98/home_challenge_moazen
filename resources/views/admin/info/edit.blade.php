@extends('layouts/contentLayoutMaster')

@section('title', _t('dashboard.Edit info'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">
@endsection


@section('content')
    <section id="basic-horizontal-layouts">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{_t('dashboard.Edit info in the system')}}</h4>
                </div>


                <!-- Vertical Wizard -->
                <div class="card-body">
                    <form action="{{ route('info.update',$data['id']) }}" method="post"
                          enctype="multipart/form-data"
                          id="regForm">
                        @csrf

                        <div class="row col-12 mb-2">
                            @foreach (config('translatable.locales') as $locale)
                                <div class="col-6">
                                    <label class="form-label"
                                           for="basic-addon-name">{{ _t('dashboard.Title') }} {{__($locale)}}</label>
                                    <input
                                        type="text"
                                        id="{{ $locale }}-title"
                                        class="form-control"
                                        name="{{ $locale }}[title]"
                                        aria-describedby="basic-addon-name"
                                        value="{{$data['data']->translate($locale)->title}}"
                                    />
                                    @error($locale . '.title')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                </div>
                            @endforeach

                        </div>


                        <div class="row col-12 mb-2">
                            @foreach (config('translatable.locales') as $index => $locale)
                                <div class="mb-2 col-12">
                                    <label class="form-label"
                                           for="basic-addon-name">{{ _t('dashboard.Description') }} {{__($locale)}}</label>
                                    <textarea
                                        class="form-control ckeditor"
                                        id="description_{{$index}}"
                                        rows="3"
                                        name="{{ $locale }}[description]"
                                        placeholder="{{_t('dashboard.Description')}}"
                                    >{{$data['data']->translate($locale)->description}}</textarea>
                                    @error($locale . '.description')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                </div>
                            @endforeach
                        </div>


                        <div class="row col-12 mb-2">

                            <div class="col-6">
                                <label for="section">{{_t('dashboard.Info type')}} *</label>
                                <select class="select2 form-select" name="type" required>
                                    <option selected
                                            disabled>{{_t('dashboard.Please select info type')}}</option>
                                    <option @if($data['type']) selected
                                            @endif value="{{$data['type']}}">{{$data['type_string']}}</option>
                                    @foreach($types as $type)
                                        @if(!$infos->contains($type))
                                            <option @if($data['type'] == $type) selected @endif
                                            value="{{$type}}">{{_t('dashboard.'.$type)}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('type')
                                <span class="text-danger">{{$message }}</span>
                                @enderror
                            </div>
                        </div>


                        <div class="row col-12 mb-2">

                            <div class="col-6">
                                <div style="margin-left: 3%;" class="mb-2 form-check form-switch">
                                    <input type="checkbox" class="form-check-input" name="is_active"
                                           id="customSwitch1" @if($data['is_active']) checked @endif/>
                                    <label class="form-check-label"
                                           for="customSwitch1">{{ _t('dashboard.Activate') }}</label>
                                </div>
                            </div>

                        </div>


                        @permission('update_info')
                        <button type="submit" class="btn btn-primary">{{ _t('dashboard.Submit') }}</button>
                        @endpermission
                    </form>
                </div>
                <!-- /Vertical Wizard -->
            </div>
        </div>
        </div>
    </section>

@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>

    @include('admin.partials.scripts')

    @include('admin.info.include.scripts')

@endsection


