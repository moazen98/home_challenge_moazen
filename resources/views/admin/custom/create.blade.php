@extends('layouts/contentLayoutMaster')

@section('title', _t('dashboard.Add custom page'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">
@endsection


@section('content')
    <section id="basic-horizontal-layouts">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{_t('dashboard.Add custom page to the system')}}</h4>
                </div>


                <!-- Vertical Wizard -->
                <div class="card-body">
                    <form action="{{ route('custom.store') }}" method="post"
                          enctype="multipart/form-data"
                          id="regForm">
                        @csrf

                        <div class="row col-12 mb-2">
                            @foreach (config('translatable.locales') as $locale)
                                <div class="col-6">
                                    <label class="form-label"
                                           for="basic-addon-name">{{ _t('dashboard.Title') }} {{__($locale)}} *</label>
                                    <input
                                        type="text"
                                        id="basic-addon-name"
                                        class="form-control"
                                        name="{{ $locale }}[title]"
                                        aria-describedby="basic-addon-name"
                                        value="{{ old($locale . '.title') }}" required
                                    />
                                    @error($locale . '.title')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                </div>
                            @endforeach

                        </div>


                        <div class="row col-12 mb-2">
                            @foreach (config('translatable.locales') as $locale)
                                <div class="col-6">
                                    <label class="form-label"
                                           for="basic-addon-name">{{ _t('dashboard.Sub title') }} {{__($locale)}}</label>
                                    <input
                                        type="text"
                                        id="basic-addon-name"
                                        class="form-control"
                                        name="{{ $locale }}[sub_title]"
                                        aria-describedby="basic-addon-name"
                                        value="{{old($locale.'.sub_title')}}"
                                    />
                                    @error($locale.'.sub_title')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                </div>
                            @endforeach

                        </div>


                        <div class="row col-12 mb-2">
                            @foreach (config('translatable.locales') as $index => $locale)
                                <div class="mb-2 col-12">
                                    <label class="form-label"
                                           for="basic-addon-name">{{ _t('dashboard.Description') }} {{__($locale)}}</label>
                                    <textarea
                                        class="form-control ckeditor"
                                        id="description_{{$index}}"
                                        rows="3"
                                        name="{{ $locale }}[description]"
                                        placeholder="{{_t('dashboard.Description')}}"
                                    >{{old($locale . '.description')}}</textarea>
                                    @error($locale . '.description')
                                    <span class="text-danger">{{$message }}</span>
                                    @enderror
                                </div>
                            @endforeach
                        </div>

                        <div class="row col-12 mb-2">

                            <div class="col-6">
                                <label for="section">{{_t('dashboard.Page type')}} *</label>
                                <select class="select2 form-select" name="type" required>
                                    <option selected
                                            disabled>{{_t('dashboard.Please select page type')}}</option>
                                    @foreach($types as $type)
                                        @if(!$customs->contains($type))
                                            <option @if(old('type') == $type) selected @endif
                                            value="{{$type}}">{{_t('dashbord.'.$type)}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('type')
                                <span class="text-danger">{{$message }}</span>
                                @enderror
                            </div>

                            <div class="col-6">
                                <!-- remove thumbnail file upload starts -->
                                <label class="form-label" for="vertical-username">{{ _t('dashboard.Image') }}</label>
                                <input type="file" class="form-control image" name="files" id="uploadInputImage"
                                       accept="image/*"/>
                                @error('files')
                                <span class="text-danger">{{$message }}</span>
                                @enderror
                                <img style="width: 129px; margin-top: 2%;" id="uploadInputPreview"/>
                            </div>


                        </div>


                        @permission('create_custom')
                        <button type="submit" class="btn btn-primary">{{ _t('dashboard.Submit') }}</button>
                        @endpermission
                    </form>
                </div>
                <!-- /Vertical Wizard -->
            </div>
        </div>
        </div>
    </section>

@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>

    @include('admin.partials.scripts')

    @include('admin.custom.include.scripts')

@endsection
