@extends('layouts/contentLayoutMaster')

@section('title', _t('dashboard.Custom pages'))


@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="" role="alert">
                <div class="alert-body">
                    <!-- Responsive Datatable -->


                    <div class="row">

                        <x-search-filter-component :col="4" :message="_t('dashboard.Search by Title , Sub title , Description , ID')"/>

                    </div>

                    <section id="responsive-datatable">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header border-bottom">
                                        <h4 class="card-title">{{ _t('dashboard.Custom pages info') }}</h4>
                                        <div class="row">
                                            @permission('create_custom')
                                            <div class="col-4">
                                                <a href="{{route('custom.create')}}"
                                                >
                                                    <button type="button"
                                                            class="btn btn-primary">{{ _t('dashboard.Create') }}
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </a>
                                            </div>
                                            @endpermission


                                            <x-filter-other-component/>

                                        </div>


                                    </div>


                                    <div class="card-datatable overflow-auto">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>{{ _t('dashboard.Id') }}</th>
                                                <th>{{ _t('dashboard.Title') }}</th>
                                                <th>{{ _t('dashboard.Type') }}</th>
                                                <th>{{ _t('dashboard.Date') }}</th>
                                                <th>{{ _t('dashboard.Action') }}</th>
                                            </tr>
                                            </thead>
                                            <body>
                                            @include('admin.custom.include.pagination_data')
                                            </body>

                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--/ Responsive Datatable -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>

    @include('admin.partials.scripts')
    @include('admin.custom.include.filters')
    @include('admin.partials.alert')
@endsection
