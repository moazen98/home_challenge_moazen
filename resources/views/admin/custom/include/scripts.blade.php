<script>
    $(document).ready(function () {


        $("#regForm").validate({
            rules: {

            },
            messages: {
                "ar[title]": {
                    required: "{{_t('validation.The field is required')}}",
                },
                "en[title]": {
                    required: "{{_t('validation.The field is required')}}",
                } ,
                type: {
                    required: "{{_t('validation.The field is required')}}",
                }
            }
        });
    });
</script>
