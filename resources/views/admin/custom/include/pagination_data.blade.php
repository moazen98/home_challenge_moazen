@if(count($data) > 0)
    @foreach ($data as $index => $page)
        <tr>
            <td>{{$page['id']}}</td>
            <td>{{$page['title']}}</td>

            <td>
                           <span
                            class="badge rounded-pill badge-light-info">{{$page['type_string']}}</span>
            </td>

            <td>
                                                            <span
                                                                class="badge rounded-pill badge-light-warning">{{$page['date']}}</span>
            </td>

            <td>
                <div class="dropdown">
                    <button type="button"
                            class="btn btn-sm dropdown-toggle hide-arrow py-0"
                            data-bs-toggle="dropdown">
                        <i data-feather="more-vertical"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">

                        @permission('read_custom')
                        <a class="dropdown-item" href="{{route('custom.show',$page['id'])}}">
                            <i data-feather="eye" class="me-50"></i>
                            <span>{{_t('dashboard.Show')}}</span>
                        </a>
                        @endpermission

                        @permission('update_custom')
                        <a class="dropdown-item"
                           href="{{route('custom.edit',$page['id'])}}">
                            <i data-feather="edit-2" class="me-50"></i>
                            <span>{{_t('dashboard.Edit')}}</span>
                        </a>
                        @endpermission

                        @permission('delete_custom')
                        <div class="col-4">
                            <form method="post" id="myForm"
                                  action="{{route('custom.destroy',$page['id'])}}">
                                @csrf
                                @method('DELETE')


                                <button
                                    style="display: inline-flex;width: 137px;"
                                    class="btn delete dropdown-item"><i
                                        data-feather="trash" class="me-50"></i>
                                    <span>{{_t('dashboard.Delete')}}</span></button>
                            </form>
                        </div>
                        @endpermission
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    <td colspan="3">
        {!! $data->appends(request()->query())->links("pagination::bootstrap-4") !!}
    </td>
@else
    @include('admin.include.no_data')
@endif
<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

