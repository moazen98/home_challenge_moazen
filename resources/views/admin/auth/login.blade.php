@php
    $configData = Helper::applClasses();
@endphp
@extends('layouts/fullLayoutMaster')

@section('title', _t('dashboard.Login'))

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/authentication.css')) }}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">
@endsection

@section('content')

    @include('admin.auth.partials.style')


    <div class="auth-wrapper auth-cover">
        <div class="auth-inner row m-0">
            <!-- Brand logo-->

            <!-- /Brand logo-->

            <!-- Left Text-->
            <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                    @if($configData['theme'] === 'dark')
                        <img class="img-fluid" src="{{asset('dashboard/images/auth-login-illustration-dark.png')}}" alt="Background Image"/>
                    @else
                        <img class="img-fluid" src="{{asset('dashboard/images/auth-login-illustration-dark.png')}}" alt="Background Image"/>
                    @endif
                </div>
            </div>



            <!-- /Left Text-->
            <!-- Login-->
            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                    <h4 class="card-subtitlefw-bolder mb-1 btitle2 mb-4" style="font-size: 40px">{{$_setting['title']}}</h4>
                    <div class="login-rs-title">
                        <h2 class="card-title fw-bolder mb-1 btitle1" style="font-size: 20px">{{_t('dashboard.Login to admin panel')}}</h2>
                    </div>

              <p class="card-text mb-2">{{_t('dashboard.Please sign-in to your account')}}</p>
                    <h2  class="text-white" style="font-size: 15px">{{_t('dashboard.Username') }} : admin@gmail.com</h2>
                    <h2  class="text-white" style="font-size: 15px">{{_t('dashboard.Phone') }} : 0999999999</h2>
                    <h2 class="text-white" style="font-size: 15px">{{_t('dashboard.Password') }} : 123456</h2>
                    <form class="auth-login-form mt-2" action="{{route('admin.login')}}" method="POST">
                        @csrf
                        <div class="mb-1">
                            <label class="form-label text-white" for="login-email">{{ _t('dashboard.Email/Phone') }}</label>
                            <input class="form-control border-0 p-1" id="login-email" type="text" name="credential"
                                   placeholder="admin@gmail.com" aria-describedby="login-email" tabindex="1"
                                   @error('credential') is-invalid @enderror" name="credential" value="{{ old('credential') }}"
                            required autocomplete="credential" autofocus />
                        </div>
                        <div class="mb-1">
                            <label class="form-label text-white" for="login-password">{{ _t('dashboard.Password') }}</label>
                            <div class="input-group input-group-merge form-password-toggle">
                                <input class="form-control form-control-merge border-0 p-1 mb-0" id="login-password" type="password"
                                       name="password" placeholder="123456" aria-describedby="login-password"
                                       tabindex="2" @error('password') is-invalid @enderror" name="password" required
                                autocomplete="current-password" />
                                <span class="input-group-text cursor-pointer border-0"><i data-feather="eye"></i></span>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-check">
                                <input class="form-check-input" name="remember" id="remember" type="checkbox"
                                       tabindex="3" {{ old('remember') ? 'checked' : '' }} />
                                <label class="form-check-label text-white" for="remember-me">{{ _t('dashboard.Remember Me') }}</label>
                            </div>
                        </div>
                        <button class="btn btn-primary w-100 p-1 mt-2" tabindex="4" style="border:0px solid #fff!important;">{{_t('dashboard.Sign in')}}</button>
                    </form>
                </div>
            </div>
            <!-- /Login-->
        </div>
    </div>
@endsection

@section('vendor-script')
    <script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>
    <script src="{{asset(mix('js/scripts/pages/auth-login.js'))}}"></script>

    @include('admin.partials.scripts')
    @include('admin.partials.alert')
@endsection


