@extends('layouts/contentLayoutMaster')

@section('title', $_setting['title'])

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->
    <section id="dashboard-ecommerce">
        <div class="row match-height">

            <!-- Statistics Card -->
            <div class="col-xl-12 col-md-12 col-12">
                <div class="card card-statistics">
                    <div class="card-header">
                        <h4 class="card-title">{{_t('dashboard.Statistics')}}</h4>
                        <div class="d-flex align-items-center">
                            <p class="card-text font-small-2 me-25 mb-0">{{_t('dashboard.The date')}} :
                                <script>document.write(new Date().toDateString())</script>
                            </p>
                            &nbsp; || &nbsp;
                            <hr>
                            <p class="card-text font-small-2 me-25 mb-0"> {{ _t('dashboard.The time')}} :
                                <script>document.write(new Date().toLocaleTimeString())</script>
                            </p>
                        </div>
                    </div>
                    <div class="card-body statistics-body">
                        <div class="row">

                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-primary me-2">
                                        <div class="avatar-content">
                                            <i data-feather="user" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">{{$users}}</h4>
                                        <p class="card-text font-small-3 mb-0">{{_t('dashboard.Employees')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-danger me-2">
                                        <div class="avatar-content">
                                            <i data-feather="shield" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">{{$roles}}</h4>
                                        <p class="card-text font-small-3 mb-0">{{_t('dashboard.Roles')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-warning me-2">
                                        <div class="avatar-content">
                                            <i data-feather="home" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">{{$sections}}</h4>
                                        <p class="card-text font-small-3 mb-0">{{_t('dashboard.Sections')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-info me-2">
                                        <div class="avatar-content">
                                            <i data-feather="file-text" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">{{$blogs}}</h4>
                                        <p class="card-text font-small-3 mb-0">{{_t('dashboard.Blogs')}}</p>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                </div>

                <div class="card card-statistics">

                    <div class="card-body statistics-body">
                        <div class="row">

                            <div class="col-md-12 col-12">
                                <h4>{{ _t('dashboard.About us') }}:</h4>
                                <p> {!! $_setting['description'] !!}</p>
                            </div>


                        </div>
                    </div>
                </div>

                @foreach($infos['infos'] as $info)
                    <div class="card card-statistics">

                        <div class="card-body statistics-body">
                            <div class="row">

                                <div class="col-md-12 col-12">
                                    <h4>{{ $info['title'] }}:</h4>
                                    @if($info['type'] == \App\Enums\InfoType::LINKED_IN)
                                    <a href="{!! $info['description'] !!}" target="_blank">  <p> {!! $info['description'] !!}</p></a>
                                    @else
                                       <p> {!! $info['description'] !!}</p>
                                    @endif
                                </div>


                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <!--/ Statistics Card -->
        </div>

    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
@endsection
