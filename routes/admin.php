<?php

use App\Http\Controllers\Admin\CustomPageController;
use App\Http\Controllers\Admin\GuardianBlogController;
use App\Http\Controllers\Admin\InfoController;
use App\Http\Controllers\Admin\keyController;
use App\Http\Controllers\Admin\NewsApiAiController;
use App\Http\Controllers\Admin\NewsApiBlogController;
use App\Http\Controllers\Admin\NewYorkBlogController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\TranslationController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\DashboardController;
use \App\Http\Controllers\LanguageController;
use \App\Http\Controllers\Admin\Authentication\AuthController;
use \App\Http\Controllers\Admin\User\UserController;
use \App\Http\Controllers\Admin\User\RoleController;
use \App\Http\Controllers\Admin\Area\AreaController;
use \App\Http\Controllers\Admin\User\SectionController;

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login')->name('admin.login');
    Route::get('login', 'getAdminLogin')->name('admin.get.login');
    Route::get('/', 'index')->name('admin.get.index');
});

Route::group(['middleware' => ['auth']], function () {

    Route::controller(DashboardController::class)->group(function () {
        Route::get('/dashboard', 'dashboardEcommerce')->name('admin.dashboard-ecommerce');
        Route::get('/logout', 'logout')->name('admin.logout');
    });

    Route::group(['prefix' => 'area'], function () {
        Route::controller(AreaController::class)->group(function () {
            Route::post('get-city', 'index')->name('area.cities');
        });
    });

    Route::group(['prefix' => 'users'], function () {

        Route::group(['prefix' => 'employee', 'as' => 'employee.'], function () {
            Route::controller(UserController::class)->group(function () {
                Route::get('index', 'index')->name('index');
                Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
                Route::get('create', 'create')->name('create');
                Route::get('show/{id}', 'show')->name('show');
                Route::get('edit/{id}', 'edit')->name('edit');
                Route::post('update/{id}', 'update')->name('update');
                Route::post('store', 'store')->name('store');
                Route::delete('delete/{id}', 'destroy')->name('destroy');
                Route::get('export', 'export')->name('export');

            });
        });

        Route::group(['prefix' => 'role', 'as' => 'role.'], function () {
            Route::controller(RoleController::class)->group(function () {
                Route::get('index', 'index')->name('index');
                Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
                Route::get('create', 'create')->name('create');
                Route::get('show/{id}', 'show')->name('show');
                Route::get('edit/{id}', 'edit')->name('edit');
                Route::post('update/{id}', 'update')->name('update');
                Route::post('store', 'store')->name('store');
                Route::delete('delete/{id}', 'destroy')->name('destroy');
                Route::get('export', 'export')->name('export');
            });
        });


        Route::group(['prefix' => 'section', 'as' => 'section.'], function () {
            Route::controller(SectionController::class)->group(function () {
                Route::get('index', 'index')->name('index');
                Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
                Route::get('create', 'create')->name('create');
                Route::get('show/{id}', 'show')->name('show');
                Route::get('edit/{id}', 'edit')->name('edit');
                Route::post('update/{id}', 'update')->name('update');
                Route::post('store', 'store')->name('store');
                Route::delete('delete/{id}', 'destroy')->name('destroy');
                Route::get('export', 'export')->name('export');
            });
        });


    });


    //TODO : NewsApi endpoints
    Route::group(['prefix' => 'news-api-blog', 'as' => 'social.blog.'], function () {
        Route::controller(NewsApiBlogController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::get('show/{id}', 'show')->name('show');
            Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
            Route::get('update-data', 'updateBlogData')->name('updateBlogData');
            Route::delete('delete/{id}', 'destroy')->name('destroy');
            Route::get('export', 'export')->name('export');
            Route::get('reorder', 'reorder')->name('reorder');
            Route::patch('reorder-post', 'reorderPost')->name('reorder-post');
        });
    });

    //TODO : Guardian endpoints
    Route::group(['prefix' => 'guardian-api-blog', 'as' => 'social.blog.guardian.'], function () {
        Route::controller(GuardianBlogController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::get('show/{id}', 'show')->name('show');
            Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
            Route::get('update-data', 'updateBlogData')->name('updateBlogData');
            Route::delete('delete/{id}', 'destroy')->name('destroy');
            Route::get('export', 'export')->name('export');
            Route::get('reorder', 'reorder')->name('reorder');
            Route::patch('reorder-post', 'reorderPost')->name('reorder-post');
        });
    });

    //TODO : New york endpoints
    Route::group(['prefix' => 'new-york-api-blog', 'as' => 'social.blog.york.'], function () {
        Route::controller(NewYorkBlogController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::get('show/{id}', 'show')->name('show');
            Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
            Route::get('update-data', 'updateBlogData')->name('updateBlogData');
            Route::delete('delete/{id}', 'destroy')->name('destroy');
            Route::get('export', 'export')->name('export');
            Route::get('reorder', 'reorder')->name('reorder');
            Route::patch('reorder-post', 'reorderPost')->name('reorder-post');
        });
    });

    //TODO : NewsApiAi endpoints
    Route::group(['prefix' => 'news-api-ai-blog', 'as' => 'social.blog.ai.'], function () {
        Route::controller(NewsApiAiController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::get('show/{id}', 'show')->name('show');
            Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
            Route::get('update-data', 'updateBlogData')->name('updateBlogData');
            Route::delete('delete/{id}', 'destroy')->name('destroy');
            Route::get('export', 'export')->name('export');
            Route::get('reorder', 'reorder')->name('reorder');
            Route::patch('reorder-post', 'reorderPost')->name('reorder-post');
        });
    });

    Route::group(['prefix' => 'key', 'as' => 'key.'], function () {
        Route::controller(keyController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::get('show/{id}', 'show')->name('show');
            Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
            Route::get('create', 'create')->name('create');
            Route::get('edit/{id}', 'edit')->name('edit');
            Route::post('update/{id}', 'update')->name('update');
            Route::post('store', 'store')->name('store');
            Route::delete('delete/{id}', 'destroy')->name('destroy');
            Route::get('export', 'export')->name('export');
        });
    });

    Route::group(['prefix' => 'info', 'as' => 'info.'], function () {
        Route::controller(InfoController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::get('show/{id}', 'show')->name('show');
            Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
            Route::get('create', 'create')->name('create');
            Route::get('edit/{id}', 'edit')->name('edit');
            Route::post('update/{id}', 'update')->name('update');
            Route::post('store', 'store')->name('store');
            Route::delete('delete/{id}', 'destroy')->name('destroy');
            Route::get('export', 'export')->name('export');
        });
    });


    Route::group(['prefix' => 'custom', 'as' => 'custom.'], function () {
        Route::controller(CustomPageController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::get('show/{id}', 'show')->name('show');
            Route::get('/pagination/fetch-data', 'fetchData')->name('fetchData');
            Route::get('create', 'create')->name('create');
            Route::get('edit/{id}', 'edit')->name('edit');
            Route::post('update/{id}', 'update')->name('update');
            Route::post('store', 'store')->name('store');
            Route::delete('delete/{id}', 'destroy')->name('destroy');
        });
    });

    Route::group(['prefix' => 'setting', 'as' => 'setting.'], function () {
        Route::controller(SettingController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::post('update', 'update')->name('update');
        });
    });

    Route::group(['prefix' => 'translation', 'as' => 'translation.'], function () {
        Route::controller(TranslationController::class)->group(function () {
            Route::get('index', 'index')->name('index');
            Route::get('edit/{id}', 'edit')->name('edit');
            Route::post('update/{key}', 'update')->name('update');
            Route::post('delete/{id}', 'destroy')->name('destroy');
        });
    });

    Route::get('lang/{locale}', [LanguageController::class, 'swap'])->name('lang.swap');

});

