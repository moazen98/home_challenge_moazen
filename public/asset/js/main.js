const burger = document.querySelector("#nav-container nav .navbar-toggler");
const navLinks = document.querySelectorAll(
  "#nav-container .navbar-nav .nav-item"
);
const closeBtn = document.querySelector(
  "#nav-container .offcanvas .offcanvas-header .close"
);

burger.addEventListener("click", () => {
  navLinks.forEach((link, index) => {
    link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.3}s`;
  });
});

closeBtn.addEventListener("click", () => {
  navLinks.forEach((link) => {
    link.style.animation = "";
  });
});

(function ($) {
  $(window).bind("scroll", function (event) {
    var scrollValue = $(window).scrollTop() || $("body").scrollTop();
    if (scrollValue > 270) {
      $("#nav-container").addClass("fixed-top");
    } else {
      $("#nav-container").removeClass("fixed-top");
    }
  });
})(jQuery);

$("#slider").owlCarousel({
  loop: true,
  autoplay: true,
  nav: false,
  items: 1,
  URLhashListener: true,
  autoplayHoverPause: false,
  lazyLoad: true,
  startPosition: "URLHash",
});

const indacators = Array.from(
  document.querySelectorAll(".landing .owl-dots .owl-dot")
);

if (indacators) {
  let conter = 1;
  indacators.forEach((indacator) => {
    const div = document.createElement("div");
    const divText = document.createTextNode(conter);
    div.appendChild(divText);
    div.classList.add("indacator");
    indacator.children[0].remove();
    indacator.appendChild(div);
    conter++;
  });
}

$("#projects-done").owlCarousel({
  loop: true,
  margin: 10,
  autoplay: true,
  nav: true,
  dots: true,
  autoplayHoverPause: true,
  navText: [
    `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 33.628 30.349">
            <g id="Group_6689" data-name="Group 6689" transform="translate(1.705 2.41)">
                <rect id="Rectangle_23" data-name="Rectangle 23" width="29.458" height="2.946" rx="1" transform="translate(1.965 11.193)" fill="#cca43b" stroke="#cca43b" stroke-width="1"/>
                <path id="Vector_1" data-name="Vector 1" d="M13.747,25.53,0,12.765,13.747,0" transform="translate(0 0)" fill="none" stroke="#cca43b" stroke-linecap="round" stroke-linejoin="round" stroke-width="3.41" stroke-dasharray="0 0"/>
            </g>
        </svg>
`,
    `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 33.628 30.349">
            <g id="Group_6690" data-name="Group 6690" transform="translate(0.5 2.41)">
                <rect id="Rectangle_23" data-name="Rectangle 23" width="29.458" height="2.946" rx="1" transform="translate(29.458 14.139) rotate(180)" fill="#cca43b" stroke="#cca43b" stroke-width="1"/>
                <path id="Vector_1" data-name="Vector 1" d="M13.747,0,0,12.765,13.747,25.53" transform="translate(31.423 25.53) rotate(180)" fill="none" stroke="#cca43b" stroke-linecap="round" stroke-linejoin="round" stroke-width="3.41" stroke-dasharray="0 0"/>
            </g>
        </svg>
`,
  ],
  responsive: {
    0: {
      items: 1,
    },
    768: {
      items: 2,
    },
    1092: {
      items: 3,
    },
    1300: {
      items: 4,
    },
  },
});

$("#projects-pending").owlCarousel({
  loop: true,
  margin: 10,
  autoplay: true,
  nav: true,
  dots: true,
  autoplayHoverPause: true,
  navText: [
    `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 33.628 30.349">
            <g id="Group_6689" data-name="Group 6689" transform="translate(1.705 2.41)">
                <rect id="Rectangle_23" data-name="Rectangle 23" width="29.458" height="2.946" rx="1" transform="translate(1.965 11.193)" fill="#cca43b" stroke="#cca43b" stroke-width="1"/>
                <path id="Vector_1" data-name="Vector 1" d="M13.747,25.53,0,12.765,13.747,0" transform="translate(0 0)" fill="none" stroke="#cca43b" stroke-linecap="round" stroke-linejoin="round" stroke-width="3.41" stroke-dasharray="0 0"/>
            </g>
        </svg>
`,
    `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 33.628 30.349">
            <g id="Group_6690" data-name="Group 6690" transform="translate(0.5 2.41)">
                <rect id="Rectangle_23" data-name="Rectangle 23" width="29.458" height="2.946" rx="1" transform="translate(29.458 14.139) rotate(180)" fill="#cca43b" stroke="#cca43b" stroke-width="1"/>
                <path id="Vector_1" data-name="Vector 1" d="M13.747,0,0,12.765,13.747,25.53" transform="translate(31.423 25.53) rotate(180)" fill="none" stroke="#cca43b" stroke-linecap="round" stroke-linejoin="round" stroke-width="3.41" stroke-dasharray="0 0"/>
            </g>
        </svg>
`,
  ],
  responsive: {
    0: {
      items: 1,
    },
    768: {
      items: 2,
    },
    1092: {
      items: 3,
    },
    1300: {
      items: 4,
    },
  },
});

$("#projects-can-invested").owlCarousel({
  loop: true,
  margin: 10,
  autoplay: true,
  nav: true,
  dots: true,
  autoplayHoverPause: true,
  navText: [
    `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 33.628 30.349">
            <g id="Group_6689" data-name="Group 6689" transform="translate(1.705 2.41)">
                <rect id="Rectangle_23" data-name="Rectangle 23" width="29.458" height="2.946" rx="1" transform="translate(1.965 11.193)" fill="#cca43b" stroke="#cca43b" stroke-width="1"/>
                <path id="Vector_1" data-name="Vector 1" d="M13.747,25.53,0,12.765,13.747,0" transform="translate(0 0)" fill="none" stroke="#cca43b" stroke-linecap="round" stroke-linejoin="round" stroke-width="3.41" stroke-dasharray="0 0"/>
            </g>
        </svg>
`,
    `<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 33.628 30.349">
            <g id="Group_6690" data-name="Group 6690" transform="translate(0.5 2.41)">
                <rect id="Rectangle_23" data-name="Rectangle 23" width="29.458" height="2.946" rx="1" transform="translate(29.458 14.139) rotate(180)" fill="#cca43b" stroke="#cca43b" stroke-width="1"/>
                <path id="Vector_1" data-name="Vector 1" d="M13.747,0,0,12.765,13.747,25.53" transform="translate(31.423 25.53) rotate(180)" fill="none" stroke="#cca43b" stroke-linecap="round" stroke-linejoin="round" stroke-width="3.41" stroke-dasharray="0 0"/>
            </g>
        </svg>
`,
  ],
  responsive: {
    0: {
      items: 1,
    },
    768: {
      items: 2,
    },
    1092: {
      items: 3,
    },
    1300: {
      items: 4,
    },
  },
});

$(document).ready(function () {
  var sync1 = $("#sync1");
  var sync2 = $("#sync2");
  var slidesPerPage = 4; //globaly define number of elements per page
  var syncedSecondary = true;

  sync1
    .owlCarousel({
      items: 1,
      slideSpeed: 2000,
      nav: false,
      autoplay: false,
      dots: false,
      loop: true,
      responsiveRefreshRate: 200,
      navText: [
        '<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>',
        '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>',
      ],
    })
    .on("changed.owl.carousel", syncPosition);

  sync2
    .on("initialized.owl.carousel", function () {
      sync2.find(".owl-item").eq(0).addClass("current");
    })
    .owlCarousel({
      items: slidesPerPage,
      dots: false,
      nav: false,
      smartSpeed: 200,
      slideSpeed: 500,
      slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
      responsiveRefreshRate: 100,
    })
    .on("changed.owl.carousel", syncPosition2);

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;

    //if you disable loop you have to comment this block
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

    if (current < 0) {
      current = count;
    }
    if (current > count) {
      current = 0;
    }

    //end block

    sync2
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = sync2.find(".owl-item.active").length - 1;
    var start = sync2.find(".owl-item.active").first().index();
    var end = sync2.find(".owl-item.active").last().index();

    if (current > end) {
      sync2.data("owl.carousel").to(current, 100, true);
    }
    if (current < start) {
      sync2.data("owl.carousel").to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if (syncedSecondary) {
      var number = el.item.index;
      sync1.data("owl.carousel").to(number, 100, true);
    }
  }

  sync2.on("click", ".owl-item", function (e) {
    e.preventDefault();
    var number = $(this).index();
    sync1.data("owl.carousel").to(number, 300, true);
  });
});

$("#estate-one").owlCarousel({
  loop: true,
  margin: 10,
  autoplay: true,
  nav: false,
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    578: {
      items: 2,
    },
  },
});
$("#estate-two").owlCarousel({
  loop: true,
  margin: 10,
  autoplay: true,
  nav: false,
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    578: {
      items: 2,
    },
  },
});
$("#estate-three").owlCarousel({
  loop: true,
  margin: 10,
  autoplay: true,
  nav: false,
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    578: {
      items: 2,
    },
  },
});
