<?php


use App\Enums\MediaExtension;
use Illuminate\Support\Facades\Storage;

function upload_files($upload_path, $folder, $allowed_types, $request)
{

    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;


    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

//        touch($upload_path . DIRECTORY_SEPARATOR . 'index.html');
    }

    $uploaded_file = null;

    foreach ($request->image as $file_key => $file) {

        $imageName = $file->getClientOriginalName();

        $uploaded_file[$file_key] = $file->move(public_path($upload_path), $imageName);
    }

    $res = is_null($uploaded_file);

    if ($res != true) {

        foreach ($request->image as $file_key => $file) {

            $ret_files[$file_key]['file_name'] = $file->getClientOriginalName();
            $ret_files[$file_key]['file_type'] = $file->getClientOriginalExtension();

            $imageName = $file->getClientOriginalName();

            $ret_files[$file_key]['real_path'] = $folder . '/' . $imageName;

        }

    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}

//TODO:Here the object that pass $request -> is request file http
function upload_single_files($upload_path, $folder, $allowed_types, $request)
{

    $size = $request->getSize();
    $ret_files = array();

    $upload_path = $upload_path . $folder;


    $config['upload_path'] = $upload_path;


    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

    }

    $uploaded_file = null;

    $imageName = $request->getClientOriginalName();
    $imageNameHash = $request->hashName();
    $imageExtension = $request->getClientOriginalExtension();

    $uploaded_file[0] = $request->move(public_path($upload_path), $imageNameHash);

    $res = is_null($uploaded_file);

    if ($res != true) {

        $ret_files[0]['size'] = formatBytes($size);
        $ret_files[0]['file_name'] = $imageNameHash;
        $ret_files[0]['real_name'] = $imageName;
        $ret_files[0]['file_type'] = $imageExtension;
        $ret_files[0]['real_path'] = $folder . '/' . $imageName;


    } else {
        $ret_files['file_key'] = 'error upload';
        $response['status'] = false;
        $response['ret_data'] = array();

        return $response;
    }

    return $ret_files;
}


//TODO:Here the object that pass $object -> is file object (Symfony\Component\HttpFoundation\File\File)
function upload_single_files_object($upload_path, $folder, $allowed_types, $object)
{

    $size = $object->getSize();
    $ret_files = array();

    $upload_path = $upload_path . $folder;

    if (!file_exists($upload_path)) {
        File::makeDirectory(public_path() . '/' . $upload_path, 0777, true);

    }

    $imageName = $object->getBasename();
    $imageNameHash = $object->getBasename();
    $imageExtension = $object->getMimeType();

    File::copy($object->getLinkTarget(), public_path($upload_path).'/'.$imageName);

    $ret_files[0]['size'] = formatBytes($size);
    $ret_files[0]['file_name'] = $imageNameHash;
    $ret_files[0]['real_name'] = $imageName;
    $ret_files[0]['file_type'] = $imageExtension;
    $ret_files[0]['real_path'] = $folder . '/' . $imageName;

    return $ret_files;
}


function formatBytes($bytes, $precision = 2)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');


    $bytes = max($bytes, 0);
    $pow = ceil($bytes / 1048576);

    return round($pow, $precision);
}


function image_extensions()
{

    $extensions_images = array();
    $extensions_images = MediaExtension::getValues(['PNG', 'JPEG', 'JPG', 'GIF']);

    return $extensions_images;
}


function files_extensions()
{

    $extensions_files = array();
    $extensions_files = MediaExtension::getValues(['XLS', 'PDF']);

    return $extensions_files;
}

function audio_extensions()
{

    $extensions_audio = array();
    $extensions_audio = MediaExtension::getValues(['MP3', 'OGG', 'WAV']);

    return $extensions_audio;
}

function vedio_extensions()
{

    $extensions_vedio = array();
    $extensions_vedio = MediaExtension::getValues(['MP4', 'MPEG', 'MPGA']);

    return $extensions_vedio;
}

function checkIfThereIsFilesRequest($request){

    if (count($request->allFiles()) <=0){
        return false;
    }

    return true;
}

function checkIfFileExist($request,$file){

    if ($request->has($file)){
        return array($request->file($file)); //TODO : Here I must return array files for the service file
    }

    return null;
}

//TODO: it use with the repeater image
function fileToArray($file)
{

    return array($file['images']); //TODO : Here I must return array files for the service file
}



function downloadRemoteFile($remoteUrl,$fileName,$className,$configUrl,$mediaFor,$modelObject)
{
    $remoteFileUrl = $remoteUrl; // Replace with your remote file URL

    $fileContent = file_get_contents($remoteFileUrl);


    if ($fileContent === false || !$fileContent) {
        return false;
    }

    //get the extension of base64 code file
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime = finfo_buffer($finfo, $fileContent);

    $extensions = [
        'image/jpeg' => 'jpg',
        'image/png' => 'png',
        'image/gif' => 'gif',
        'image/bmp' => 'bmp',
        // Add more mime types and extensions as needed
    ];

// Determine the file extension based on the MIME type
    $extension = $extensions[$mime] ?? 'unknown';

    if ($extension === 'unknown') {
        return false;
    }
    $stringResSpecial = str_replace( array( '\'', '"',
        ',','?',':', ';', '<', '>' ), '', $fileName);

    $stringRes = str_replace( array(' '), '_', $stringResSpecial);

    $localFilePath = 'downloads'; // Set your local path
    $localFileName = $stringRes.'.'.$extension; // Set your desired local filename

    // Use the Storage facade to save the downloaded content to the local filesystem
    Storage::disk('local')->put($localFilePath . '/' . $localFileName, $fileContent);

    // Create a Laravel File object for the saved file
    $localFile = new Illuminate\Http\File(storage_path('app/' . $localFilePath . '/' . $localFileName));
    $fileArray = array($localFile);

    app('servicesV1')->fileService->storeFileWithWithObject($className,$configUrl,$mediaFor,$modelObject,$fileArray);

    return true;
}
