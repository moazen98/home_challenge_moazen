<?php


use App\Enums\CustomerVerificationMethod;
use App\Enums\GenderType;
use App\Enums\MediaType;
use App\Http\Resources\Dashboard\User\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Laravel\Sanctum\Sanctum;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;


//TODO : put other users when you create them
function getCurrentDashboardUser()
{

    $user = Auth::user();
    return $user;

}


function getUserAgencyType($type)
{
    if ($type->authentication) {
        return $type->authentication->authenticatable_type;
    }

    return null;
}


function CheckUserVerified($type, $user)
{

    if ($user->authentication) {
        return $user->authentication->is_verified;
    }

    return false;
}


function CheckUserStatus($type, $user)
{

    if ($user->authentication) {
        return $user->authentication->is_active;
    }

    return false;

}


function getUserAgencyResource($type, $user)
{

    if ($type == User::class) {
        return (new UserResource($user))->toArray();
    }

    return null;
}

function inputCredentialType($request)
{
    $method = $request->input('credential') == null ? null : (filter_var($request->input('credential'), FILTER_VALIDATE_EMAIL) ? CustomerVerificationMethod::EMAIL : (is_numeric($request->input('credential')) ? CustomerVerificationMethod::PHONE : CustomerVerificationMethod::OTHER));

    return $method;

}


function getUserAuthGuard()
{

    if (Auth::guard('web')->check()) {
        $user = Auth::guard('web')->user();
        return $user;
    }

    return  null;
}


function getUserMobile($request){

    if ($request->header('Authorization')) {
        $token = $request->bearerToken();
        $model = Sanctum::$personalAccessTokenModel;
        $accessToken = $model::where('token', $token)->first();
        if (!$accessToken || !$accessToken->tokenable) {
            return null;
        }

        return $accessToken->tokenable;

    } else {

        return null;
    }
}



function getActiveString($bool)
{

    if ($bool) {
        return _t('dashboard.Active');
    }

    if (!$bool) {
        return _t('dashboard.InActivate');
    }

    return null;
}


function getMainUserString($bool)
{

    if ($bool) {
        return _t('dashboard.Main');
    }

    if (!$bool) {
        return _t('dashboard.Not main');
    }

    return null;
}

function getIsMainString($bool)
{

    if ($bool) {
        return _t('dashboard.Main');
    }

    if (!$bool) {
        return _t('dashboard.Not main');
    }

    return null;
}

function getActiveClass($type)
{

    if ($type) {
        return "success";
    }

    if (!$type) {
        return "warning";
    }

    return null;
}


function getTypeString($type)
{

    if ($type == MediaType::IMAGE) {
        return _t('dashboard.Image');
    }

    if ($type == MediaType::VIDEO) {
        return _t('dashboard.Video');
    }

    return _t('dashboard.No data');
}

function getTypeClass($type)
{

    if ($type == MediaType::IMAGE) {
        return "success";
    }

    if ($type == MediaType::VIDEO) {
        return "warning";
    }

    return "danger";
}


function getTrTableClass($status){

    if ($status){
        return "table-success";
    }

    if (!$status){
        return "table-danger";
    }


    return "table-info";
}


function getGenderDefault($gender){

    if ($gender == GenderType::FEMALE){
        return Config::get('custom_file_paths.image_employee_female_default');
    }

    if ($gender == GenderType::MALE){
        return Config::get('custom_file_paths.image_employee_female_default');
    }

    return Config::get('custom_file_paths.image_employee_other_default');
}

function getReadClass($type)
{

    if ($type) {
        return "success";
    }

    if (!$type) {
        return "warning";
    }

    return null;
}

function getReadString($type)
{

    if ($type) {
        return _t('dashboard.Read');
    }

    if (!$type) {
        return _t('dashboard.Not read');
    }

    return null;
}

function myPaginate($items, $perPage = 5, $page = null, $options = [])
{
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $items = $items instanceof Collection ? $items : Collection::make($items);
    return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}

