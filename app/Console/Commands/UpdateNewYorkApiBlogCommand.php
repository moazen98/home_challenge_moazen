<?php

namespace App\Console\Commands;

use App\Enums\SocialEnum;
use Illuminate\Console\Command;

class UpdateNewYorkApiBlogCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateNewYorkApiBlog:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for update the blog for NewYork api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //TODO: Fetch the blogs from the api and store it
        $dataBlogs = app('servicesV1')->socialService->getBlogsNewYorkApi();

        //TODO: Fetch the categories from the api and store it
        app('servicesV1')->categoryService->storeNewyorkCategoriesApi($dataBlogs, SocialEnum::NEW_YORK_API);

        //TODO: Store the source of the blogs
        app('servicesV1')->sourceService->storeNewYorkSourceApi($dataBlogs, SocialEnum::NEW_YORK_API);

        app('servicesV1')->socialService->storeDataNewyorkyType($dataBlogs, SocialEnum::NEW_YORK_API);

        return 1;
    }
}
