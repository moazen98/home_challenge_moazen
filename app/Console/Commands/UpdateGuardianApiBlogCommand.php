<?php

namespace App\Console\Commands;

use App\Enums\SocialEnum;
use Illuminate\Console\Command;

class UpdateGuardianApiBlogCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateGuardianApiBlog:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for update the blog for Guardian api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       //TODO: Fetch the categories from the api and store it
        $dataCategories = app('servicesV1')->categoryService->getGuardianCategoriesApi();
        app('servicesV1')->categoryService->storeGuardianCategoriesApi($dataCategories, SocialEnum::GUARDIAN_API);

       //TODO: Store the source of the blogs
        app('servicesV1')->sourceService->storeGuardianSourceApi($dataCategories, SocialEnum::GUARDIAN_API);

        //TODO: Fetch the blogs from the api and store it
        $dataBlogs = app('servicesV1')->socialService->getBlogsGuardianApi();
        app('servicesV1')->socialService->storeDataGuardianByType($dataBlogs, SocialEnum::GUARDIAN_API,false);

        return 1;
    }
}
