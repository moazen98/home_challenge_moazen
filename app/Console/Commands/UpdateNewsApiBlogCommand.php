<?php

namespace App\Console\Commands;

use App\Enums\SocialEnum;
use Illuminate\Console\Command;

class UpdateNewsApiBlogCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateNewsApiBlog:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for update the blog for NewsApi api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //TODO: Fetch the sources from the api and store it
        $dataSource = app('servicesV1')->sourceService->getSourceNewsApi();
        app('servicesV1')->sourceService->storeSourceNewsApi($dataSource, SocialEnum::NEWS_API);

        //TODO: Fetch the sources from the api and store it
        $dataBlogs = app('servicesV1')->socialService->getBlogsNewsApi();
        app('servicesV1')->socialService->storeDataByType($dataBlogs, SocialEnum::NEWS_API,false);

        return 1;
    }
}
