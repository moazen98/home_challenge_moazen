<?php

namespace App\Console\Commands;

use App\Enums\SocialEnum;
use Illuminate\Console\Command;

class UpdateNewsApiAiBlogCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateNewsApiAiBlog:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for update the blog for News ai api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //TODO: Fetch the category from the api and store it
        $dataCategory = app('servicesV1')->categoryService->getCategoryNewsApiAi();
        app('servicesV1')->categoryService->storeNewsApiAiCategoriesApi($dataCategory,SocialEnum::NEWS_AI_API);

        //TODO: Fetch the sources from the api and store it
        $dataSource = app('servicesV1')->sourceService->getSourceNewsApiAi();
        app('servicesV1')->sourceService->storeSourceNewsApiAi($dataSource, SocialEnum::NEWS_AI_API);

        //TODO: Fetch the sources from the api and store it
        $dataBlogs = app('servicesV1')->socialService->getBlogsNewsApiAi();
        app('servicesV1')->socialService->storeDataNewsAiByType(array_slice($dataBlogs,20,40), SocialEnum::NEWS_AI_API,false);


        return 1;
    }
}
