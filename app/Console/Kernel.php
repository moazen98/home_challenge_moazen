<?php

namespace App\Console;

use App\Console\Commands\UpdateGuardianApiBlogCommand;
use App\Console\Commands\UpdateNewsApiAiBlogCommand;
use App\Console\Commands\UpdateNewsApiBlogCommand;
use App\Console\Commands\UpdateNewYorkApiBlogCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateGuardianApiBlogCommand::class,
        UpdateNewsApiBlogCommand::class,
        UpdateNewYorkApiBlogCommand::class,
        UpdateNewsApiAiBlogCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('updateGuardianApiBlog:cron')->at('01:00');
        $schedule->command('updateNewsApiBlog:cron')->at('02:00');
        $schedule->command('updateNewYorkApiBlog:cron')->at('03:00');
        $schedule->command('updateNewsApiAiBlog:cron')->at('04:00');

        //TODO: The strategy that will be used to cleanup old backups The default strategy  will keep all backups for a certain amount of days After that period only  a daily backup will be kept
        $schedule->command('backup:clean')->daily()->at('01:00');
        $schedule->command('backup:run')->daily()->at('01:30');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
