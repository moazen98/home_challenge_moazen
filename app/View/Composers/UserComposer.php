<?php

namespace App\View\Composers;

use App\Http\Resources\Dashboard\User\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class UserComposer
{

    /**
     * Create a new profile composer.
     *
     * @param
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $auth = Auth::guard('web')->check() ? Auth::guard('web')->user() : null;

        $user = $auth ? (new UserResource($auth))->toArrayLessData() : null;

        $view->with('_auth', $user);
    }
}
