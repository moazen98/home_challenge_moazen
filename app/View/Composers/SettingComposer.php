<?php

namespace App\View\Composers;

use App\Http\Resources\Dashboard\Setting\SettingResource;
use App\Models\Setting\Setting;
use Illuminate\View\View;

class SettingComposer
{


    /**
     * Create a new profile composer.
     *
     * @param
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $settingResult = Setting::first();

        $setting = (new SettingResource($settingResult))->toArray();

        $view->with('_setting', $setting);
    }
}
