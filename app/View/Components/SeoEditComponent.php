<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SeoEditComponent extends Component
{

    public $dataObject;
    public $slug;
    public $keywords;
    public $hideSlug;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($slug = null, $data = null, $keywords = null,$hideSlug=false)
    {

        $KeywordsString = '';

        if ($keywords) {
            foreach ($keywords as $atrr) {
                $KeywordsString .= $atrr->value . ',';
            }
        }

        $this->dataObject = $data;
        $this->slug = $slug;
        $this->keywords = $KeywordsString;
        $this->hideSlug = $hideSlug;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.seo-edit-component');
    }
}
