<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SearchFilterComponent extends Component
{
    public $col;
    public $message;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($col = 4,$message=null)
    {
        $this->col = $col;
        $this->message = $message ?? _t('dashboard.Search');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.search-filter-component');
    }
}
