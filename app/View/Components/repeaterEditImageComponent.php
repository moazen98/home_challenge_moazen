<?php

namespace App\View\Components;

use Illuminate\View\Component;

class repeaterEditImageComponent extends Component
{

    public $dataObject;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->dataObject = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.repeater-edit-image-component');
    }
}
