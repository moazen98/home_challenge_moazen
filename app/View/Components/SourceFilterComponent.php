<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SourceFilterComponent extends Component
{
    public $col;
    public $sources;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($col=4,$sources = null)
    {

        $this->col = $col;
        $this->sources = $sources;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.source-filter-component');
    }
}
