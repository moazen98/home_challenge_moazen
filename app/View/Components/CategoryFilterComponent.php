<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CategoryFilterComponent extends Component
{
    public $col;
    public $categories;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($col=4,$categories = null)
    {

        $this->col = $col;
        $this->categories = $categories;
    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.category-filter-component');
    }
}
