<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SeoComponent extends Component
{
    public $hideSlug;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($hideSlug = false)
    {
        $this->hideSlug = $hideSlug;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.seo-component');
    }
}
