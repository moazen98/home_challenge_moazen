<?php

namespace App\View\Components;

use Illuminate\View\Component;

class RelatedEditComponent extends Component
{
    public $categories;
    public $tags;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($tags = null,$data = null)
    {
        $this->categories = $tags;
        $this->tags = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.related-edit-component');
    }
}
