<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ReadFilterComponent extends Component
{
    public $col;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($col = 6)
    {
        $this->col = $col;
    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.read-filter-component');
    }
}
