<?php

namespace App\Models\Source;

use App\Models\Category\Category;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Source extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use SoftDeletes;

    protected $table = 'sources';
    protected $fillable = ['url', 'country', 'type','slug','category_id','language'];
    public $translatedAttributes = ['name', 'description'];


    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function setTranslatedAttributes($locale=null,$name=null,$description=null)
    {
        $this->getTranslationOrNew($locale)->name = $name;
        $this->getTranslationOrNew($locale)->description = $description;
    }
}
