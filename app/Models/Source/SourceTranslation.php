<?php

namespace App\Models\Source;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SourceTranslation extends Model
{
    use HasFactory;
}
