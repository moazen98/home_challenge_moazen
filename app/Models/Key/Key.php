<?php

namespace App\Models\Key;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Key extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'keys';
    protected $fillable = ['secret_key','is_active','type'];
}
