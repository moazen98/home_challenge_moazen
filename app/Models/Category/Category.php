<?php

namespace App\Models\Category;

use App\Models\Social\Social;
use App\Models\Source\Source;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'categories';
    protected $fillable = ['slug','type'];

    public function socials()
    {
        return $this->belongsToMany(Social::class, 'category_social');
    }

    public function sources(){
        return $this->hasMany(Source::class, 'category_id','id');
    }


}
