<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaExtension extends Model
{
    use HasFactory;
    protected $table = 'media_extensions';
    protected $fillable = ['extension'];


    public function files(){
        return $this->hasMany(File::class,'extension_id','id');
    }
}
