<?php

namespace App\Models\Info;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoTranslation extends Model
{
    use HasFactory;
    protected $table = 'info_translations_table';
}
