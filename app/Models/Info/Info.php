<?php

namespace App\Models\Info;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Info extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    protected $table = 'infos';
    protected $fillable = ['is_active','type'];

    public $translatedAttributes = ['title','description'];


    public function setTranslatedAttributes(Request $request)
    {
        foreach (config('translatable.locales') as $locale) {
            if ($request->has($locale)) {
                $this->getTranslationOrNew($locale)->title = isset($request->get($locale)['title']) ? $request->get($locale)['title'] : null;
                $this->getTranslationOrNew($locale)->description = isset($request->get($locale)['description']) ? $request->get($locale)['description'] : null;
            }

        }
    }

}
