<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TranslationModel extends Model
{
    use HasFactory;
    protected $table = 'translations';
}
