<?php

namespace App\Models;

use App\Enums\MediaFor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class Authentication extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'authentications';
    protected $fillable = ['first_name','last_name','email','phone','international_code','password','is_active','is_verified','last_login_date','is_reset_password','birthdate','email_verified_at'];

    protected $dates = ['last_login_date'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_date' => 'datetime',
        'created_at' => 'datetime'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'image_path',
    ];

    //TODO: morph relation here
    public function authenticatable(){
        return $this->morphTo('authenticatable');
    }


    public function userImage()
    {
        return $this->morphOne(File::class, 'fileable')->where('flag',MediaFor::PERSONAL_IMAGE);
    }


    public function getImagePathAttribute()
    {
        return asset(Config::get('custom_settings.image_employee_default_url') . $this->image_url);

    }//end of get image path


    //TODO: here we use set attribute for password field
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }
}
