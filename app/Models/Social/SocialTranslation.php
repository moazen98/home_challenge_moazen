<?php

namespace App\Models\Social;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialTranslation extends Model
{
    use HasFactory;
    protected $table = 'social_translations';
}
