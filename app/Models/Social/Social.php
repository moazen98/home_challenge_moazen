<?php

namespace App\Models\Social;

use App\Enums\MediaFor;
use App\Models\File;
use App\Models\Source\Source;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;


class Social extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use SoftDeletes;

    protected $table = 'socials';
    protected $fillable = ['type', 'order', 'url', 'source_id', 'published_at'];
    public $translatedAttributes = ['title', 'description', 'author', 'content'];


    public function scoailImage()
    {
        return $this->morphOne(File::class, 'fileable')->where('flag', MediaFor::BLOG_MAIN);
    }

    public function source()
    {
        return $this->belongsTo(Source::class, 'source_id', 'id');
    }

    public function setTranslatedAttributes($locale=null, $title=null , $description=null, $author=null , $content=null)
    {
        $this->getTranslationOrNew($locale)->title = $title;
        $this->getTranslationOrNew($locale)->description = $description;
        $this->getTranslationOrNew($locale)->author = $author;
        $this->getTranslationOrNew($locale)->content = $content;
    }
}
