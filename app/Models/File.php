<?php

namespace App\Models;

use App\Models\Category\Category;
use App\Models\Custom\CustomPage;
use App\Models\Setting\Setting;
use App\Models\Social\Social;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;


class File extends Model
{
    use HasFactory;

    protected $table = 'files';
    protected $fillable = ['url', 'extension_id', 'size', 'real_name', 'fileable_type', 'fileable_id', 'title_ar', 'title_en','media_url','flag'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'file_path',
    ];


    public function fileable()
    {
        return $this->morphTo('fileable');
    }

    public function extension()
    {
        return $this->belongsTo(MediaExtension::class, 'extension_id', 'id');
    }

    public function getFilePathAttribute()
    {
        if ($this->fileable_type == Category::class) {
            return asset(Config::get('custom_file_paths.category_url') . $this->fileable_id . '/' . $this->url);
        }elseif ($this->fileable_type == Authentication::class) {
            return asset(Config::get('custom_file_paths.image_employee') . $this->fileable_id . '/' . $this->url);
        }  elseif ($this->fileable_type == Social::class) {
            return asset(Config::get('custom_file_paths.social_url') . $this->fileable_id . '/' . $this->url);
        }elseif ($this->fileable_type == CustomPage::class) {
            return asset(Config::get('custom_file_paths.custom_url') . $this->fileable_id . '/' . $this->url);
        }elseif ($this->fileable_type == Setting::class) {
            return asset(Config::get('custom_file_paths.image_setting') . $this->fileable_id . '/' . $this->url);
        }else {
            return asset(Config::get('custom_file_paths.images') . $this->fileable_id . '/' . $this->url);
        }
    }
}
