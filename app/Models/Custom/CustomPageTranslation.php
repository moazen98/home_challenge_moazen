<?php

namespace App\Models\Custom;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomPageTranslation extends Model
{
    use HasFactory;
    protected $table = 'custom_page_translations';
}
