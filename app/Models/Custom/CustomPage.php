<?php

namespace App\Models\Custom;

use App\Enums\MediaFor;
use App\Models\File;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CustomPage extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    protected $table = 'custom_pages';
    protected $fillable = ['type'];
    public $translatedAttributes = ['title','sub_title','description'];

    public function customImage(){
        return $this->morphOne(File::class, 'fileable')->where('flag',MediaFor::CUSTOM_PAGE);
    }

    public function setTranslatedAttributes(Request $request)
    {
        foreach (config('translatable.locales') as $locale) {
            if ($request->has($locale)) {
                $this->getTranslationOrNew($locale)->title = isset($request->get($locale)['title']) ? $request->get($locale)['title'] : null;
                $this->getTranslationOrNew($locale)->sub_title = isset($request->get($locale)['sub_title']) ? $request->get($locale)['sub_title'] : null;
                $this->getTranslationOrNew($locale)->description = isset($request->get($locale)['description']) ? $request->get($locale)['description'] : null;
            }
        }
    }
}
