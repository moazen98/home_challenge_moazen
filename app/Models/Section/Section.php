<?php

namespace App\Models\Section;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Section extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use SoftDeletes;

    protected $table = 'sections';
    protected $fillable = ['active','image_url'];
    public $translatedAttributes = ['name','description'];
    protected $appends = ['image_path'];

    public function employees(){
        return $this->hasMany(User::class,'section_id','id');
    }

    public function getImagePathAttribute()
    {
        return $this->image_url ? asset('storage/uploads/sections/' . $this->image_url) : null;

    }//end of get image path

    public function setTranslatedAttributes(Request $request)
    {
        foreach (config('translatable.locales') as $locale) {
            if ($request->has($locale)) {
                $this->getTranslationOrNew($locale)->description = isset($request->get($locale)['description']) ? $request->get($locale)['description'] : null;
                $this->getTranslationOrNew($locale)->name = isset($request->get($locale)['name']) ? $request->get($locale)['name'] : null;
            }

        }
    }
}
