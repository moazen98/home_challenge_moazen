<?php

namespace App\Models\Setting;

use App\Enums\MediaFor;
use App\Models\File;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Setting extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    protected $table = 'settings';
    protected $fillable = ['longitude','latitude','slug'];
    public $translatedAttributes = ['title','address','description'];

    public function englishLogo(){
        return $this->morphOne(File::class, 'fileable')->where('flag',MediaFor::LOGO_ENGLISH);
    }

    public function arabicLogo(){
        return $this->morphOne(File::class, 'fileable')->where('flag',MediaFor::LOGO_ARABIC);
    }

    public function englishIcon(){
        return $this->morphOne(File::class, 'fileable')->where('flag',MediaFor::ICON_ENGLISH);
    }

    public function arabicIcon(){
        return $this->morphOne(File::class, 'fileable')->where('flag',MediaFor::ICON_ARABIC);
    }


    public function setTranslatedAttributes(Request $request)
    {
        foreach (config('translatable.locales') as $locale) {
            if ($request->has($locale)) {
                $this->getTranslationOrNew($locale)->title = isset($request->get($locale)['title']) ? $request->get($locale)['title'] : null;
                $this->getTranslationOrNew($locale)->description = isset($request->get($locale)['description']) ? $request->get($locale)['description'] : null;
            }

        }
    }
}
