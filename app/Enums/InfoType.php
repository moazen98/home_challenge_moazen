<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class InfoType extends Enum
{
    const LINKED_IN =   'linked_in';
    const EMAIL =   'email';
    const MOBILE =   'mobile';
}
