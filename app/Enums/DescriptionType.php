<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class DescriptionType extends Enum
{
    const BLOG =   'Blog';
    const DESERVE =   'Deserve';
    const LEARN =   'Learn';
}
