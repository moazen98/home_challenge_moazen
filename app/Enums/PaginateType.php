<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class PaginateType extends Enum
{
    const FIRST =   15;
    const SECOND =   25;
    const THIRD = 30;
}
