<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class LanguageType extends Enum
{
    const AR = 'ar';
    const EN = 'en';
    const DE = 'de';
    const ES = 'es';
    const FR = 'fr';
    const HE = 'he';
    const IT = 'it';
    const NL = 'nl';
    const NO = 'no';
    const PT = 'pt';
    const RU = 'ru';
    const SV = 'sv';
    const UD = 'ud';
    const ZH = 'zh';
}
