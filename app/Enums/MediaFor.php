<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class MediaFor extends Enum
{
    const EMPLOYEE = 'employee';
    const CUSTOMER = 'customer';
    const PERSONAL_IMAGE = 'Personal image';
    const LOGO_ENGLISH = 'Logo english';
    const LOGO_ARABIC = 'Logo arabic';
    const ICON_ENGLISH = 'Icon english';
    const ICON_ARABIC = 'Icon arabic';
    const SLIDER_MAIN = 'Slider main';
    const TESTIMONIAL = 'Testimonial';
    const CLIENT = 'Client';
    const COUNTER = 'Counter';
    const CATEGORY_MAIN = 'Category main';
    const BLOG_MAIN = 'Blog main';
    const CUSTOM_PAGE = 'Custom page';
    const PAGE_HEADER_IMAGE = 'Page header image';
    const PAGE_FIRST_IMAGE = 'Page first image';
    const PAGE_SECOND_IMAGE = 'Page second image';
}
