<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class SocialEnum extends Enum
{
    const NEWS_API =   'NewsAPI';
    const GUARDIAN_API =   'guardianApi';
    const NEW_YORK_API =   'newYork';
    const NEWS_AI_API =   'NewsAPIAI';
}
