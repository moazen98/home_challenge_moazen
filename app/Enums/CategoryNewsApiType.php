<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class CategoryNewsApiType extends Enum
{
    const BUSINESS =   'business';
    const ENTERTAINMENT =   'entertainment';
    const GENERAL = 'general';
    const HEALTH = 'health';
    const SCIENCE = 'science';
    const SPORTS = 'sports';
    const TECHNOLOGY = 'technology';
}
