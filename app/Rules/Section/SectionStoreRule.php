<?php

namespace App\Rules\Section;

use Illuminate\Contracts\Validation\Rule;

class SectionStoreRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $rules = [];


        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.name' => ['required', \Illuminate\Validation\Rule::unique('section_translations', 'name')]];

        }//end of for each

//        dump($rules);die();

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
