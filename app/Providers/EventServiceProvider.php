<?php

namespace App\Providers;

use App\Event\NotificationAllUserStoreEvent;
use App\Listeners\NotificationAllUserStoreListener;
use App\Models\Agency\Branch;
use App\Models\Agency\Dealership;
use App\Models\Agency\Office;
use App\Observers\BranchObserver;
use App\Observers\DealershipObserver;
use App\Observers\OfficeObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NotificationAllUserStoreEvent::class => [
            NotificationAllUserStoreListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
    }
}
