<?php

namespace App\Exports;

use App\Http\Resources\Dashboard\Role\RoleCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RoleExport implements FromArray, WithHeadings
{
    public function array(): array
    {
        $data = array();

        $roles = app('servicesV1')->roleService->getAllRoles()->get();
        $roles = (new RoleCollection($roles))->toArray();

        foreach ($roles['roles'] as $index => $role) {
            $data[$index]['id'] = $role['id'];
            $data[$index]['name'] = $role['name'];
            $data[$index]['description'] = $role['description'];
            $data[$index]['employees_number'] = $role['employees_number'] ?? '-';
            $data[$index]['created_at'] = $role['created_at'];
        }

        return $data;
    }

    public function headings(): array
    {
        return [_t("dashboard.ID"), _t("dashboard.Name"), _t("dashboard.Description"), _t("dashboard.Employees number"),_t("dashboard.Date")];
    }
}
