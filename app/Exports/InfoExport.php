<?php

namespace App\Exports;

use App\Http\Resources\Dashboard\Info\InfoCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InfoExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $result = app('servicesV1')->infoService->getAllData()->get();

        $result =  (new InfoCollection($result))->toArray();

        $data = array();

        foreach ($result['infos'] as $index => $info) {
            $data[$index]['id'] = $info['id'];
            $data[$index]['title'] = $info['title'];
            $data[$index]['description'] = strip_tags($info['description']);
            $data[$index]['type_string'] = $info['type_string'];
            $data[$index]['is_active_string'] = $info['is_active_string'];
            $data[$index]['date'] = $info['date'];
        }

        return $data;
    }

    public function headings(): array
    {
        return [_t("dashboard.Id"),  _t("dashboard.Title"),  _t("dashboard.Description"),  _t("dashboard.Type"), _t("dashboard.Status"),_t("dashboard.Date")];
    }
}
