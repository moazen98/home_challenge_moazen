<?php

namespace App\Exports;

use App\Http\Resources\Dashboard\Section\SectionCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SectionExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $sections = app('servicesV1')->sectionService->getAllSections()->get();
        $sections =  (new SectionCollection($sections))->toArray();

        foreach ($sections['sections'] as $index => $section) {
            $data[$index]['id'] = $section['id'];
            $data[$index]['name'] = $section['name'];
            $data[$index]['description'] = $section['description'];
            $data[$index]['employees_count'] = $section['employees_count'];
            $data[$index]['active'] = $section['is_active_string'];
            $data[$index]['date'] = $section['date'];
        }

        return $data;
    }

    public function headings(): array
    {
        return [_t("dashboard.ID"), _t("dashboard.Name"), _t("dashboard.Description"), _t("dashboard.Employees Number"),_t("dashboard.Status"),_t("dashboard.Date")];
    }
}
