<?php

namespace App\Exports;

use App\Http\Resources\Dashboard\Social\SocialCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SocialExport implements FromArray, WithHeadings
{
    public $type;

    public function __construct($type)
    {
        $this->type = $type;
    }//end of constructor

    public function array(): array
    {

        $result = app('servicesV1')->socialService->getAllDataByType($this->type)->get();

        $result =  (new SocialCollection($result))->toArray();

        $data = array();

        foreach ($result['socials'] as $index => $item) {
            $data[$index]['id'] = $item['id'];
            $data[$index]['title'] = $item['title'];
            $data[$index]['description'] = $item['description'];
            $data[$index]['content'] = $item['content'];
            $data[$index]['author'] = $item['author'];
            $data[$index]['type_string'] = $item['type_string'];
            $data[$index]['source_name'] = $item['source_name'];
            $data[$index]['category_name'] = $item['category_name'];
            $data[$index]['url'] = $item['url'];
            $data[$index]['image_url'] = $item['type_string'];
            $data[$index]['order'] = $item['order'];
            $data[$index]['published_at_format'] = $item['published_at_format'];
        }

        return $data;
    }

    public function headings(): array
    {
        return [_t("dashboard.Id"),  _t("dashboard.Title"),  _t("dashboard.Description"),  _t("dashboard.Content"), _t("dashboard.Author"),_t("dashboard.Type"),_t("dashboard.Source"),_t("dashboard.Category"),_t("dashboard.Url"),_t("dashboard.Image"),_t("dashboard.Order"),_t("dashboard.Publish date")];
    }
}
