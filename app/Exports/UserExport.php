<?php

namespace App\Exports;

use App\Http\Resources\Dashboard\User\UserCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $users = app('servicesV1')->userService->getAllUsers()->get();
        $users = (new UserCollection($users))->toArray();

        foreach ($users['users'] as $index => $user) {
            $data[$index]['id'] = $user['id'];
            $data[$index]['first_name'] = $user['first_name'];
            $data[$index]['last_name'] = $user['last_name'];
            $data[$index]['job_title'] = $user['job_title'];
            $data[$index]['phone'] = $user['full_phone'];
            $data[$index]['email'] = $user['email'];
            $data[$index]['address'] = $user['address'];
            $data[$index]['note'] = $user['note'];
            $data[$index]['active'] = $user['is_active_string'];
            $data[$index]['section_id'] =  $user['section_name'];
            $data[$index]['date'] = $user['created_at'];

        }

        return $data;
    }

    public function headings(): array
    {
        return [_t("dashboard.ID"), _t("dashboard.First name"), _t("dashboard.Last name"), _t("dashboard.Job title"), _t("dashboard.Phone"), _t("dashboard.Email"), _t("dashboard.Address"), _t("dashboard.Note"), _t("dashboard.Status"), _t("dashboard.Section"), _t("dashboard.Date")];
    }
}
