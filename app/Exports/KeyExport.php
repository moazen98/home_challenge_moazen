<?php

namespace App\Exports;

use App\Http\Resources\Dashboard\key\KeyCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class KeyExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $result = app('servicesV1')->keyService->getAllData()->get();

        $result =  (new KeyCollection($result))->toArray();

        $data = array();

        foreach ($result['keys'] as $index => $info) {
            $data[$index]['id'] = $info['id'];
            $data[$index]['secret_key'] = $info['secret_key'];
            $data[$index]['is_active_string'] = $info['is_active_string'];
            $data[$index]['date'] = $info['date'];
        }

        return $data;
    }

    public function headings(): array
    {
        return [_t("dashboard.Id"),  _t("dashboard.Secret key"), _t("dashboard.Status"),_t("dashboard.Date")];
    }
}
