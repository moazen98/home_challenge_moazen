<?php

namespace App\Http\Requests\Admin\Category;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return RuleFactory::make([
            '%name%' => 'required',
            'order' => 'required|numeric',
            'slug' => 'nullable|unique:categories,slug',
        ]);

    }


    public function attributes()
    {
        return RuleFactory::make([
            '%name%' => __('validation.attributes.name'),
        ]);
    }

    public function messages()
    {
        return [
            '*.name.required' => _t('validation.The field is required'),
            '*.title.required' => _t('validation.The field is required'),
            'order.required' => _t('validation.The field is required'),
            'order.numeric' => _t('validation.The field must be a number'),
            'type.required' => _t('validation.The field is required'),
            'slug.unique' => _t('validation.This value was already taken'),
        ];
    }
}
