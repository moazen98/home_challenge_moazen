<?php

namespace App\Http\Requests\Admin\Tag;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class TagStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return RuleFactory::make([
            '%title%' => ['required'],
        ]);

    }

    public function attributes()
    {
        return RuleFactory::make([
            '%title%' => __('validation.attributes.title'),
        ]);
    }

    public function messages()
    {
        return [
            '*.title.required' => _t('validation.The field is required'),
        ];
    }
}
