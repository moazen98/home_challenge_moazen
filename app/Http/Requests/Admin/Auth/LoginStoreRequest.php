<?php

namespace App\Http\Requests\Admin\Auth;

use App\Enums\CustomerVerificationMethod;
use Illuminate\Foundation\Http\FormRequest;

class LoginStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = inputCredentialType($this);

        if ($method == 'email') {

            $this->merge(['email' => $this->input('credential')]);

            return [
                'credential' => 'required|email',
                'password' => 'required|min:6'
            ];

        } elseif ($method == 'phone') {

            $this->merge(['phone' => trim($this->input('credential'))]);

            return [
                'credential' => 'required|numeric',
                'password' => 'required|min:6'
            ];
        } else {

            return [
                'credential' => 'required|email',
                'password' => 'required|min:6'
            ];
        }
    }

    public function messages()
    {
        return [
            'email.required' => _t('validation.The field is required'),
            'email.email' => _t('validation.The field must be an email'),
            'password.required' => _t('validation.The field is required'),
            'password.min' => _t('validation.Password Field Must be greater than 6 character'),
        ];
    }
}
