<?php

namespace App\Http\Requests\Admin\Social;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class SocialStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RuleFactory::make([
            'language' => 'required',
            'slug' => 'nullable|unique:socials,slug',
        ]);
    }


    public function attributes()
    {
        return RuleFactory::make([
            '%description%' => __('validation.attributes.description'),
        ]);
    }

    public function messages()
    {
        return [
            'language.required' => _t('validation.The field is required'),
            'slug.required' => _t('validation.The field is required'),
            'slug.unique' => _t('validation.This value was already taken'),
        ];
    }
}
