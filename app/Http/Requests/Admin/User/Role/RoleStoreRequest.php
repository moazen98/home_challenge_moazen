<?php

namespace App\Http\Requests\Admin\User\Role;

use Illuminate\Foundation\Http\FormRequest;

class RoleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar' => 'required',
            'name' => 'required',
            'description_ar' => 'required',
            'description' => 'required',
            'permissions' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name_ar.required' => _t('validation.The field is required'),
            'name.required' => _t('validation.The field is required'),
            'description_ar.required' => _t('validation.The field is required'),
            'description.required' => _t('validation.The field is required'),
            'permissions.required' => _t('validation.The field is required'),
        ];
    }
}
