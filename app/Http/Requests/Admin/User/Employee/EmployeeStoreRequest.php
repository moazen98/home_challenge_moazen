<?php

namespace App\Http\Requests\Admin\User\Employee;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'section' => 'required',
            'email' => 'required|email|unique:authentications,email',
            'password' => 'required|min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required|min:6',
            'role' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => _t('validation.The field is required'),
            'last_name.required' => _t('validation.The field is required'),
            'section.required' => _t('validation.The field is required'),
            'international_code.required' => _t('validation.The field is required'),
            'email.required' => _t('validation.The field is required'),
            'email.email' => _t('validation.The field must be an email'),
            'email.unique' => _t('validation.This value was already taken'),
            'password.required' => _t('validation.The field is required'),
            'password.min' => _t('validation.Password Field Must be greater than 6 character'),
            'password.same' => _t('validation.The Password is mismatch'),
            'confirm_password.min' => _t('validation.Password Field Must be greater than 6 character'),
            'confirm_password.required' => _t('validation.The field is required'),
            'role.required' => _t('validation.The field is required'),
        ];
    }
}
