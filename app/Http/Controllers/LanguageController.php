<?php

namespace App\Http\Controllers;

class LanguageController extends Controller
{


    public function swap($locale)
    {
        // available language in template array
        $availLocale = ['en' => 'en', 'ar' => 'ar', 'de' => 'de', 'pt' => 'pt'];
        // check for existing language
        if (array_key_exists($locale, $availLocale)) {
            session()->put('locale', $locale);
            if ($locale == 'ar') {
                config(['MIX_CONTENT_DIRECTION' => 'rtl']);
                session()->put('direction', 'rtl');
                session()->put('locale',$locale);
            } else {
                config(['MIX_CONTENT_DIRECTION' => 'ltr']);
                session()->put('direction', 'ltr');
                session()->put('locale',$locale);
            }
        }

        return redirect()->back();
    }
}
