<?php

namespace App\Http\Controllers;

use App\Enums\InquiryType;
use App\Enums\SocialEnum;
use App\Http\Resources\Dashboard\Info\InfoCollection;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    // Dashboard - Analytics
    public function dashboardAnalytics()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/dashboard/dashboard-analytics', ['pageConfigs' => $pageConfigs]);
    }

    // Dashboard - Ecommerce
    public function dashboardEcommerce()
    {
        $pageConfigs = ['pageHeader' => false];

        $users = app('servicesV1')->userService->getAllUsers()->count();
        $roles = app('servicesV1')->roleService->getAllRoles()->count();
        $sections = app('servicesV1')->sectionService->getAllSections()->count();
        $blogs = app('servicesV1')->socialService->getAllData()->count();
        $infos = app('servicesV1')->infoService->getAllData()->get();
        $infos = (new InfoCollection($infos))->toArray();


        return view('content.dashboard.dashboard-ecommerce', compact('pageConfigs','infos', 'users', 'roles', 'sections','blogs'));
    }

    public function logout()
    {

        app('servicesV1')->authenticationService->logout();

        return redirect()->route('admin.get.login');

    }
}
