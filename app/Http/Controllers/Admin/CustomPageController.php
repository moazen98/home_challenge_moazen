<?php

namespace App\Http\Controllers\Admin;

use App\Enums\CustomPageType;
use App\Http\Requests\Admin\Custom\CustomStoreRequest;
use App\Http\Resources\Dashboard\Custom\CustomResource;
use Illuminate\Http\Request;

class CustomPageController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_custom'])->only('index');
        $this->middleware(['permission:create_custom'])->only('create');
        $this->middleware(['permission:update_custom'])->only('edit');
        $this->middleware(['permission:delete_custom'])->only('destroy');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['name' => _t('dashboard.Custom pages')]
        ];

        $data = app('servicesV1')->customService->getAllData()->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) {
            return (new CustomResource($item))->toArray();
        });

        return view('admin.custom.index', compact('data', 'breadcrumbs'));
    }

    public function fetchData(Request $request)
    {

        $data = app('servicesV1')->customService->filterData($request)->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) {
            return (new CustomResource($item))->toArray();
        });

        return view('admin.custom.include.pagination_data', compact('data'))->render();
    }

    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('custom.index'), 'name' => _t('dashboard.Custom pages')],
            ['name' => _t('dashboard.Create')]
        ];

        $types = CustomPageType::getValues();
        $customs = app('servicesV1')->customService->getAllCustomsStore();

        return view('admin.custom.create', compact('breadcrumbs','customs','types'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomStoreRequest $request)
    {
        $result = app('servicesV1')->customService->storeData($request);

        if ($result) {
            return redirect()->route('custom.index')->with('success', _t('message.The data created successful'));
        }

        return redirect()->route('custom.index')->with('failed', _t('message.There is an error'));
    }

    public function show($id)
    {
        $data = app('servicesV1')->customService->findData($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('custom.index'), 'name' => _t('dashboard.Custom pages')],
            ['name' => _t('dashboard.Show')],
            ['name' => "#" . $data->id . ' - ' . $data->created_at->format('m/d/Y')]

        ];

        $data = (new CustomResource($data))->toArray();

        return view('admin.custom.show', compact('data', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = app('servicesV1')->customService->findData($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('custom.index'), 'name' => _t('dashboard.Custom pages')],
            ['name' => _t('dashboard.Edit')],
            ['name' => "#" . $data->id . ' - ' . $data->created_at->format('m/d/Y')]

        ];

        $data = (new CustomResource($data))->toArray();
        $types = CustomPageType::getValues();

        $customs = app('servicesV1')->customService->getAllCustomsStore();


        return view('admin.custom.edit', compact('data','customs','types','breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomStoreRequest $request, $id)
    {

        $result = app('servicesV1')->customService->updateData($id,$request);

        if ($result) {
            return redirect()->route('custom.index')->with('success', _t('message.The data updated successful'));
        }
        return redirect()->route('custom.index')->with('failed', _t('message.There is an error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = app('servicesV1')->customService->deleteData($id);

        if ($result) {
            return redirect()->route('custom.index')->with('success', _t('message.The data deleted successful'));
        }
        return redirect()->route('custom.index')->with('failed', _t('message.There is an error'));
    }

}
