<?php

namespace App\Http\Controllers\Admin\Area;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;

class AreaController extends Controller
{

    public function index(Request $request){

        $request->validate([
            'country_id' => 'required'
        ]);

        $country_id = $request->input('country_id');

        if (app()->getLocale() == 'ar') {
            $country = Country::where('id', $country_id)->first();
        } else {
            $country = Country::where('id', $country_id)->first();
        }

        $cities = $country->cities;

        return $cities;
    }
}
