<?php

namespace App\Http\Controllers\Admin;

use App\Enums\SocialEnum;
use App\Exports\SocialExport;
use App\Http\Resources\Dashboard\Category\CategoryCollection;
use App\Http\Resources\Dashboard\Ordering\OrderingCollection;
use App\Http\Resources\Dashboard\Social\SocialResource;
use App\Http\Resources\Dashboard\Source\SourceCollection;
use App\Models\Social\Social;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class NewsApiAiController extends MainDashboardController
{

    public $type;

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_blog'])->only('index');
        $this->middleware(['permission:create_blog'])->only('create');
        $this->middleware(['permission:update_blog'])->only('edit');
        $this->middleware(['permission:delete_blog'])->only('destroy');
        $this->middleware(['permission:read_blog'])->only('export');
        $this->middleware(['permission:update_blog'])->only('reorder');
        $this->middleware(['permission:update_blog'])->only('reorderPost');

        $this->type = SocialEnum::NEWS_AI_API;

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['name' => _t('dashboard.News api ai blogs')]
        ];

        $categories = app('servicesV1')->categoryService->getAllDataByType($this->type)->get();
        $categories = (new CategoryCollection($categories))->toArray();

        $sources = app('servicesV1')->sourceService->getSourcesDataByType($this->type)->get();
        $sources = (new SourceCollection($sources))->toArray();

        $blogs = app('servicesV1')->socialService->getAllDataByType($this->type)->paginate($this->dashboardPaginate);
        $blogs->getCollection()->transform(function ($item) {
            return (new SocialResource($item))->toArray();
        });


        return view('admin.ai.index', compact('blogs', 'categories', 'sources', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {

        $blogs = app('servicesV1')->socialService->filterData($request, $this->type)->paginate($this->dashboardPaginate);

        $blogs->getCollection()->transform(function ($item) {
            return (new SocialResource($item))->toArray();
        });

        return view('admin.ai.include.pagination_data', compact('blogs'))->render();
    }


    public function show($id)
    {
        $data = app('servicesV1')->socialService->getDataByType($id,$this->type);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('social.blog.ai.index'), 'name' => _t('dashboard.News api ai blogs')],
            ['name' => _t('dashboard.Show')],
            ['name' => "#" . $data->id . ' - ' . $data->created_at->format('m/d/Y')]

        ];

        $data = (new SocialResource($data))->toArray();

        return view('admin.ai.show', compact('data', 'breadcrumbs'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = app('servicesV1')->socialService->deleteDataByType($id, $this->type);

        if ($result) {
            return redirect()->route('social.blog.ai.index')->with('success', _t('message.The data deleted successful'));
        }
        return redirect()->route('social.blog.ai.index')->with('failed', _t('message.There is an error'));
    }

    public function reorder()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('social.blog.ai.index'), 'name' => _t('dashboard.News api ai blogs')],
            ['name' => _t('dashboard.Reordering')]
        ];

        $items = app('servicesV1')->socialService->getDataByTypeOrder($this->type)->get();
        $items = (new OrderingCollection($items))->toArray();
        $title = _t('dashboard.Blogs');
        $route = route('social.blog.ai.reorder-post');

        return view('admin.reorder.index', compact('title', 'route', 'items', 'breadcrumbs'));
    }

    public function reorderPost(Request $request)
    {

        $process = app('servicesV1')->orderingService->updateOrderingWithType($request, new Social(), $this->type);

        if ($process) {
            return response()->json(['message' => _t('dashboard.Order updated')]);
        }
        return response()->json(['message' => _t('dashboard.Order failed')]);
    }

    public function updateBlogData(){


        //TODO: Fetch the category from the api and store it
        $dataCategory = app('servicesV1')->categoryService->getCategoryNewsApiAi();
        app('servicesV1')->categoryService->storeNewsApiAiCategoriesApi($dataCategory,$this->type);

        //TODO: Fetch the sources from the api and store it
        $dataSource = app('servicesV1')->sourceService->getSourceNewsApiAi();
        app('servicesV1')->sourceService->storeSourceNewsApiAi($dataSource, $this->type);

        //TODO: Fetch the sources from the api and store it
        $dataBlogs = app('servicesV1')->socialService->getBlogsNewsApiAi();
        app('servicesV1')->socialService->storeDataNewsAiByType(array_slice($dataBlogs,20,35), $this->type);

        return redirect()->route('social.blog.ai.index')->with('success', _t('message.The data updated successful'));
    }


    public function export()
    {
        return Excel::download(new SocialExport($this->type), _t('dashboard.News api ai blogs') . '.xlsx');
    }
}
