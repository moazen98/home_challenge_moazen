<?php

namespace App\Http\Controllers\Admin;

use Alaaeta\Translation\Facades\Translation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TranslationController extends Controller
{

    public function __construct(Request $request)
    {

        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_translation'])->only('index');
        $this->middleware(['permission:update_translation'])->only('edit');
        $this->middleware(['permission:read_translation'])->only('show');
        $this->middleware(['permission:delete_translation'])->only('destroy');
    }//end of constructor


    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['name' => _t('dashboard.Translations')]
        ];

        if ($request->has('lang_name') || $request->has('lang_file_name')){

            if ($request->lang_name == 'NON'){
                $request->request->remove('lang_name');
            }

            if ($request->lang_file_name == 'NON'){
                $request->request->remove('lang_file_name');
            }
        }


        $locales = config('translation')['locales'];
        $translationTypes = Translation::getTypes();


        request()->pageNumber = 50;

        $translations = Translation::getTranslations();

        return view('admin.translation.index', compact('locales', 'breadcrumbs', 'translationTypes', 'translations'));
    }

    public function edit($id)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('translation.index'), 'name' => _t('dashboard.Translations')],
            ['name' => _t('dashboard.Edit')]
        ];

        $translations = app('servicesV1')->translationService->getTranslation($id);

        return view('admin.translation.edit', compact('translations', 'breadcrumbs'));
    }

    public function update($key, Request $request)
    {

        $result = app('servicesV1')->translationService->updateTranslation($key, $request);

        if ($result) {
            return redirect()->route('translation.index')->with('success', _t('message.The data updated successful'));
        }

        return redirect()->route('translation.index')->with('failed', _t('message.There is an error'));
    }


    public function destroy($id,Request $request)
    {

        $result = app('servicesV1')->translationService->deleteTranslation($id);

        if ($result) {
            return redirect()->route('translation.index')->with('success', _t('message.The data updated successful'));
        }
        return redirect()->route('translation.index')->with('failed', _t('message.There is an error'));
    }
}
