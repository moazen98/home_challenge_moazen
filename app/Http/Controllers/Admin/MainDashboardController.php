<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class MainDashboardController extends Controller
{

    protected $dashboardPaginate = 15;

    public function __construct()
    {
        $this->dashboardPaginate = config('custom_settings.dashboard_paginate');
    }
}
