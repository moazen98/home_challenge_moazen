<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Setting\SettingStoreRequest;
use App\Http\Resources\Dashboard\Setting\SettingResource;
use Illuminate\Http\Request;

class SettingController extends MainDashboardController
{


    public function __construct(Request $request)
    {
        //create read update delete
        $this->middleware(['permission:read_setting'])->only('index');
        $this->middleware(['permission:update_setting'])->only('update');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Setting')]
        ];

        $result = app('servicesV1')->settingService->getSetting();

        $common = (new SettingResource($result))->toArray();

        return view('admin.setting.index', compact('common', 'breadcrumbs'));
    }


    public function update(SettingStoreRequest $request)
    {
        $result = app('servicesV1')->settingService->updateSetting($request);

        if ($result) {
            return redirect()->route('setting.index')->with('success', __('message.update'));

        } else {
            return redirect()->route('setting.index')->with('failed', __('message.failed'));
        }
    }
}
