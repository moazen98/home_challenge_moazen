<?php

namespace App\Http\Controllers\Admin;

use App\Enums\InfoType;
use App\Exports\InfoExport;
use App\Exports\SocialExport;
use App\Http\Resources\Dashboard\Info\InfoResource;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InfoController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_info'])->only('index');
        $this->middleware(['permission:create_info'])->only('create');
        $this->middleware(['permission:update_info'])->only('edit');
        $this->middleware(['permission:delete_info'])->only('destroy');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['name' => _t('dashboard.Infos')]
        ];

        $data = app('servicesV1')->infoService->getAllData()->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) {
            return (new InfoResource($item))->toArray();
        });

        return view('admin.info.index', compact('data', 'breadcrumbs'));
    }

    public function fetchData(Request $request)
    {

        $data = app('servicesV1')->infoService->filterData($request)->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) {
            return (new InfoResource($item))->toArray();
        });

        return view('admin.info.include.pagination_data', compact('data'))->render();
    }

    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('info.index'), 'name' => _t('dashboard.Infos')],
            ['name' => _t('dashboard.Create')]
        ];

        $types = InfoType::getValues();
        $infos = app('servicesV1')->infoService->getAllTypeStore();

        return view('admin.info.create', compact('breadcrumbs','types','infos'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = app('servicesV1')->infoService->storeData($request);

        if ($result) {
            return redirect()->route('info.index')->with('success', _t('message.The data created successful'));
        }

        return redirect()->route('info.index')->with('failed', _t('message.There is an error'));
    }

    public function show($id)
    {
        $data = app('servicesV1')->infoService->findData($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('info.index'), 'name' => _t('dashboard.Infos')],
            ['name' => _t('dashboard.Show')],
            ['name' => "#" . $data->id . ' - ' . $data->created_at->format('m/d/Y')]

        ];

        $data = (new InfoResource($data))->toArray();

        return view('admin.info.show', compact('data', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = app('servicesV1')->infoService->findData($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('info.index'), 'name' => _t('dashboard.Infos')],
            ['name' => _t('dashboard.Edit')],
            ['name' => "#" . $data->id . ' - ' . $data->created_at->format('m/d/Y')]

        ];

        $data = (new InfoResource($data))->toArray();
        $types = InfoType::getValues();
        $infos = app('servicesV1')->infoService->getAllTypeStore();

        return view('admin.info.edit', compact('data','types','infos','breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $result = app('servicesV1')->infoService->updateData($request,$id);

        if ($result) {
            return redirect()->route('info.index')->with('success', _t('message.The data updated successful'));
        }
        return redirect()->route('info.index')->with('failed', _t('message.There is an error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = app('servicesV1')->infoService->deleteData($id);

        if ($result) {
            return redirect()->route('info.index')->with('success', _t('message.The data deleted successful'));
        }
        return redirect()->route('info.index')->with('failed', _t('message.There is an error'));
    }


    public function export()
    {

        return Excel::download(new InfoExport(), _t('dashboard.Infos') . '.xlsx');
    }

}
