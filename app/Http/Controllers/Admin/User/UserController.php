<?php

namespace App\Http\Controllers\Admin\User;

use App\Exports\UserExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Requests\Admin\User\Employee\EmployeeStoreRequest;
use App\Http\Requests\Admin\User\Employee\EmployeeUpdateRequest;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\Role\RoleCollection;
use App\Http\Resources\Dashboard\Section\SectionCollection;
use App\Http\Resources\Dashboard\User\UserResource;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends MainDashboardController
{

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_employee'])->only('index');
        $this->middleware(['permission:create_employee'])->only('create');
        $this->middleware(['permission:update_employee'])->only('edit');
        $this->middleware(['permission:delete_employee'])->only('destroy');
        $this->middleware(['permission:read_employee'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['name' => _t('dashboard.Employees')]
        ];

        $data = app('servicesV1')->userService->getAllUsers()->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) use ($request) {
            return (new UserResource($item))->toArray($request);
        });

        $roles = app('servicesV1')->roleService->getAllRoles($request)->get();
        $roles = (new RoleCollection($roles))->toArray();

        return view('admin.user.employee.index', compact('data','roles', 'breadcrumbs'));
    }

    public function fetchData(Request $request)
    {
        $data = app('servicesV1')->userService->filterUsers($request)->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) use ($request) {
            return (new UserResource($item))->toArray($request);
        });

        return view('admin.user.employee.include.pagination_data', compact('data'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('employee.index'), 'name' => _t('dashboard.Employees')],
            ['name' => _t('dashboard.Create')]
        ];

        $roles = app('servicesV1')->roleService->getAllRoles()->get();
        $roles = (new RoleCollection($roles))->toArray();

        $countries = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countries))->toArray();

        $sections = app('servicesV1')->sectionService->getAllActiveSection()->get();
        $sections = (new SectionCollection($sections))->toArray();

        return view('admin.user.employee.create', compact('roles', 'sections', 'countries', 'sections', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStoreRequest $request)
    {
        $result = app('servicesV1')->userService->storeUser($request);

        if ($result) {
            return redirect()->route('employee.index')->with('success', _t('message.The data created successful'));
        }
        return redirect()->route('employee.index')->with('failed', _t('message.There is an error'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $employeeData = app('servicesV1')->userService->getUserById($id);
        $employee = (new UserResource($employeeData))->toArray();
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('employee.index'), 'name' => _t('dashboard.Employees')],
            ['name' => _t('dashboard.Show')],
            ['name' => "#" . $employee['id'] . ' - ' . $employee['first_name'] . ' ' . $employee['last_name'] . ' - ' . $employee['created_at']]

        ];

        return view('admin.user.employee.show', compact('employee', 'breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $employeeData = app('servicesV1')->userService->getUserById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('employee.index'), 'name' => _t('dashboard.Employees')],
            ['name' => _t('dashboard.Edit')],
            ['name' => "#" . $employeeData->id . ' - ' . $employeeData->first_name . ' ' . $employeeData->last_name . ' - ' . $employeeData->created_at->format('m/d/Y')]

        ];

        $employee = (new UserResource($employeeData))->toArray();

        $roles = app('servicesV1')->roleService->getAllRoles()->get();
        $roles = (new RoleCollection($roles))->toArray();

        $countries = app('servicesV1')->locationService->getAllCountries();
        $countries = (new CountryCollection($countries))->toArray();

        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($employeeData->country->id);
        $cities = (new CityCollection($citiesData))->toArray();

        $sections = app('servicesV1')->sectionService->getAllActiveSection()->get();
        $sections = (new SectionCollection($sections))->toArray();


        return view('admin.user.employee.edit', compact('employee', 'roles', 'countries', 'sections', 'cities', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, $id)
    {

        $result = app('servicesV1')->userService->updateUser($request, $id);

        if ($result) {
            return redirect()->route('employee.index')->with('success', _t('message.The data updated successful'));
        }

        return redirect()->route('employee.index')->with('failed', _t('message.There is an error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = app('servicesV1')->userService->destroyUserById($id);

        if ($result) {
            return redirect()->route('employee.index')->with('success', _t('message.The data deleted successful'));
        }

        return redirect()->route('employee.index')->with('failed', _t('message.There is an error'));
    }

    public function export()
    {
        return Excel::download(new UserExport(), _t('dashboard.Employees') . '.xlsx');
    }
}
