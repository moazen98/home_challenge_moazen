<?php

namespace App\Http\Controllers\Admin\User;

use App\Exports\SectionExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Requests\Admin\User\Section\SectionStoreRequest;
use App\Http\Requests\Admin\User\Section\SectionUpdateRequest;
use App\Http\Resources\Dashboard\Section\SectionRescourse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SectionController extends MainDashboardController
{

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_section'])->only('index');
        $this->middleware(['permission:read_section'])->only('export');
        $this->middleware(['permission:create_section'])->only('create');
        $this->middleware(['permission:update_section'])->only('edit');
        $this->middleware(['permission:delete_section'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['name' => _t('dashboard.Sections')]
        ];

        $data = app('servicesV1')->sectionService->getAllSections()->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) use ($request) {
            return (new SectionRescourse($item))->toArray();
        });

        return view('admin.user.section.index', compact('data', 'breadcrumbs'));

    }

    public function fetchData(Request $request)
    {
        $data = app('servicesV1')->sectionService->filtireSections($request)->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) use ($request) {
            return (new SectionRescourse($item))->toArray($request);
        });

        return view('admin.user.section.include.pagination_data', compact('data'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('section.index'), 'name' => _t('dashboard.Sections')],
            ['name' => _t('dashboard.Create')]
        ];

        return view('admin.user.section.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SectionStoreRequest $request)
    {
        $section = app('servicesV1')->sectionService->storeSection($request);

        if (!$section) {
            return redirect()->route('section.index')->with('failed', _t('message.There is an error'));
        }

        return redirect()->route('section.index')->with('success',_t('message.The data created successful'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $section =  app('servicesV1')->sectionService->findSection($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('section.index'), 'name' => _t('dashboard.Sections')],
            ['name' => _t('dashboard.Show')],
            ['name' => "#" . $section->id . ' - '. $section->name .' - ' . $section->created_at->format('m/d/Y')]
        ];


        $data =  (new SectionRescourse($section))->toArray();

        return view('admin.user.section.show', compact('data','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section =  app('servicesV1')->sectionService->findSection($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('section.index'), 'name' => _t('dashboard.Sections')],
            ['name' => _t('dashboard.Edit')],
            ['name' => "#" . $section->id . ' - '. $section->name .' - ' . $section->created_at->format('m/d/Y')]
        ];

        $section =  app('servicesV1')->sectionService->findSection($id);
        $data =  (new SectionRescourse($section))->toArray();

        return view('admin.user.section.edit', compact('data', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SectionUpdateRequest $request, $id)
    {
        $section = app('servicesV1')->sectionService->updateSection($request,$id);

        if (!$section) {
            return redirect()->route('section.index')->with('failed', _t('message.There is an error'));
        }

        return redirect()->route('section.index')->with('success', _t('message.The data updated successful'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = app('servicesV1')->sectionService->deleteSection($id);

        if (!$section) {
            return redirect()->route('section.index')->with('failed', _t('message.There is an error'));
        }

        return redirect()->route('section.index')->with('success', _t('message.The data deleted successful'));
    }

    public function export(){
        return Excel::download(new SectionExport(), _t('dashboard.Sections').''.'.xlsx');
    }
}
