<?php

namespace App\Http\Controllers\Admin\User;

use App\Exports\RoleExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Requests\Admin\User\Role\RoleStoreRequest;
use App\Http\Requests\Admin\User\Role\RoleUpdateRequest;
use App\Http\Resources\Dashboard\Role\RoleResource;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class RoleController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_role'])->only('index');
        $this->middleware(['permission:read_role'])->only('export');
        $this->middleware(['permission:create_role'])->only('create');
        $this->middleware(['permission:update_role'])->only('edit');
        $this->middleware(['permission:delete_role'])->only('destroy');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['name' => _t('dashboard.Roles')]
        ];

        $data = app('servicesV1')->roleService->getAllRoles($request)->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) use ($request) {
            return (new RoleResource($item))->toArray();
        });

        return view('admin.user.role.index', compact('data', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {
        $data = app('servicesV1')->roleService->filterRoles($request)->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) use ($request) {
            return (new RoleResource($item))->toArray($request);
        });

        return view('admin.user.role.include.pagination_data', compact('data'))->render();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('role.index'), 'name' => _t('dashboard.Roles')],
            ['name' => _t('dashboard.Create')]
        ];

        return view('admin.user.role.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleStoreRequest $request)
    {
        $role = app('servicesV1')->roleService->storeRole($request);

               if (!$role) {
            return redirect()->route('role.index')->with('failed', _t('message.There is an error'));
        }

        return redirect()->route('role.index')->with('success', _t('message.The data created successful'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = app('servicesV1')->roleService->findRole($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('role.index'), 'name' => _t('dashboard.Roles')],
            ['name' => _t('dashboard.Show')],
            ['name' => "#" . $role->id . ' - ' . $role->created_at->format('m/d/Y')]
        ];

        $data = (new RoleResource($role))->toArray();

        return view('admin.user.role.show', compact('data', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $role = app('servicesV1')->roleService->findRole($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('role.index'), 'name' => _t('dashboard.Roles')],
            ['name' => _t('dashboard.Edit')],
            ['name' => "#" . $role->id . ' - ' . $role->created_at->format('m/d/Y')]
        ];

        $data = (new RoleResource($role))->toArray();

        return view('admin.user.role.edit', compact('data', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, $id)
    {

        $role = app('servicesV1')->roleService->updateRole($request,$id);

        if (!$role) {
            return redirect()->route('role.index')->with('failed', _t('message.There is an error'));
        }

        return redirect()->route('role.index')->with('success', _t('message.The data updated successful'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = app('servicesV1')->roleService->deleteRole($id);

        if (!$role){
            return redirect()->route('role.index')->with('failed', _t('message.There is an error'));
        }

        return redirect()->route('role.index')->with('success', _t('message.The data deleted successful'));

    }

    public function export()
    {
        return Excel::download(new RoleExport(), _t('dashboard.Roles').'.xlsx');
    }
}
