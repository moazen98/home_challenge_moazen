<?php

namespace App\Http\Controllers\Admin;

use App\Enums\KeyType;
use App\Exports\KeyExport;
use App\Http\Resources\Dashboard\key\KeyResource;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class keyController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $request->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_key'])->only('index');
        $this->middleware(['permission:create_key'])->only('create');
        $this->middleware(['permission:update_key'])->only('edit');
        $this->middleware(['permission:delete_key'])->only('destroy');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['name' => _t('dashboard.Keys')]
        ];

        $data = app('servicesV1')->keyService->getAllData()->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) {
            return (new KeyResource($item))->toArray();
        });

        return view('admin.key.index', compact('data', 'breadcrumbs'));
    }

    public function fetchData(Request $request)
    {

        $data = app('servicesV1')->keyService->filterData($request)->paginate($this->dashboardPaginate);

        $data->getCollection()->transform(function ($item) {
            return (new KeyResource($item))->toArray();
        });

        return view('admin.key.include.pagination_data', compact('data'))->render();
    }

    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('key.index'), 'name' => _t('dashboard.Keys')],
            ['name' => _t('dashboard.Create')]
        ];

        $types = KeyType::getValues();
        $keys = app('servicesV1')->keyService->getAllTypeStore();

        return view('admin.key.create', compact('breadcrumbs', 'types', 'keys'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = app('servicesV1')->keyService->storeData($request);

        if ($result) {
            return redirect()->route('key.index')->with('success', _t('message.The data created successful'));
        }

        return redirect()->route('key.index')->with('failed', _t('message.There is an error'));
    }

    public function show($id)
    {
        $data = app('servicesV1')->keyService->findData($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('key.index'), 'name' => _t('dashboard.Keys')],
            ['name' => _t('dashboard.Show')],
            ['name' => "#" . $data->id . ' - ' . $data->created_at->format('m/d/Y')]

        ];

        $data = (new KeyResource($data))->toArray();

        return view('admin.key.show', compact('data', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = app('servicesV1')->keyService->findData($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => _t('dashboard.Dashboard')],
            ['link' => route('key.index'), 'name' => _t('dashboard.Keys')],
            ['name' => _t('dashboard.Edit')],
            ['name' => "#" . $data->id . ' - ' . $data->created_at->format('m/d/Y')]

        ];

        $data = (new KeyResource($data))->toArray();
        $types = KeyType::getValues();
        $keys = app('servicesV1')->keyService->getAllTypeStore();

        return view('admin.key.edit', compact('data', 'types', 'keys', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $result = app('servicesV1')->keyService->updateData($request, $id);

        if ($result) {
            return redirect()->route('key.index')->with('success', _t('message.The data updated successful'));
        }
        return redirect()->route('key.index')->with('failed', _t('message.There is an error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = app('servicesV1')->keyService->deleteData($id);

        if ($result) {
            return redirect()->route('key.index')->with('success', _t('message.The data deleted successful'));
        }
        return redirect()->route('key.index')->with('failed', _t('message.There is an error'));
    }


    public function export()
    {

        return Excel::download(new KeyExport(), _t('dashboard.Keys') . '.xlsx');
    }

}
