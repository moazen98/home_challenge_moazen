<?php

namespace App\Http\Middleware;

use App\Http\Responses\V1\CustomResponse;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

class SetMobileLocal
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $availableLanguages = config("available_locales.lang");

        $language = $request->header("Accept-Language", "ar");

        App::setLocale($language);

        if (!in_array($language, $availableLanguages) && !is_null($language)) {
            return CustomResponse::Failure(Response::HTTP_INTERNAL_SERVER_ERROR, trans('mobile_message.lang_not_fount'), [], []);
        }

        session(["language" => $language]);

        return $next($request);
    }
}
