<?php

namespace App\Http\Middleware;

use App\Http\Responses\V1\CustomResponse;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Laravel\Sanctum\Sanctum;

class SanctumAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('Authorization')) {
            $token = $request->bearerToken();
            $model = Sanctum::$personalAccessTokenModel;
            $accessToken = $model::where('token', $token)->first();
            if (!$accessToken || !$accessToken->tokenable) {
                return CustomResponse::Failure(Response::HTTP_FORBIDDEN, 'الرجاء تسجيل الدخول أولا', [], []);
            }
            Session::put('current_agency', $accessToken->tokenable);


            return $next($request);
        } else {


            return CustomResponse::Failure(Response::HTTP_FORBIDDEN, trans('mobile_message.login_failed'), [], []);
        }
    }
}
