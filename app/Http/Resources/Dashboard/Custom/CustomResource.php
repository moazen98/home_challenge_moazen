<?php

namespace App\Http\Resources\Dashboard\Custom;

use App\Http\Resources\Dashboard\File\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request = null)
    {

        return [
            'id' => $this->id,
            'title' => $this->title,
            'sub_title' => $this->sub_title,
            'description' => $this->description,
            'type' => $this->type,
            'type_string' => _t('dashboard.'.$this->type),
            'data' => $this,
            'image_url' => $this->customImage == null ? null : $this->customImage->file_path,
            'files' => $this->customImage == null ? null : (new FileResource($this->customImage))->toArray(),
            'date' => $this->created_at->format('Y-m-d H:i'),
        ];
    }
}
