<?php

namespace App\Http\Resources\Dashboard\Custom;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CustomCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'customs' => $this->collection->map(function ($custom) use ($request) {
                return (new CustomResource($custom))->toArray($request);
            })
        ];
    }
}
