<?php

namespace App\Http\Resources\Dashboard\Role;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'roles' => $this->collection->map(function ($role) use ($request) {
                return (new RoleResource($role))->toArray($request);
            })
        ];
    }
}
