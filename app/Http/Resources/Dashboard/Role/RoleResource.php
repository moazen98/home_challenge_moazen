<?php

namespace App\Http\Resources\Dashboard\Role;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'name' => app()->getLocale() == 'ar' ? $this['name_ar'] : $this['name'],
            'name_ar' => $this['name_ar'],
            'name_en' => $this['name'],
            'description' => app()->getLocale() == 'ar' ? $this['description_ar'] : $this['description'],
            'description_ar' => $this['description_ar'],
            'description_en' => $this['description'],
            'employees_number' => $this->users()->count(),
            'is_main' => $this->is_main,
            'is_main_class' => getActiveClass($this->is_main),
            'is_main_string' => getIsMainString($this->is_main),
            'role' => $this,
            'created_at' => $this->created_at->format('m-d-Y'),
        ];
    }
}
