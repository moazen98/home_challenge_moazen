<?php

namespace App\Http\Resources\Dashboard\key;

use Illuminate\Http\Resources\Json\ResourceCollection;

class KeyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'keys' => $this->collection->map(function ($key) use ($request) {
                return (new KeyResource($key))->toArray($request);
            })
        ];
    }
}
