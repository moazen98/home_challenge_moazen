<?php

namespace App\Http\Resources\Dashboard\key;

use Illuminate\Http\Resources\Json\JsonResource;

class KeyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'secret_key' => $this->secret_key,
            'type' => $this->type,
            'type_string' => _t('dashboard.'.$this->type),
            'is_active' => $this->is_active,
            'is_active_string' => getActiveString($this->is_active),
            'is_active_class' => getActiveClass($this->is_active),
            'date' => $this->created_at->format('Y-m-d H:i'),
        ];
    }
}
