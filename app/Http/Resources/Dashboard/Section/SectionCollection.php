<?php

namespace App\Http\Resources\Dashboard\Section;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SectionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'sections' => $this->collection->map(function ($section) use ($request) {
                return (new SectionRescourse($section))->toArray($request);
            })
        ];
    }
}
