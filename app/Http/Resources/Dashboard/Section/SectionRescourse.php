<?php

namespace App\Http\Resources\Dashboard\Section;

use App\Http\Resources\Dashboard\User\UserCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class SectionRescourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'employees' => $this->employees->count() == 0 ? null : (new UserCollection($this->employees))->toArrayMainDetailsUser(),
            'employees_count' => $this->employees->count(),
            'is_active' => $this->is_active,
            'is_active_string' => getActiveString($this->is_active),
            'is_active_class' => getActiveClass($this->is_active),
            'data' => $this,
            'date' => $this->created_at->format('Y-m-d'),
        ];
    }
}
