<?php

namespace App\Http\Resources\Dashboard\Social;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class SocialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this['id'],
            'title' => $this['title'],
            'description' => $this['description'],
            'content' => $this['content'],
            'author' => $this['author'],
            'type' => $this['type'],
            'type_string' => _t('dashboard.'.$this['type']),
            'order' => $this['order'],
            'source' => $this['source'],
            'source_name' => $this->source?->name,
            'category' => $this->source?->category,
            'category_name' => $this->source?->category?->slug,
            'source_id' => $this->source?->id,
            'url' => $this['url'],
            'image_url' => $this->scoailImage?->file_path,
            'published_at' => $this['published_at'],
            'published_at_format' => Carbon::parse($this->published_at)->format('Y-m-d'),
        ];
    }
}
