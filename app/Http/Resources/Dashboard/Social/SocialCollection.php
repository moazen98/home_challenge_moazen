<?php

namespace App\Http\Resources\Dashboard\Social;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SocialCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'socials' => $this->collection->map(function ($social) use ($request) {
                return (new SocialResource($social))->toArray($request);
            })
        ];
    }
}
