<?php

namespace App\Http\Resources\Dashboard\Ordering;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderingCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'items' => $this->collection->map(function ($item) use ($request) {
                return (new OrderingResource($item))->toArray($request);
            })
        ];
    }
}
