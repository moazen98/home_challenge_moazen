<?php

namespace App\Http\Resources\Dashboard\Location\Country;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $lang = app()->getLocale();

        return [
            'id' => $this->id,
            'name' => $this['name_' . $lang],
            'code' => $this->code
        ];
    }
}
