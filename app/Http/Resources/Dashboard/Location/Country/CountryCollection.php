<?php

namespace App\Http\Resources\Dashboard\Location\Country;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CountryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'countries' => $this->collection->map(function ($country) use ($request) {
                return (new CountryResource($country))->toArray($request);
            })
        ];
    }
}
