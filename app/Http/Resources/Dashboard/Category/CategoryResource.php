<?php

namespace App\Http\Resources\Dashboard\Category;

use App\Http\Resources\Dashboard\File\FileResource;
use App\Http\Resources\Dashboard\Seo\KeywordCollection;
use App\Models\Category\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{


    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request = null)
    {
        return [
            'id' => $this->id,
            'value' => $this->slug,
            'value_name' => _t('dashboard.'.$this->slug),
            'type' => $this->type,
            'type_name' => _t('dashboard.'.$this->type),
        ];
    }

}
