<?php

namespace App\Http\Resources\Dashboard\Source;

use Illuminate\Http\Resources\Json\JsonResource;

class SourceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this['id'],
            'name' => $this['name'],
            'description' => $this['description'],
            'url' => $this['url'],
            'category' => $this['category'],
            'category_name' => $this['category']?->slug,
            'language' => $this['language'],
            'country' => $this['country'],
        ];
    }
}
