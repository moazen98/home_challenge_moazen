<?php

namespace App\Http\Resources\Dashboard\Source;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'sources' => $this->collection->map(function ($source) use ($request) {
                return (new SourceResource($source))->toArray($request);
            })
        ];
    }
}
