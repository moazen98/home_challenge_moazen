<?php

namespace App\Http\Resources\Dashboard\Info;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InfoCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'infos' => $this->collection->map(function ($info) use ($request) {
                return (new InfoResource($info))->toArray($request);
            })
        ];
    }
}
