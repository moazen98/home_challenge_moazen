<?php

namespace App\Http\Resources\Dashboard\Info;

use Illuminate\Http\Resources\Json\JsonResource;

class InfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'data' => $this->resource,
            'type' => $this->type,
            'type_string' => _t('dashboard.'.$this->type),
            'is_active' => $this->is_active,
            'is_active_string' => getActiveString($this->is_active),
            'is_active_class' => getActiveClass($this->is_active),
            'date' => $this->created_at->format('Y-m-d H:i'),
        ];
    }
}
