<?php

namespace App\Http\Resources\Dashboard\Setting;

use App\Http\Resources\Dashboard\Seo\KeywordCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {

        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'en_logo' => $this->englishLogo?->file_path,
            'ar_logo' => $this->arabicLogo?->file_path,
            'en_icon' => $this->englishIcon?->file_path,
            'ar_icon' => $this->arabicIcon?->file_path,
            'icon' => app()->getLocale() == 'en' ? $this->englishIcon?->file_path : $this->arabicIcon?->file_path, //TODO: For dashboard
            'logo' => app()->getLocale() == 'en' ? $this->englishLogo?->file_path : $this->arabicLogo?->file_path, //TODO: For dashboard
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'slug' => $this->slug,
            'data' => $this,
        ];
    }
}
