<?php

namespace App\Http\Resources\Dashboard\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request = null)
    {

        return [
            'id' => $this->id,
            'first_name' => $this->authentication == null ? null : $this->authentication->first_name,
            'last_name' => $this->authentication == null ? null : $this->authentication->last_name,
            'full_name' => $this->authentication == null ? null : ($this->authentication->first_name.' '.$this->authentication->last_name),
            'job_title' => $this->job_title,
            'role' => $this->roles()->count() == 0 ? __('No data') : (app()->getLocale() == 'ar' ? $this->roles()->first()->name_ar : $this->roles()->first()->name),
            'role_object' => $this->roles()->count() == 0 ? null : $this->roles()->first(),
            'role_id' => $this->roles()->count() == 0 ? null : $this->roles()->first()->id,
            'phone' => $this->authentication == null ? null : $this->authentication->phone,
            'international_code' => $this->authentication == null ? null : $this->authentication->international_code,
            'full_phone' => $this->authentication == null ? null : ($this->authentication->international_code . $this->authentication->phone),
            'email' => $this->authentication == null ? null : $this->authentication->email,
            'gender' => $this->authentication == null ? null : $this->authentication->gender,
            'birthdate' => $this->authentication == null ? null : $this->authentication->birthdate,
            'address' => $this->address,
            'section' => $this->section == null ? null : $this->section->id,
            'section_name' => $this->section == null ? null : $this->section->name,
            'note' => $this->note,
            'is_active' => $this->authentication == null ? null : $this->authentication->is_active,
            'auth_id' => $this->authentication == null ? null : $this->authentication->id,
            'is_active_string' => $this->authentication == null ? null : getActiveString($this->authentication->is_active),
            'is_active_class' => $this->authentication == null ? null : getActiveClass($this->authentication->is_active),
            'country' => $this->country == null ? null : (app()->getLocale() == 'ar' ? $this->country->name_ar : $this->country->name_en),
            'country_id' => $this->country == null ? null : $this->country->id,
            'city' => $this->city == null ? null : (app()->getLocale() == 'ar' ? $this->city->name_ar : $this->city->name_en),
            'image_path' => $this->authentication == null ? null : ($this->authentication->userImage == null ? null : $this->authentication->userImage->file_path),
            'city_id' => $this->city == null ? null : $this->city->id,
            'basic_user' => $this->basic_user,
            'basic_user_string' =>getMainUserString($this->basic_user),
            'basic_user_class' => getActiveClass($this->basic_user),
            'created_at' => $this->created_at->format('Y-m-d'),
        ];
    }


    public function toArrayLessData($request = null)
    {
        return [
            'id' => $this->id,
            'full_name' => $this->authentication == null ? null : ($this->authentication->first_name.' '.$this->authentication->last_name),
            'email' => $this->authentication == null ? null : $this->authentication->email,
            'is_active' => $this->authentication == null ? null : $this->authentication->is_active,
            'is_active_string' => $this->authentication == null ? null : getActiveString($this->authentication->is_active),
            'is_active_class' => $this->authentication == null ? null : getActiveClass($this->authentication->is_active),
            'image_path' => $this->authentication == null ? null : ($this->authentication->userImage == null ? null : $this->authentication->userImage->file_path),
            'basic_user' => $this->basic_user,
            'basic_user_string' =>getMainUserString($this->basic_user),
            'basic_user_class' => getActiveClass($this->basic_user),
            'role' => $this->roles()->count() == 0 ? __('No data') : (app()->getLocale() == 'ar' ? $this->roles()->first()->name_ar : $this->roles()->first()->name),
        ];
    }

    public function toArrayExport($request = null)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->authentication == null ? null : $this->authentication->first_name,
            'last_name' => $this->authentication == null ? null : $this->authentication->last_name,
            'job_title' => $this->job_title,
            'full_phone' => $this->authentication == null ? null : ($this->authentication->international_code . $this->authentication->phone),
            'email' => $this->authentication == null ? null : $this->authentication->email,
            'address' => $this->address,
            'note' => $this->note,
            'is_active_string' => $this->authentication == null ? null : getActiveString($this->authentication->is_active),
            'created_at' => $this->created_at->format('Y-m-d'),
            'section_name' => $this->section == null ? null : $this->section->name,
            'role' => $this->roles()->count() == 0 ? __('No data') : (app()->getLocale() == 'ar' ? $this->roles()->first()->name_ar : $this->roles()->first()->name),
        ];
    }
}
