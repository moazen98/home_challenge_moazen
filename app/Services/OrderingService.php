<?php

namespace App\Services;


/**
 * Class OrderingService.
 */
class OrderingService extends MainDashboardService
{

    public function updateOrdering($request,$object){

        $itemOrder = json_decode($request->input('order'));

        foreach ($itemOrder as $key => $itemId) {
            $object->where('id', $itemId)->update(['order' => ++$key]);
        }
        return true;
    }

    public function updateOrderingWithType($request,$object,$type){

        $itemOrder = json_decode($request->input('order'));

        foreach ($itemOrder as $key => $itemId) {
            $object->where('id', $itemId)->where('type', $type)->update(['order' => ++$key]);
        }
        return true;
    }
}
