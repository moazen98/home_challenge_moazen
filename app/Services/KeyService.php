<?php

namespace App\Services;

use App\Enums\KeyType;
use App\Models\Key\Key;
use Illuminate\Support\Facades\DB;

/**
 * Class KeyService.
 */
class KeyService
{

    public function getAllData()
    {
        return Key::query()->orderBy('id','DESC');
    }

    public function getDataByType($type)
    {
        return Key::query()->where('type',$type)->first()?->secret_key;
    }



    public function findData($id)
    {
        return Key::findOrFail($id);
    }


    public function storeData($request)
    {

        try {

            DB::beginTransaction();

            $key = new Key();
            $key->secret_key = $request->secret_key;
            $key->type = $request->type;
            $key->is_active = $request->has('is_active') ? 1 : 0;
            $key->save();

            DB::commit();

            return true;

        } catch (\Exception $exception) {
            DB::rollBack();
            return false;
        }
    }


    public function getKeyByType($type)
    {
        return Key::query()->where('type',$type)->first();

    }


    public function updateData($request, $id)
    {

        try {

            DB::beginTransaction();

            $key = Key::findOrFail($id);
            $key->secret_key = $request->secret_key;
            $key->type = $request->type;
            $key->is_active = $request->has('is_active') ? 1 : 0;
            $key->save();


            DB::commit();


            return true;

        } catch (\Exception $exception) {
            DB::rollBack();
            return false;
        }
    }


    public function deleteData($id)
    {

        try {

            DB::beginTransaction();

            $key = Key::findOrFail($id);
            $key->delete();

            DB::commit();

            return true;

        } catch (\Exception $exception) {
            DB::rollBack();
            return false;
        }

    }


    public function filterData($request)
    {

        $keys = Key::query();

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {

            $search = $request->get('query');
            $keys->where(function ($query) use ($search) {
                $query->where('secret_key', 'like', '%' . $search . '%')
                    ->orWhere('id', $search);
            });
        }


        $orderBy = $dataTableFilter['sortby'] ?? 'DESC';

        $keys = $keys->orderBy('id', $orderBy);

        return $keys;
    }

    public function getAllTypeStore()
    {

        return Key::query()->get()->pluck('type');
    }
}
