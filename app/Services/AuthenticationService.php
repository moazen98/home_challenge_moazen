<?php

namespace App\Services;

use App\Enums\AccountStatus;
use App\Enums\AgencyType;
use App\Enums\FcmType;
use App\Enums\UserType;
use App\Http\Responses\V1\CustomResponse;
use App\Models\Agency\Branch;
use App\Models\Agency\Dealership;
use App\Models\Agency\Office;
use App\Models\Authentication;
use App\Models\City;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthenticationService.
 */
class AuthenticationService extends MainDashboardService
{


    public function getUserMobileByEmail($email)
    {

        $credential = Authentication::where('email', $email)->first();


        if (!$credential) {
            return null;
        }

        $user = $credential->authenticatable;

        return $user;

    }

    public function getUserMobileByPhone($phone)
    {

        $credential = Authentication::where('phone', $phone)->first();


        if (!$credential) {
            return null;
        }

        $user = $credential->authenticatable;

        return $user;
    }



    public function loginViaEmail($email, $password)
    {

        if ($user = Authentication::where('email', $email)->first()) {


            if (!$user->is_active) {
                return AccountStatus::NOT_ACTIVE;
            }

            if (!$user->is_verified) {
                return AccountStatus::NOT_VERIFY;
            }


            if (Hash::check($password, $user->password)) {
                return $this->login($user->authenticatable->id, $user->authenticatable_type);
            }

            return 0;
        }

        return 0;
    }


    public function loginViaPhone($phone, $password)
    {
        if ($user = Authentication::where('phone', $phone)->first()) {


            if (!$user->is_active) {
                return AccountStatus::NOT_ACTIVE;
            }

            if (!$user->is_verified) {
                return AccountStatus::NOT_VERIFY;
            }


            if (Hash::check($password, $user->password)) {
                return $this->login($user->authenticatable->id, $user->authenticatable_type);
            }

            return 0;

        }

        return 0;
    }


    public function login($id, $type)
    {

        if ($type == User::class) {

            Auth::guard('web')->loginUsingId($id);
            return 1;
        }


        return 0;

    }


    public function logout()
    {

        if (Auth::guard('web')->check()) {
            Auth::guard('web')->logout();
            return UserType::ADMIN;
        }

        return false;

    }


    public function storeCredentials($user, $request)
    {

        $credentials = $user->authentication()->updateOrCreate(
            [
                'id' => $user->authentication ? $user->authentication->id : null,
            ],
            [
                'phone' => $request->phone,
                'email' => $request->email,
                'international_code' => $request->international_code,
                'password' => $request->password,
                'is_active' => 1,
                'is_verified' => 0,
            ]);

        return $credentials;
    }



    public function updateProfilePassowrd($request)
    {
        $user = getUserMobile($request);

        if (!$user) {
            return ['status' => false, 'message' => trans('mobile_message.failed')];

        }

        $authentication = $user->authentication;

        if (!$authentication) {
            return ['status' => false, 'message' => trans('mobile_message.failed')];
        }


        if (Hash::check($request->old_password, $authentication->password)) {

            if (Hash::check($request->password, $authentication->password)) {

                return ['status' => false, 'message' => trans('mobile_message.password_same_matching')];
            }

            $authentication->password = $request->password;
            $authentication->save();

            return ['status' => true, 'message' => trans('mobile_message.update')];

        } else {
            return ['status' => false, 'message' => trans('mobile_message.password_wrong')];
        }


    }
}
