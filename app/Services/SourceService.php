<?php

namespace App\Services;

use App\Enums\KeyType;
use App\Models\Key\Key;
use App\Models\Source\Source;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

/**
 * Class SourceService.
 */
class SourceService extends MainDashboardService
{
    protected $newsApiKey;
    protected $newsAiApinKey;

    public function __construct()
    {
        $this->newsApiKey = Key::query()->where('type',KeyType::NEWS_API)->first()?->secret_key ?? config('api_keys.news_api_apiKey');
        $this->newsAiApinKey = Key::query()->where('type',KeyType::NEWS_AI_API)->first()?->secret_key ?? config('api_keys.news_api_apiKey');

    }//end of constructor

    public function getSourcesData()
    {

        return Source::query();
    }

    public function getSourcesDataByType($type)
    {

        return Source::query()->where('type', $type)->latest();
    }

    public function getSourcesBySlug($slug)
    {

        return Source::query()->where('slug', $slug)->first();
    }

    //TODO : NewsApi endpoints
    public function getSourceNewsApi($category = null, $language = null, $country = null)
    {

        $URI = 'http://newsapi.org/v2/top-headlines/sources?language=' . $language . '&category=' . $category . '&country=' . $country . '&apiKey=' . $this->newsApiKey;

        $http = Http::withHeaders([]);

        $response = $http->get($URI);

        $result = $response->json();

        return $result['status'] == 'ok' ? $result['sources'] : false;
    }

    public function storeSourceNewsApi($sources, $type)
    {

        DB::beginTransaction();

        foreach ($sources as $source) {

            $src = Source::query()->updateOrCreate(
                [
                    'slug' => $source['id'],
                ],
                [
                    'url' => $source['url'],
                    'country' => $source['country'],
                    'language' => $source['language'],
                    'slug' => $source['id'],
                    'category_id' => app('servicesV1')->categoryService->findDataBySlug($source['category'])?->id,
                    'type' => $type,
                ]);
            $name = isset($source['name']) ? $source['name'] : null;
            $description = isset($source['description']) ? $source['description'] : null;

            $src->setTranslatedAttributes($source['language'], $name, $description);
            $src->save();
        }

        DB::commit();

        return true;
    }


    //TODO : Guardian endpoints
    public function storeGuardianSourceApi($categories, $type)
    {

        DB::beginTransaction();

        foreach ($categories as $category) {

            if ($category['editions']) {
                if ($category['editions'][0]) {
                    $edition = $category['editions'][0];
                    $src = Source::query()->updateOrCreate(
                        [
                            'slug' => $edition['id'],
                        ],
                        [
                            'url' => $edition['webUrl'],
                            'slug' => $edition['id'],
                            'category_id' => app('servicesV1')->categoryService->findDataBySlugAndType($category['id'], $type)?->id,
                            'type' => $type,
                        ]);

                    $name = isset($edition['webTitle']) ? $edition['webTitle'] : null;
                    $description = isset($edition['webTitle']) ? $edition['webTitle'] : null;

                    $src->setTranslatedAttributes(app()->getLocale(), $name, $description);
                    $src->save();
                }
            }

        }

        DB::commit();

        return true;
    }

    //TODO : NewYork endpoints
    public function storeNewYorkSourceApi($blogs, $type)
    {

        DB::beginTransaction();

        foreach ($blogs as $blog) {

            if ($blog['source']) {
                $source = $blog['source'];
                $src = Source::query()->updateOrCreate(
                    [
                        'slug' => $source,
                    ],
                    [
                        'url' => null,
                        'slug' => $source,
                        'category_id' => app('servicesV1')->categoryService->findDataBySlugAndType($blog['section_name'], $type)?->id,
                        'type' => $type,
                    ]);

                $name = $source;
                $description = $source;

                $src->setTranslatedAttributes(app()->getLocale(), $name, $description);
                $src->save();
            }

        }

        DB::commit();

        return true;
    }


    //TODO : NewsApiAi endpoints
    public function getSourceNewsApiAi($category = null, $language = null, $country = null)
    {

        $URI = 'http://eventregistry.org/api/v1/suggestSourcesFast?apiKey=' . $this->newsAiApinKey;

        $http = Http::withHeaders([]);

        $response = $http->get($URI);

        $result = $response->json();

        return $result;
    }


    public function storeSourceNewsApiAi($sources, $type)
    {

        DB::beginTransaction();

        foreach ($sources as $source) {

            $src = Source::query()->updateOrCreate(
                [
                    'slug' => $source['uri'],
                ],
                [
                    'url' => $source['uri'],
                    'country' => null,
                    'language' => null,
                    'slug' => $source['uri'],
                    'category_id' => app('servicesV1')->categoryService->getAllDataByTypeRandomly($type)?->id,
                    'type' => $type,
                ]);
            $name = isset($source['title']) ? $source['title'] : null;
            $description = isset($source['title']) ? $source['title'] : null;

            $src->setTranslatedAttributes(app()->getLocale(), $name, $description);
            $src->save();
        }

        DB::commit();

        return true;
    }
}
