<?php

namespace App\Services;

/**
 * Class KernelServices.
 */
class KernelServices
{

    public $locationService;
    public $authenticationService;
    public $userService;
    public $settingService;
    public $sectionService;
    public $roleService;
    public $fileService;
    public $translationService;
    public $orderingService;
    public $keyService;
    public $sourceService;
    public $customService;
    public $infoService;

    /**
     * KernelServices constructor.
     */
    public function __construct()
    {
        $this->locationService = new LocationService();
        $this->authenticationService = new AuthenticationService();
        $this->userService = new UserService();
        $this->settingService = new SettingService();
        $this->socialService = new SocialService();
        $this->categoryService = new CategoryService();
        $this->sectionService = new SectionService();
        $this->roleService = new RoleService();
        $this->fileService = new FileService();
        $this->translationService = new TranslationService();
        $this->orderingService = new OrderingService();
        $this->keyService = new KeyService();
        $this->sourceService = new SourceService();
        $this->customService = new CustomService();
        $this->infoService = new InfoService();
    }
}
