<?php

namespace App\Services;

use App\Enums\KeyType;
use App\Enums\LanguageType;
use App\Enums\MediaFor;
use App\Models\Key\Key;
use App\Models\Social\Social;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

/**
 * Class SocialService.
 */
class SocialService extends MainDashboardService
{

    protected $newsApiKey;
    protected $newsAiApinKey;
    protected $guardianApinKey;
    protected $newYorkApinKey;

    public function __construct()
    {
        $this->newsApiKey = Key::query()->where('type',KeyType::NEWS_API)->first()?->secret_key ?? config('api_keys.news_api_apiKey');
        $this->newsAiApinKey = Key::query()->where('type',KeyType::NEWS_AI_API)->first()?->secret_key ?? config('api_keys.news_api_apiKey');
        $this->guardianApinKey = Key::query()->where('type',KeyType::GUARDIAN_API)->first()?->secret_key ?? config('api_keys.guardian_api_apiKey');
        $this->newYorkApinKey = Key::query()->where('type',KeyType::NEW_YORK_API)->first()?->secret_key ?? config('api_keys.new_york_api_apiKey');

    }//end of constructor

    public function getAllData()
    {
        return Social::query();
    }


    public function getDataByType($id, $type)
    {
        return Social::where('id', $id)->where('type', $type)->firstOrFail();
    }

    public function getDataByTypeOrder($type)
    {
        return Social::query()->where('type', $type)->orderBy('order', 'ASC');
    }


    public function getAllDataByType($type)
    {
        return Social::query()->where('type', $type)->latest();
    }


    public function deleteDataByType($id, $type)
    {
        DB::beginTransaction();

        $data = Social::where('id', $id)->where('type', $type)->firstOrFail();
        $data->delete();
        $data->save();
        DB::commit();

        return true;
    }


    public function filterData($request, $type)
    {
        $data = Social::query()->where('type', $type);

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {

            $search = $request->get('query');
            $data->where(function ($query) use ($search) {
                $query->whereTranslationLike('description', '%' . $search . '%')
                    ->orWhereTranslationLike('title', '%' . $search . '%')
                    ->orWhereTranslationLike('author', '%' . $search . '%')
                    ->orWhereTranslationLike('content', '%' . $search . '%')
                    ->orWhere('id', $search);
            });
        }

        if ($dataTableFilter['publish_date']) {

            $publishDate = $request->get('publish_date');
            $data->where(function ($query) use ($publishDate) {
                $query->whereDate('published_at', '=', $publishDate);
            });
        }


        if ($dataTableFilter['category'] != 'NON') {
            $categorySearch = $request->get('category');
            $data->whereHas('source', function ($query) use ($categorySearch) {
                $query->whereHas('category', function ($query2) use ($categorySearch) {
                    $query2->where('id', $categorySearch);
                });
            });
        }


        if ($dataTableFilter['source'] != 'NON') {
            $sourceSearch = $request->get('source');
            $data->whereHas('source', function ($query) use ($sourceSearch) {
                $query->where('id', $sourceSearch);
            });
        }

        $orderBy = $dataTableFilter['sortby'] ? $dataTableFilter['sortby'] : 'DESC';

        $data = $data->orderBy('id', $orderBy);

        return $data;
    }

    //TODO : NewsApi endpoints
    //TODO: You should provide a default query because the Required parameters are missing, the scope of your search is too broad
    public function getBlogsNewsApi($pageSize = 100, $query = null, $date = null, $source = null, $author = null, $language = LanguageType::EN, $category = null)
    {

        $URI = 'http://newsapi.org/v2/top-headlines?q=' . $query . '&sources=' . $source . '&pageSize=' . $pageSize . '&from=' . $date . '&category=' . $category . '&language=' . $language . '&apiKey=' . $this->newsApiKey;

        $http = Http::withHeaders([]);

        $response = $http->get($URI);

        $result = $response->json();

        return $result['status'] == 'ok' ? $result['articles'] : false;
    }

    public function storeDataByType($blogs, $type, $withFile = true)
    {
        DB::beginTransaction();

        foreach ($blogs as $index => $blog) {


            if (app('servicesV1')->sourceService->getSourcesBySlug($blog['source']['id'])?->category) {

                $data = Social::query()->updateOrCreate(
                    [
                        'url' => $blog['url'],
                    ],
                    [
                        'order' => ++$index,
                        'url' => $blog['url'],
                        'published_at' => Carbon::parse($blog['publishedAt'])->format('Y-m-d'),
                        'source_id' => $blog['source'] ? app('servicesV1')->sourceService->getSourcesBySlug($blog['source']['id'])?->id : null,
                        'type' => $type,
                    ]);

                $title = isset($blog['title']) ? $blog['title'] : null;
                $description = isset($blog['description']) ? $blog['description'] : null;
                $content = isset($blog['content']) ? $blog['content'] : null;
                $author = isset($blog['author']) ? $blog['author'] : null;

                $data->setTranslatedAttributes(app('servicesV1')->sourceService->getSourcesBySlug($blog['source']['id'])?->language, $title, $description, $author, $content);
                $data->save();

                if ($withFile) {
                    if (isset($blog['urlToImage'])) {
                        downloadRemoteFile($blog['urlToImage'], $blog['source']['id'] . '_' . $index, Social::class, Config::get('custom_file_paths.social_url'), MediaFor::BLOG_MAIN, $data);
                    }
                }

            }
        }


        DB::commit();

        return true;
    }


    //TODO : Guardian endpoints
    public function getBlogsGuardianApi($pageSize = 1000, $query = null, $date = null, $source = null, $author = null, $language = LanguageType::EN, $sectionName = null)
    {
        $URI = 'http://content.guardianapis.com/search?q=' . $query . '&pageSize=' . $pageSize . '&sectionName=' . $sectionName . '&language=' . $language . '&show-fields=body,headline,thumbnail,byline' . '&show-references=author' . '&api-key=' . $this->guardianApinKey;

        $http = Http::withHeaders([]);

        $response = $http->get($URI);

        $result = $response->json();

        return $result['response']['status'] == 'ok' ? $result['response']['results'] : false;
    }


    public function storeDataGuardianByType($blogs, $type, $withFile = true)
    {
        DB::beginTransaction();

        foreach ($blogs as $index => $blog) {

            if (app('servicesV1')->categoryService->findDataBySlugAndType($blog['sectionName'], $type)) {

                $data = Social::query()->updateOrCreate(
                    [
                        'url' => $blog['id'],
                    ],
                    [
                        'order' => ++$index,
                        'url' => $blog['id'],
                        'published_at' => Carbon::parse($blog['webPublicationDate'])->format('Y-m-d'),
                        'source_id' => app('servicesV1')->categoryService->findDataBySlugAndType($blog['sectionName'], $type)->sources()->count() <= 0 ? null : app('servicesV1')->categoryService->findDataBySlugAndType($blog['sectionName'], $type)->sources()->first()->id,
                        'type' => $type,
                    ]);

                $body = isset($blog['fields']['body']) ? $blog['fields']['body'] : null;
                $byline = isset($blog['fields']['byline']) ? $blog['fields']['byline'] : null;
                $headline = isset($blog['fields']['headline']) ? $blog['fields']['headline'] : null;

                $data->setTranslatedAttributes(app()->getLocale(), $blog['webTitle'], $body, $byline, $headline);

                if ($withFile) {
                    if (isset($blog['fields']['thumbnail'])) {
                        downloadRemoteFile($blog['fields']['thumbnail'], $blog['sectionName'] . '_' . $index, Social::class, Config::get('custom_file_paths.social_url'), MediaFor::BLOG_MAIN, $data);
                    }
                }
                $data->save();

            }
        }

        DB::commit();

        return true;
    }


    //TODO : NewYork endpoints
    public function getBlogsNewYorkApi($pageSize = 10, $query = null, $date = null, $source = null, $author = null, $language = LanguageType::EN, $sectionName = null)
    {
        $URI = 'http://api.nytimes.com/svc/search/v2/articlesearch.json?q=' . $query . '&page=' . $pageSize . '&api-key=' . $this->newYorkApinKey;

        $http = Http::withHeaders([]);

        $response = $http->get($URI);

        $result = $response->json();

        return $result['status'] == 'OK' ? $result['response']['docs'] : false;
    }


    public function storeDataNewyorkyType($blogs, $type, $withFile = true)
    {
        DB::beginTransaction();

        foreach ($blogs as $index => $blog) {

            if (app('servicesV1')->categoryService->findDataBySlugAndType($blog['section_name'], $type)) {

                $data = Social::query()->updateOrCreate(
                    [
                        'url' => $blog['web_url'],
                    ],
                    [
                        'order' => ++$index,
                        'url' => $blog['web_url'],
                        'published_at' => Carbon::parse($blog['pub_date'])->format('Y-m-d'),
                        'source_id' => app('servicesV1')->categoryService->findDataBySlugAndType($blog['section_name'], $type)->sources()->count() <= 0 ? null : app('servicesV1')->categoryService->findDataBySlugAndType($blog['section_name'], $type)->sources()->first()->id,
                        'type' => $type,
                    ]);

                $body = isset($blog['lead_paragraph']) ? $blog['lead_paragraph'] : null;
                $byline = isset($blog['byline']) ? $blog['byline']['original'] : null;
                $headline = isset($blog['abstract']) ? $blog['abstract'] : null;
                $title = isset($blog['abstract']) ? $blog['abstract'] : null;

                $data->setTranslatedAttributes(app()->getLocale(), $title, $body, $byline, $headline);

                $data->save();

            }
        }

        DB::commit();

        return true;
    }


    //TODO : NewsApiAi endpoints
    public function getBlogsNewsApiAi($pageSize = 100, $query = null, $date = null, $source = null, $author = null, $language = LanguageType::EN, $category = null)
    {

        $URI = 'http://newsapi.ai/api/v1/article/getArticles?query=%7B%22%24query%22%3A%7B%22conceptUri%22%3A%22http%3A%2F%2Fen.wikipedia.org%2Fwiki%2FTest_cricket%22%7D%2C%22%24filter%22%3A%7B%22forceMaxDataTimeWindow%22%3A%2231%22%7D%7D&resultType=articles&articlesSortBy=rel&apiKey=' . $this->newsAiApinKey;
        $http = Http::withHeaders([]);

        $response = $http->get($URI);

        $result = $response->json();

        return $result ? $result['articles']['results'] : false;
    }


    //TODO:This addons above the three other apis
    public function storeDataNewsAiByType($blogs, $type, $withFile = true)
    {
        DB::beginTransaction();

        foreach ($blogs as $index => $blog) {

            $data = Social::query()->updateOrCreate(
                [
                    'url' => $blog['url'],
                ],
                [
                    'order' => ++$index,
                    'url' => $blog['url'],
                    'published_at' => Carbon::parse($blog['dateTimePub'])->format('Y-m-d'),
                    'source_id' => $blog['source'] ? app('servicesV1')->sourceService->getSourcesBySlug($blog['source']['uri'])?->id : null,
                    'type' => $type,
                ]);

            $title = isset($blog['title']) ? $blog['title'] : null;
            $description = isset($blog['body']) ? $blog['body'] : null;
            $content = isset($blog['body']) ? $blog['body'] : null;
            $author = isset($blog['authors']) ? (isset($blog['authors'][0]) ? $blog['authors'][0]['name'] : null) : null;

            $data->setTranslatedAttributes(app()->getLocale(), $title, $description, $author, $content);
            $data->save();

            if ($withFile) {
                if (isset($blog['image'])) {
                    downloadRemoteFile($blog['image'], $blog['source']['uri'] . '_' . $index, Social::class, Config::get('custom_file_paths.social_url'), MediaFor::BLOG_MAIN, $data);
                }
            }
        }

        DB::commit();

        return true;
    }

}
