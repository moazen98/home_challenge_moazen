<?php

namespace App\Services;

use App\Models\File;
use App\Models\MediaExtension;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\File as FileAlias;

/**
 * Class FileService.
 */
class FileService extends MainDashboardService
{

    public function getFileByType($type)
    {

        $file = File::query()->where('fileable_type', $type)->first();

        return $file;
    }

    public function getFileByTypeOnly($type, $flag = null)
    {

        $file = File::query()->where('fileable_type', $type)->whereNull('fileable_id')->where('flag', $flag)->first();

        return $file;
    }

    public function getFileByTypeAndFlag($type, $flag)
    {

        $file = File::query()->where('fileable_type', $type)->where('flag', $flag)->whereNull('fileable_id')->first();

        return $file;
    }

    public function getFileByTypeAndFlagAndId($type, $flag, $id)
    {

        $file = File::query()->where('fileable_type', $type)->where('flag', $flag)->where('fileable_id', $id)->first();

        return $file;
    }

    public function storeFile($request, $type, $path, $flag = null)
    {

        DB::beginTransaction();

        $file = File::query()->where('fileable_type', $type)->where('flag', $flag)->first();

        if ($file) {

            if ($request->has('files')) {
                $filesRequest = $request->all()['files'];
                $files = upload_single_files($path, null, 'png|jpg|jpeg|gif|mp4', $filesRequest[0]);
                $data = $files[0];

                $file->updateOrCreate([
                        'id' => $file->id
                    ]
                    , [
                        'flag' => $flag,
                        'url' => $data['file_name'],
                        'real_name' => $data['real_name'],
                        'size' => $data['size'],
                        'extension_id' => MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id,
                    ]);
            }

        } else {
            $file = new File();

            if ($request->has('files')) {
                $filesRequest = $request->all()['files'];

                $files = upload_single_files($path, null, 'png|jpg|jpeg|gif|mp4', $filesRequest[0]);
                $data = $files[0];

                $file->flag = $flag;
                $file->url = $data['file_name'];
                $file->fileable_type = $type;
                $file->real_name = $data['real_name'];
                $file->size = $data['size'];
                $file->extension_id = MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id;
                $file->save();

            }
        }

        DB::commit();

        return true;
    }


    //TODO: Under development
    public function storeFileWithDefault($request, $type, $path, $flag = null, $defaultImageUrl = null, $object = null, $fileObject = null)
    {

        DB::beginTransaction();


        if ($fileObject) {

            $file = File::query()->where('fileable_type', $type)->where('flag', $flag)->where('fileable_id', $object->id)->where('flag', $flag)->first();

            if ($file) {

                $filesRequest = $fileObject[0];

                $files = upload_single_files($path, $object->id, 'png|jpg|jpeg|gif|mp4', $filesRequest);
                $data = $files[0];

                $file->updateOrCreate([
                        'id' => $file->id
                    ]
                    , [
                        'flag' => $flag,
                        'url' => $data['file_name'],
                        'real_name' => $data['real_name'],
                        'size' => $data['size'],
                        'extension_id' => MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id,
                    ]);

            } else {

                $file = new File();

                if ($fileObject) {
                    $filesRequest = $fileObject[0];

                    $files = upload_single_files($path, $object->id, 'png|jpg|jpeg|gif|mp4', $filesRequest);
                    $data = $files[0];

                    $file->flag = $flag;
                    $file->url = $data['file_name'];
                    $file->fileable_type = $type;
                    $file->fileable_id = $object->id;
                    $file->real_name = $data['real_name'];
                    $file->size = $data['size'];
                    $file->extension_id = MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id;
                    $file->save();
                }
            }
        } else {

            if ($defaultImageUrl) {

                //TODO: Here I put default image
                $uploadedDefaultFile = new FileAlias($defaultImageUrl);

                if ($uploadedDefaultFile) {

                    $file = new File();

                    $filesObject = $uploadedDefaultFile;

                    $files = upload_single_files_object($path, $object->id, 'png|jpg|jpeg|gif|mp4', $filesObject);

                    $data = $files[0];

                    $file->flag = $flag;
                    $file->fileable_id = $object->id;
                    $file->url = $data['file_name'];
                    $file->fileable_type = $type;
                    $file->real_name = $data['real_name'];
                    $file->size = $data['size'];
                    $file->extension_id = MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id;
                    $file->save();
                }

            }
        }

        DB::commit();

        return true;
    }


    //TODO: Using for direct insert file for the seeder because seeder can't read the files in the folder
    public function storeFileWithDefaultDirect($type, $path, $flag = null, $size = null , $object , $extension=null)
    {

        DB::beginTransaction();

        $file = new File();

        $file->flag = $flag;
        $file->fileable_id = $object->id;
        $file->url = $path;
        $file->fileable_type = $type;
        $file->real_name = $path;
        $file->size = $size;
        $file->extension_id = $extension;
        $file->save();

        DB::commit();

        return true;
    }


    public function storeFileWithDefaultDirectWithObject($type, $path, $flag = null, $object,$defaultImageUrl)
    {

        DB::beginTransaction();
        //TODO: Here I put default image
        $uploadedDefaultFile = new FileAlias($defaultImageUrl);


        if ($uploadedDefaultFile) {

            $file = new File();

            $filesObject = $uploadedDefaultFile;

            $files = upload_single_files_object($path, $object->id, 'png|jpg|jpeg|gif|mp4', $filesObject);

            $data = $files[0];

            $file->flag = $flag;
            $file->fileable_id = $object->id;
            $file->url = $data['file_name'];
            $file->fileable_type = $type;
            $file->real_name = $data['real_name'];
            $file->size = $data['size'];
            $file->extension_id = MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id;
            $file->save();
        }

        DB::commit();

        return true;
    }

    public function storeFileOnly($request, $type, $path, $flag = null)
    {

        DB::beginTransaction();

        $file = File::query()->where('fileable_type', $type)->whereNull('fileable_id')->where('flag', $flag)->first();

        if ($file) {

            if ($request->has('files')) {
                $filesRequest = $request->all()['files'];

                $files = upload_single_files($path, null, 'png|jpg|jpeg|gif|mp4', $filesRequest[0]);
                $data = $files[0];

                $file->updateOrCreate([
                        'id' => $file->id
                    ]
                    , [
                        'flag' => $flag,
                        'url' => $data['file_name'],
                        'real_name' => $data['real_name'],
                        'size' => $data['size'],
                        'extension_id' => MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id,
                    ]);
            }

        } else {
            $file = new File();

            if ($request->has('files')) {
                $filesRequest = $request->all()['files'];

                $files = upload_single_files($path, null, 'png|jpg|jpeg|gif|mp4', $filesRequest[0]);
                $data = $files[0];

                $file->flag = $flag;
                $file->url = $data['file_name'];
                $file->fileable_type = $type;
                $file->real_name = $data['real_name'];
                $file->size = $data['size'];
                $file->extension_id = MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id;
                $file->save();

            }
        }

        DB::commit();

        return true;
    }


    public function storeFileWithModel($request, $type, $path, $flag = null, $model,$objectFile=null)
    {

        DB::beginTransaction();

        $file = File::query()->where('fileable_type', $type)->where('flag', $flag)->where('fileable_id', $model->id)->where('fileable_type', get_class($model))->first();

        if ($file) {

            if ($objectFile) {
                $filesRequest = $objectFile[0];

                $files = upload_single_files($path, $model->id, 'png|jpg|jpeg|gif|mp4', $filesRequest);
                $data = $files[0];
                $file->updateOrCreate([
                        'id' => $file->id
                    ]
                    , [
                        'flag' => $flag,
                        'url' => $data['file_name'],
                        'real_name' => $data['real_name'],
                        'size' => $data['size'],
                        'extension_id' => MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id,
                    ]);

            }

        } else {

            $file = new File();

            if ($objectFile) {
                $filesRequest = $objectFile[0];

                $files = upload_single_files($path, $model->id, 'png|jpg|jpeg|gif|mp4', $filesRequest);
                $data = $files[0];

                $file->fileable()->associate($model);
                $file->flag = $flag;
                $file->url = $data['file_name'];
                $file->fileable_type = $type;
                $file->fileable_id = $model->id;
                $file->real_name = $data['real_name'];
                $file->size = $data['size'];
                $file->extension_id = MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id;
                $file->save();

            }
        }

        DB::commit();

        return true;
    }

    public function getFileByTypeWithModel($type, $flag = null, $model)
    {
        $file = File::query()->where('flag', $flag)->where('fileable_id', $model->id)->first();

        return $file;
    }


    public function deleteFile($id){

        DB::beginTransaction();

        $image = File::findOrFail($id);
        $image->delete();

        DB::commit();

        return true;
    }


    //TODO: it used for the repeater images
    public function storeFileWithModelRepeater($request, $type, $path, $flag = null, $model, $objectFile = null)
    {

        DB::beginTransaction();

        $file = new File();

        if ($objectFile) {
            $filesRequest = $objectFile[0];

            $files = upload_single_files($path, $model->id, 'png|jpg|jpeg|gif|mp4', $filesRequest);
            $data = $files[0];

            $file->fileable()->associate($model);
            $file->flag = $flag;
            $file->url = $data['file_name'];
            $file->fileable_type = $type;
            $file->fileable_id = $model->id;
            $file->real_name = $data['real_name'];
            $file->size = $data['size'];
            $file->extension_id = MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id;
            $file->save();

        }

        DB::commit();

        return true;
    }


    public function storeFileWithWithObject($type, $path, $flag = null, $object, $objectFile)
    {

        DB::beginTransaction();

        if ($objectFile) {

            $file = new File();

            $filesObject = $objectFile;

            $files = upload_single_files_object($path, $object->id, 'png|jpg|jpeg|gif|mp4', $filesObject[0]);

            $data = $files[0];

            $file->flag = $flag;
            $file->fileable_id = $object->id;
            $file->url = $data['file_name'];
            $file->fileable_type = $type;
            $file->real_name = $data['real_name'];
            $file->size = $data['size'];
            $file->extension_id = MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id;
            $file->save();
        }

        DB::commit();

        return true;
    }
}
