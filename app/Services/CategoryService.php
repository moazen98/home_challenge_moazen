<?php

namespace App\Services;

use App\Enums\CategoryNewsApiType;
use App\Enums\KeyType;
use App\Models\Category\Category;
use App\Models\Key\Key;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

/**
 * Class CategoryService.
 */
class CategoryService extends MainDashboardService
{

    protected $newsAiApinKey;
    protected $guardianApinKey;

    public function __construct()
    {
        $this->guardianApinKey = Key::query()->where('type',KeyType::GUARDIAN_API)->first()?->secret_key ?? config('api_keys.guardian_api_apiKey');
        $this->newsAiApinKey = Key::query()->where('type',KeyType::NEWS_AI_API)->first()?->secret_key ?? config('api_keys.news_api_apiKey');
    }//end of constructor


    public function getCategoryNewsApi()
    {

        return CategoryNewsApiType::getValues();
    }


    public function getAllData()
    {
        return Category::latest();
    }

    public function getAllDataByType($type)
    {
        return Category::query()->where('type', $type)->latest();
    }

    public function getAllDataByTypeRandomly($type)
    {
        return Category::query()->where('type', $type)->inRandomOrder()->first();
    }

    public function findData($id)
    {
        return Category::query()->where('id', $id)->orWhere('slug', $id)->firstOrFail();
    }

    public function findDataBySlug($slug)
    {
        return Category::query()->where('slug', $slug)->first();
    }

    public function findDataBySlugAndType($slug, $type)
    {
        return Category::query()->where('slug', $slug)->where('type', $type)->first();
    }


    public function storeDataNewsApi($slug, $type)
    {

        try {

            DB::beginTransaction();

            $data = new Category();
            $data->slug = $slug;
            $data->type = $type;
            $data->save();

            DB::commit();

            return true;

        } catch (\Exception $exception) {
            DB::rollBack();
            return false;
        }
    }

    //TODO : Guardian endpoints
    public function getGuardianCategoriesApi()
    {

        $URI = 'http://content.guardianapis.com/sections?api-key=' . $this->guardianApinKey;

        $http = Http::withHeaders([]);

        $response = $http->get($URI);

        $result = $response->json();

        return $result['response']['status'] == 'ok' ? $result['response']['results'] : false;
    }


    public function storeGuardianCategoriesApi($categories, $type)
    {

        DB::beginTransaction();

        foreach ($categories as $category) {

            $data = Category::query()->updateOrCreate(
                [
                    'slug' => $category['webTitle'],
                ],
                [
                    'slug' => $category['webTitle'],
                    'type' => $type,
                ]);

            $data->save();
        }

        DB::commit();

        return true;
    }

    //TODO : New york endpoints
    public function storeNewyorkCategoriesApi($blogs, $type)
    {

        DB::beginTransaction();

        foreach ($blogs as $blog) {

            if ($blog['section_name']) {

                $data = Category::query()->updateOrCreate(
                    [
                        'slug' => $blog['section_name'],
                    ],
                    [
                        'slug' => $blog['section_name'],
                        'type' => $type,
                    ]);

                $data->save();

            }
        }

        DB::commit();

        return true;
    }

    //TODO : NewsApiAi endpoints
    public function getCategoryNewsApiAi($category = null, $language = null, $country = null)
    {

        $URI = 'http://eventregistry.org/api/v1/suggestCategoriesFast?apiKey=' . $this->newsAiApinKey;

        $http = Http::withHeaders([]);

        $response = $http->get($URI);

        $result = $response->json();

        return $result;
    }

    public function storeNewsApiAiCategoriesApi($categories, $type)
    {

        DB::beginTransaction();

        foreach ($categories as $category) {

                $data = Category::query()->updateOrCreate(
                    [
                        'slug' => $category['uri'],
                    ],
                    [
                        'slug' => $category['uri'],
                        'type' => $type,
                    ]);

                $data->save();

        }

        DB::commit();

        return true;
    }
}
