<?php

namespace App\Services;

use App\Enums\MediaFor;
use App\Models\Setting\Setting;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class SettingService.
 */
class SettingService extends MainDashboardService
{

    public function getSetting()
    {
        return Setting::firstOrFail();
    }

    public function updateSetting($request)
    {
        try {

            DB::beginTransaction();

            $setting = Setting::first();

            $enLogo = checkIfFileExist($request, 'en_logo');
            app('servicesV1')->fileService->storeFileWithModel($request, Setting::class, Config::get('custom_file_paths.image_setting'), MediaFor::LOGO_ENGLISH, $setting, $enLogo);

            $arLogo = checkIfFileExist($request, 'ar_logo');
            app('servicesV1')->fileService->storeFileWithModel($request, Setting::class, Config::get('custom_file_paths.image_setting'), MediaFor::LOGO_ARABIC, $setting, $arLogo);

            $enIcon = checkIfFileExist($request, 'en_icon');
            app('servicesV1')->fileService->storeFileWithModel($request, Setting::class, Config::get('custom_file_paths.image_setting'), MediaFor::ICON_ENGLISH, $setting, $enIcon);

            $arIcon = checkIfFileExist($request, 'ar_icon');
            app('servicesV1')->fileService->storeFileWithModel($request, Setting::class, Config::get('custom_file_paths.image_setting'), MediaFor::ICON_ARABIC, $setting, $arIcon);

            $setting->longitude = $request->longitude;
            $setting->latitude = $request->latitude;
            $setting->slug = $request->slug;

            $setting->setTranslatedAttributes($request);

            $result = app('servicesV1')->seoService->seoOperation($setting, $request);

            if (!$result) {
                DB::rollBack();
                return false;
            }

            $setting->save();

            DB::commit();

            return true;

        } catch (\Exception $exception) {

            DB::rollBack();
            return false;
        }

    }
}
