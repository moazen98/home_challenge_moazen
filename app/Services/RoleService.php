<?php

namespace App\Services;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

/**
 * Class RoleService.
 */
class RoleService extends MainDashboardService
{

    public function getAllRoles()
    {
        $roles = Role::query()->orderBy('id','DESC');
        return $roles;
    }

    public function storeRole($request)
    {

        DB::beginTransaction();

        $role = Role::firstOrCreate([
            'name' => $request->name,
            'name_ar' => $request->name_ar,
            'display_name' => $request->name,
            'description' => $request->description,
            'description_ar' => $request->description_ar
        ]);

        $permissions = [];

        foreach ($request->permissions as $permission) {

            $permissions[] = Permission::where('name', '=', $permission)->first()->id;
        }

        $role->permissions()->sync($permissions);


        DB::commit();

        return true;
    }


    public function updateRole($request, $id)
    {

        $role = Role::findOrFail($id);

        DB::beginTransaction();

        $role->name = $request->name;
        $role->name_ar = $request->name_ar;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->description_ar = $request->description_ar;

        $role->save();

        $permissions = [];

        foreach ($role->permissions as $rolePermissions) {
            $role->detachPermission($rolePermissions);
        }


        foreach ($request->permissions as $permission) {
            $permissions[] = Permission::where('name', $permission)->first()->id;
        }


        $role->permissions()->sync($permissions);

        DB::commit();

        return true;
    }


    public function deleteRole($id)
    {

        $role = Role::findOrFail($id);

        DB::beginTransaction();

        foreach ($role->permissions as $rolePermissions) {
            $role->detachPermission($rolePermissions);
        }

        $role->delete();

        DB::commit();

        return true;
    }

    public function findRole($id)
    {

        $role = Role::findOrFail($id);

        return $role;
    }


    public function filterRoles($request)
    {
        $roles = Role::query();

        $dataTableFilter = $request->all();

        if ($dataTableFilter['query']) {

            $search = $request->get('query');

            $roles->where(function ($query) use ($search) {
                return $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('name_ar', 'like', '%' . $search . '%')
                    ->orWhere('description', 'like', '%' . $search . '%')
                    ->orWhere('description_ar', 'like', '%' . $search . '%');
            });
        }

        $orderBy = $dataTableFilter['sorttype'] ?? 'DESC';

        $roles = $roles->orderBy('id', $orderBy);

        return $roles;
    }
}
