<?php

namespace App\Services;

use App\Models\City;
use App\Models\Country;

/**
 * Class LocationService.
 */
class LocationService
{

    public function getAllCountries(){

        $countries = Country::has('cities')->get();

        return $countries;
    }

    public function getCitiesByCountryId($id){
        $cites = array();
        $cites = City::where('country_id', '=',$id)->get();

        return $cites;
    }


}
