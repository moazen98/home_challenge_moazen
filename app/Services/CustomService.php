<?php

namespace App\Services;

use App\Enums\MediaFor;
use App\Models\Custom\CustomPage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class CustomService.
 */
class CustomService extends MainDashboardService
{
    public function getAllData()
    {
        return CustomPage::latest();
    }



    public function findData($id)
    {
        return CustomPage::findOrFail($id);
    }

    public function getDataByType($type)
    {
        return CustomPage::query()->where('type',$type)->first();
    }


    public function storeData($request)
    {

//        try {

        DB::beginTransaction();

        $custom = new CustomPage();
        $custom->type = $request->type;
        $custom->setTranslatedAttributes($request);
        $custom->save();

        $customImage = checkIfFileExist($request, 'files');
        app('servicesV1')->fileService->storeFileWithModel($request, CustomPage::class, Config::get('custom_file_paths.custom_url'), MediaFor::CUSTOM_PAGE, $custom, $customImage);

        DB::commit();

        return true;

//        } catch (\Exception $exception) {
//            DB::rollBack();
//            return false;
//        }
    }


    public function updateData($id, $request)
    {

//        try {

        DB::beginTransaction();

        $custom = CustomPage::findOrFail($id);
        $custom->type = $request->type;
        $custom->setTranslatedAttributes($request);
        $custom->save();

        $customImage = checkIfFileExist($request, 'files');
        app('servicesV1')->fileService->storeFileWithModel($request, CustomPage::class, Config::get('custom_file_paths.custom_url'), MediaFor::CUSTOM_PAGE, $custom, $customImage);


        DB::commit();

        return true;

//        } catch (\Exception $exception) {
//            DB::rollBack();
//            return false;
//        }
    }

    public function deleteData($id)
    {

        DB::beginTransaction();

        $custom = CustomPage::findOrFail($id);
        $custom->delete();

        DB::commit();

        return true;
    }


    public function filterData($request)
    {
        $data = CustomPage::query();

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {

            $search = $request->get('query');
            $data->where(function ($query) use ($search) {
                $query->whereTranslationLike('title', '%' . $search . '%')
                    ->orWhereTranslationLike('sub_title', '%' . $search . '%')
                    ->orWhereTranslationLike('description', '%' . $search . '%')
                    ->orWhere('id',$search);
            });
        }

        $orderBy = $dataTableFilter['sortby'] ? $dataTableFilter['sortby'] : 'DESC';

        $data = $data->orderBy('id', $orderBy);

        return $data;
    }

    public function getAllCustomsStore(){

        return CustomPage::query()->get()->pluck('type');
    }
}
