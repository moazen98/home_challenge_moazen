<?php

namespace App\Services;

use App\Models\Info\Info;
use Illuminate\Support\Facades\DB;

/**
 * Class InfoService.
 */
class InfoService extends MainDashboardService
{

    public function getAllData()
    {
        return Info::query()->orderBy('id','DESC');
    }


    public function findData($id)
    {
        return Info::findOrFail($id);
    }


    public function storeData($request)
    {

//        try {

        DB::beginTransaction();

        $info = new Info();
        $info->type = $request->type;
        $info->is_active = $request->has('is_active') ? 1 : 0;
        $info->setTranslatedAttributes($request);
        $info->save();

        DB::commit();

        return true;

//        } catch (\Exception $exception) {
//            DB::rollBack();
//            return false;
//        }
    }


    public function updateData($request, $id)
    {

//        try {

        DB::beginTransaction();

        $info = Info::findOrFail($id);
        $info->type = $request->type;
        $info->is_active = $request->has('is_active') ? 1 : 0;
        $info->setTranslatedAttributes($request);
        $info->save();


        DB::commit();


        return true;

//        } catch (\Exception $exception) {
//            DB::rollBack();
//            return false;
//        }
    }


    public function deleteData($id)
    {

        try {

            DB::beginTransaction();

            $info = Info::findOrFail($id);
            $info->delete();

            DB::commit();

            return true;

        } catch (\Exception $exception) {
            DB::rollBack();
            return false;
        }

    }


    public function filterData($request)
    {

        $infos = Info::query();

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {

            $search = $request->get('query');
            $infos->where(function ($query) use ($search) {
                $query->whereTranslationLike('title', '%' . $search . '%')
                    ->orWhereTranslationLike('description', '%' . $search . '%')
                    ->orWhere('id', $search);
            });
        }


        $orderBy = $dataTableFilter['sortby'] ?? 'DESC';

        $infos = $infos->orderBy('id', $orderBy);

        return $infos;
    }

    public function getAllTypeStore(){

        return Info::query()->get()->pluck('type');
    }

}
