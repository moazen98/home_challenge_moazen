<?php

namespace App\Services;

use Alaaeta\Translation\Facades\Translation;
use App\Models\TranslationModel;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

/**
 * Class TranslationService.
 */
class TranslationService extends MainDashboardService
{

    public function updateTranslation($key,$request)
    {
        DB::beginTransaction();

        foreach (config('translation')['locales']  as $code => $locale) {
            Translation::updateOrCreateTranslation([
                'language_code' => $code,
                'key' => $key
            ], [
                'value' => $request->get($code)['title']
            ]);
        }

        DB::commit();

        Artisan::call('view:clear');

        return true;
    }

    public function getTranslation($id){

        $translation = TranslationModel::query()->findOrFail($id);

        $translations = TranslationModel::query()->where('key',$translation->key)->get();

        return $translations;
    }

    public function deleteTranslation($id){

        DB::beginTransaction();

        $data = TranslationModel::query()->findOrFail($id);

        $data->delete();

        DB::commit();

        Artisan::call('cache:clear');

        return true;
    }
}
