<?php

namespace App\Services;

use App\Enums\MediaFor;
use App\Models\Authentication;
use App\Models\City;
use App\Models\Country;
use App\Models\Role;
use App\Models\Section\Section;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class UserService.
 */
class UserService extends MainDashboardService
{

    public function getAllUsers()
    {

        $users = User::query()->orderBy('id', 'DESC');

        return $users;
    }


    public function storeUser($request)
    {
//        try {
        DB::beginTransaction();

        $employee = new User();
        $employee->job_title = $request->job_title;
        $employee->address = $request->address;
        $employee->note = $request->note;

        $section = Section::findOrFail($request->section);
        $employee->section()->associate($section);

        if ($request->has('country')) {
            $country = Country::findOrFail($request->country);
            $employee->country()->associate($country);
        }

        if ($request->has('city')) {
            $city = City::findOrFail($request->city);
            $employee->city()->associate($city);
        }
        $employee->save();
        $credential = $this->storeCredentials($employee, $request);

        if ($credential) {
            $default = getGenderDefault($request->gender);
            $file = checkIfThereIsFilesRequest($request) ? $request->all()['files'] : null;
            app('servicesV1')->fileService->storeFileWithDefault($request, Authentication::class, Config::get('custom_file_paths.image_employee'), MediaFor::PERSONAL_IMAGE, $default, $credential,$file);
        }
        $role = Role::findOrFail($request->role);
        $employee->attachRole($role);

        DB::commit();

        return true;

//        } catch (\Exception $exception) {
//
//            DB::rollBack();
//            return false;
//        }

    }

    public function storeCredentials($user, $request)
    {

        if ($request->has('password')) {

            $credentials = $user->authentication()->updateOrCreate(
                [
                    'id' => $user->authentication ? $user->authentication->id : null,
                ],
                [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'gender' => $request->gender,
                    'birthdate' => $request->birthdate,
                    'phone' => $request->phone_number,
                    'email' => $request->email,
                    'international_code' => $request->international_code,
                    'password' => $request->password,
                    'is_active' => $request->has('is_active') ? 1 : 0,
                    'is_verified' => 1,
                ]);

        } else {
            $credentials = $user->authentication()->updateOrCreate(
                [
                    'id' => $user->authentication ? $user->authentication->id : null,
                ],
                [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'gender' => $request->gender,
                    'birthdate' => $request->birthdate,
                    'phone' => $request->phone_number,
                    'email' => $request->email,
                    'international_code' => $request->phone_number ? $request->international_code : null,
                    'is_active' => $request->has('is_active') ? 1 : 0,
                    'is_verified' => 1,
                ]);
        }

        return $credentials;
    }


    public function updateUser($request, $id)
    {

//        try {

        DB::beginTransaction();

        $employee = User::findOrFail($id);
        $employee->job_title = $request->job_title;
        $employee->address = $request->address;
        $employee->note = $request->note;

        $section = Section::findOrFail($request->section);
        $employee->section()->associate($section);

        if ($request->has('country')) {
            $country = Country::findOrFail($request->country);
            $employee->country()->associate($country);
        }

        if ($request->has('city')) {
            $city = City::findOrFail($request->city);
            $employee->city()->associate($city);
        }

        $employee->save();

        $credential = $this->storeCredentials($employee, $request);

        if ($credential) {
            $file = checkIfThereIsFilesRequest($request) ? $request->all()['files'] : null;
            app('servicesV1')->fileService->storeFileWithModel($request, Authentication::class, Config::get('custom_file_paths.image_employee'), MediaFor::PERSONAL_IMAGE, $credential,$file);
        }

        $role = Role::findOrFail($request->role);
        foreach ($employee->roles as $employeeRole) {
            $employee->detachRole($employeeRole);
        }

        $employee->attachRole($role);

        DB::commit();

        return true;

//        } catch (\Exception $exception) {
//
//            DB::rollBack();
//            return false;
//        }

    }


    public function getUserById($id)
    {
        $user = User::findOrFail($id);

        return $user;
    }

    public function destroyUserById($id)
    {

        DB::beginTransaction();

        $user = User::findOrFail($id);
        $user->delete();

        DB::commit();

        return true;
    }


    public function filterUsers($request)
    {

        $users = User::query();

        $dataTableFilter = $request->all();

        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $users->where(function ($query) use ($search) {
                $query->where('id', 'like', '%' . $search . '%');
            })->orWhere(function ($query2) use ($search) {
                $query2->whereHas('authentication', function ($query3) use ($search) {
                    $query3->where('email', 'like', '%' . $search . '%')->orWhereRaw(
                        "concat(international_code, ' ',  phone) like '%" . $search . "%' "
                    )->orWhereRaw(
                        "concat(international_code, '',  phone) like '%" . $search . "%' "
                    )->orWhereRaw(
                        "concat('+',international_code, '',  phone) like '%" . $search . "%' "
                    )->orWhereRaw(
                        "concat('+',international_code, ' ',  phone) like '%" . $search . "%' "
                    )->orWhere('first_name', 'like', '%' . $search . '%')
                        ->orWhere('last_name', 'like', '%' . $search . '%')
                        ->orWhereRaw(
                            "concat(first_name, ' ',  last_name) like '%" . $search . "%' "
                        );
                });
            });
        }

        if ($dataTableFilter['status'] != "2") {
            $status = $request->get('status');
            $users->whereHas('authentication', function ($query) use ($status) {
                $query->where('is_active', $status);
            });

        }

        if ($dataTableFilter['role'] != "ALL") {
            $role = $request->get('role');
            $users->whereHas('roles', function ($query) use ($role) {
                $query->where('id', $role);
            });
        }

        $orderBy = $dataTableFilter['sorttype'] ?? 'DESC';

        $users = $users->orderBy('id', $orderBy);

        return $users;
    }

}
