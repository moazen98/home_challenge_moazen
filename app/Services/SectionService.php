<?php

namespace App\Services;

use App\Models\Section\Section;
use Illuminate\Support\Facades\DB;

/**
 * Class SectionService.
 */
class SectionService extends MainDashboardService
{

    public function getAllSections()
    {
        $sections = Section::query()->orderBy('id','DESC');
        return $sections;
    }

    public function findSection($id)
    {
        $section = Section::query()->findOrFail($id);
        return $section;
    }

    public function filtireSections($request)
    {

        $sections = Section::query();

        $dataTableFilter = $request->all();

        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $sections->where(function ($query) use ($search) {
                $query->whereTranslationLike('name', '%' . $search . '%')
                ->orWhereTranslationLike('description', '%' . $search . '%')
                ->orWhere('id',$search);
            });
        }

        $orderBy = $dataTableFilter['sorttype'] ?? 'DESC';

        $sections = $sections->orderBy('id', $orderBy);

        return $sections;
    }

    public function getAllActiveSection()
    {
        $sections = Section::query()->where('is_active', 1)->latest();
        return $sections;
    }

    public function storeSection($request){

        $section = new Section();

        DB::beginTransaction();

        $section->setTranslatedAttributes($request);
        $section->is_active = $request->has('is_active') ? 1 : 0;
        $section->save();

        DB::commit();

        return true;
    }

    public function updateSection($request,$id){

        $section = Section::findOrFail($id);

        DB::beginTransaction();

        $section->setTranslatedAttributes($request);
        $section->is_active = $request->has('is_active') ? 1 : 0;
        $section->save();

        DB::commit();

        return true;

    }


    public function deleteSection($id){

        $section = Section::query()->findOrFail($id);

        DB::beginTransaction();

        $section->delete();

        DB::commit();

        return true;
    }
}
