<?php

namespace App\Exceptions;

use App\Enums\CustomPageType;
use App\Http\Resources\Dashboard\Custom\CustomResource;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param $request
     * @param Exception $exception

     * @return mixed
     */
    public function render($request,$exception)
    {

        if($exception instanceof NotFoundHttpException || $exception instanceof ModelNotFoundException){

            $result = app('servicesV1')->customService->getDataByType(CustomPageType::NOT_FOUND);
            $data = ($result ? (new CustomResource($result))->toArray() : null);

            return response()->view('errors.404', ['data'=>$data], 404);
        }

        if ($exception instanceof ClientException) {

            $result = app('servicesV1')->customService->getDataByType(CustomPageType::EXCEPTION);
            $data = ($result ? (new CustomResource($result))->toArray() : null);

            return response()->view('errors.500', ['data'=>$data], 500);

        }

        return parent::render($request, $exception);
    }
}
