<?php

namespace Database\Seeders;

use App\Enums\KeyType;
use App\Models\Key\Key;
use Illuminate\Database\Seeder;

class KeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newsKey = new Key();
        $newsKey->secret_key = config('api_keys.news_api_apiKey');
        $newsKey->type = KeyType::NEWS_API;
        $newsKey->is_active = 1;
        $newsKey->save();

        $guardianKey = new Key();
        $guardianKey->secret_key = config('api_keys.guardian_api_apiKey');
        $guardianKey->type = KeyType::GUARDIAN_API;
        $guardianKey->is_active = 1;
        $guardianKey->save();


        $newYorkKey = new Key();
        $newYorkKey->secret_key = config('api_keys.new_york_api_apiKey');
        $newYorkKey->type = KeyType::NEW_YORK_API;
        $newYorkKey->is_active = 1;
        $newYorkKey->save();


        $newsAiKey = new Key();
        $newsAiKey->secret_key = config('api_keys.news_api_ai_apiKey');
        $newsAiKey->type = KeyType::NEWS_AI_API;
        $newsAiKey->is_active = 1;
        $newsAiKey->save();
    }
}
