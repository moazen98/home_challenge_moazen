<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //TODO: Here I read the cities sql file and insert it in my database
        $path = 'app/developer_docs/cities.sql';
        DB::unprepared(file_get_contents($path));
    }
}
