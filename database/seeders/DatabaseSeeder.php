<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesSeeder::class);
        $this->call(MediaExtensionSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(LaratrustSeeder::class);
//        $this->call(LaratrustSeederUpdateSeeder::class);  //TODO:Just call this seeder when you have new permissions
        $this->call(SectionSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(KeySeeder::class);
        $this->call(InfoSeed::class);
    }
}
