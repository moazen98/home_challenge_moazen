<?php

namespace Database\Seeders;

use App\Models\Section\Section;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $section = Section::create([
            'is_active' => 1,
        ]);

        foreach (config('translatable.locales') as $locale){
            $attr[$locale]['name'] = 'Admin';
            $attr[$locale]['description'] = 'Admin section';
            $section->update($attr);
        }


    }
}
