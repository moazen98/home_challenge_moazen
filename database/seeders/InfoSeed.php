<?php

namespace Database\Seeders;

use App\Enums\InfoType;
use App\Models\Info\Info;
use Illuminate\Database\Seeder;

class InfoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $emailLinkedin = new Info();
        $emailLinkedin->type = InfoType::EMAIL;
        $emailLinkedin->is_active = 1;
        $emailLinkedin->save();

        foreach (config('translatable.locales') as $locale){
            $attr[$locale]['title'] = 'Mohamad Al Moazen Email address';
            $attr[$locale]['description'] = 'mhd.moazen98@gmail.com';
            $emailLinkedin->update($attr);
        }

        $phoneLinkedin = new Info();
        $phoneLinkedin->type = InfoType::MOBILE;
        $phoneLinkedin->is_active = 1;
        $phoneLinkedin->save();

        foreach (config('translatable.locales') as $locale){
            $attr[$locale]['title'] = 'Mohamad Al Moazen Phone number';
            $attr[$locale]['description'] = '+963 0992875193';
            $phoneLinkedin->update($attr);
        }


        $infoLinkedin = new Info();
        $infoLinkedin->type = InfoType::LINKED_IN;
        $infoLinkedin->is_active = 1;
        $infoLinkedin->save();

        foreach (config('translatable.locales') as $locale){
            $attr[$locale]['title'] = 'Mohamad Al Moazen Linked-in account';
            $attr[$locale]['description'] = 'https://www.linkedin.com/in/mohamad-al-moazen/';
            $infoLinkedin->update($attr);
        }
    }
}
