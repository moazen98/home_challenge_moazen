<?php

namespace Database\Seeders;

use App\Enums\MediaFor;
use App\Models\Authentication;
use App\Models\MediaExtension;
use App\Models\Setting\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting();
        $setting->longitude = '55.296249';
        $setting->latitude = '25.276987';
        $setting->save();

        app('servicesV1')->fileService->storeFileWithDefaultDirect(Setting::class,Config::get('custom_file_paths.logo_default'),MediaFor::LOGO_ENGLISH,1.0,$setting,MediaExtension::query()->first()->id);
        app('servicesV1')->fileService->storeFileWithDefaultDirect(Setting::class,Config::get('custom_file_paths.logo_default'),MediaFor::LOGO_ARABIC,1.0,$setting,MediaExtension::query()->first()->id);
        app('servicesV1')->fileService->storeFileWithDefaultDirect(Setting::class,Config::get('custom_file_paths.logo_default'),MediaFor::ICON_ENGLISH,1.0,$setting,MediaExtension::query()->first()->id);
        app('servicesV1')->fileService->storeFileWithDefaultDirect(Setting::class,Config::get('custom_file_paths.logo_default'),MediaFor::ICON_ARABIC,1.0,$setting,MediaExtension::query()->first()->id);

        foreach (config('translatable.locales') as $locale){
            $attr[$locale]['title'] = 'Project test';
            $attr[$locale]['description'] = 'Project test Backend Take-Home challenge by : Mohamad Al Moazen';
            $setting->update($attr);
        }
    }
}
