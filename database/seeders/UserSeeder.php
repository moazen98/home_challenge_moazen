<?php

namespace Database\Seeders;

use App\Enums\GenderType;
use App\Enums\MediaFor;
use App\Models\Authentication;
use App\Models\Country;
use App\Models\MediaExtension;
use App\Models\Section\Section;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'remember_token' => Str::random(10),
            'basic_user' => 1,
            'section_id' => Section::first()->id,
            'country_id' => Country::first()->id,
            'job_title' => 'Admin',
            'address' => 'UAE',
            'note' => 'Admin',
            'city_id' => Country::first()->cities()->first()->id,
        ]);

         $user->authentication()->updateOrCreate([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'admin@gmail.com',
            'phone' => '0999999999',
            'international_code' => '+971',
            'gender' => GenderType::MALE,
            'email_verified_at' => now(),
            'birthdate' => now(),
            'password' => '123456',
            'is_active' => 1,
            'is_verified' => 1,
            'last_login_date' => now(),
        ]);

        app('servicesV1')->fileService->storeFileWithDefaultDirect(Authentication::class,Config::get('custom_file_paths.image_default'),MediaFor::PERSONAL_IMAGE,1.0,$user->authentication,MediaExtension::query()->first()->id);

        $user->attachRole('super_admin');
    }
}
