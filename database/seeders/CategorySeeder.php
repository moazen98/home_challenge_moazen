<?php

namespace Database\Seeders;

use App\Enums\CategoryNewsApiType;
use App\Enums\SocialEnum;
use App\Models\Social\Social;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $categories = CategoryNewsApiType::getValues();

       foreach ($categories as $category){
           app('servicesV1')->categoryService->storeDataNewsApi($category,SocialEnum::NEWS_API);
       }
    }
}
