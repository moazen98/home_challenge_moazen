<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoTranslationsTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_translations_table', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();
            $table->longText('title')->nullable();
            $table->longText('description')->nullable();
            $table->foreignId('info_id')->references('id')->on('infos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_translations_table');
    }
}
