<?php

use App\Enums\SocialEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials', function (Blueprint $table) {
            $table->id();

            $table->string('type');  //TODO : SocialEnum::getValues() enum values
            $table->integer('order')->default(1);
            $table->text('url')->nullable();
            $table->date('published_at')->nullable();
            $table->foreignId('source_id')->nullable()->references('id')->on('sources')->nullOnDelete();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials');
    }
}
