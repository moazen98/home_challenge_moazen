<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_translations', function (Blueprint $table) {
            $table->id();

            $table->string('locale')->index();
            $table->longText('title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('author')->nullable();
            $table->longText('content')->nullable();

            $table->foreignId('social_id')->references('id')->on('socials')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_translations');
    }
}
