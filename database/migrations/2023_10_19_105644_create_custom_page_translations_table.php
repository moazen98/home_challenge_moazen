<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomPageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_page_translations', function (Blueprint $table) {

            $table->id();

            $table->string('locale')->index();
            $table->longText('title')->nullable();
            $table->longText('sub_title')->nullable();
            $table->longText('description')->nullable();

            $table->foreignId('custom_page_id')->references('id')->on('custom_pages')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_page_translations');
    }
}
