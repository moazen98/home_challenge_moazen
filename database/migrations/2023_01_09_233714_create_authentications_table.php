<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthenticationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authentications', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('international_code')->nullable();
            //TODO: if there is shipping in the system you maybe should put the phone required
            $table->string('phone')->nullable();
            //TODO: The password is nullable for the social login
            $table->text('password')->nullable();
            $table->boolean('is_active')->default(true);
            $table->boolean('is_verified')->default(false);
            $table->boolean('is_reset_password')->default(false);
            $table->timestamp('last_login_date')->nullable();
            $table->nullableMorphs('authenticatable');
            $table->date('birthdate')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            //TODO: Custom gender (enum)
            $table->string('gender')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authentications');
    }
}
