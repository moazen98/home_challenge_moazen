<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('source_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();
            $table->longText('name')->nullable();
            $table->longText('description')->nullable();

            $table->foreignId('source_id')->references('id')->on('sources')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_translations');
    }
}
