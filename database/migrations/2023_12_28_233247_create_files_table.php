<?php

use App\Enums\MediaExtension;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('fileable');
            $table->text('flag')->nullable();
            $table->text('url');
            $table->text('title_ar')->nullable();
            $table->text('title_en')->nullable();
            $table->float('size')->nullable();
            $table->text('real_name')->nullable();
            $table->text('media_url')->nullable();
            $table->foreignId('extension_id')->nullable()->references('id')->on('media_extensions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
