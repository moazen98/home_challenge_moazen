<?php


return [

    'user_image_path' => 'storage'.'/'.'user_image'.'/',
    'customer_image_path' => 'storage'.'/'.'customer_image'.'/',
    'api_section' => 'storage'.'/'.'section'.'/',
    'api_slider' => 'storage'.'/'.'api'.'/',
    'api_service' => 'storage'.'/'.'api'.'/',
    'api_advert' => 'storage'.'/'.'api'.'/',

];
