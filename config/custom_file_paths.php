<?php


return [
    'image_default' => 'employee_male_default.jpg',
    'image_employee_male_default' => 'dashboard'.'/'.'employee'.'/'.'employee_male_default.jpg',
    'image_employee_female_default' => 'dashboard'.'/'.'employee'.'/'.'employee_female_default.jpg',
    'image_employee_other_default' => 'dashboard'.'/'.'employee'.'/'.'employee_male_default.jpg',
    'image_employee' => 'storage'.'/'.'employees'.'/'.'images'.'/',
    'image_employee_default_admin' => 'storage'.'/'.'employees'.'/'.'images'.'/'.'1'.'/',


    'logo_default' => 'logo.png',
    'image_logo_default' => 'dashboard'.'/'.'logo'.'/'.'logo.png',
    'image_setting' => 'storage'.'/'.'setting'.'/'.'images'.'/',
    'image_setting_default' => 'storage'.'/'.'setting'.'/'.'images'.'/'.'1'.'/',

    'images' => 'storage'.'/'.'images'.'/',
    'logo' => 'dashboard'.'/'.'logo'.'/',
    'default_logo' => 'logo.png',
    'category_url' => 'storage'.'/'.'category'.'/',
    'social_url' => 'storage'.'/'.'social'.'/',
    'custom_url' => 'storage'.'/'.'custom'.'/',
];
