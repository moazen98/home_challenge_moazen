<?php

return [

    //TODO : This for first time seeder
    'role_structure' => [

        'super_admin' => [
            'employee' => 'c,r,u,d',
            'role' => 'c,r,u,d',
            'section' => 'c,r,u,d',
            'setting' => 'c,r,u,d',
            'category' => 'c,r,u,d',
            'tag' => 'c,r,u,d',
            'blog' => 'c,r,u,d',
            'translation' => 'c,r,u,d',
            'custom' => 'c,r,u,d',
            'key' => 'c,r,u,d',
            'info' => 'c,r,u,d',
        ]
    ],


    //TODO : This for new permission you want to add , Please notice after you add the permission here and after run the db seed of it , please transfer the permission to up to role_structure
    'role_update_structure' => [
        'super_admin' => [
        ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
